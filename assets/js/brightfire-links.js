/*! BrigthFire Core - v1.0.0
 * https://www.brightfire.com
 * Copyright (c) 2018; * Licensed GPL-2.0+ */
var $ = jQuery.noConflict();

$(document).ready( function() {

    // Things we want to do to EVERY <a> tag
    $('a').each( function() {

        set_external_to_new_window( this );
        add_rel_noopener( this );
        add_tel_event_tracking( this );

    });

});

/**
 * Adds target="_blank" to all <a> tags with external href values
 */
function set_external_to_new_window( el ) {

    var href = $(el).attr('href');
    var target = $(el).attr('target');

    // This is an anchor link, or has not external protocol, or has no href attribute
    if ( ! href || '#' === href.substr( 0, 1 ) || ! /^https?:$/.test( el.protocol ) ) {
        return true;
    }

    // Link host differs from current host
    if( el.hostname !== location.hostname ) {
        // External Link
        if( ! target ) {
            $(el).attr('target', '_blank');
        }
    }

    return true;

}

/**
 * Adds rel="noopener" to all <a> tags with target="_blank"
 */
function add_rel_noopener( el ) {

        if( '_blank' === $(el).attr('target') ) {

            var rel = $(el).attr('rel');

            if( rel ) {
                rel = rel.split(' ');
                rel.push("noopener");
            } else {
                rel = ["noopener"];
            }

            $(el).attr('rel', rel.join(' ') );
        }

}

/**
 * Adds event tracking to click for all tel:* links
 * @see https://docs.google.com/spreadsheets/d/1o0luxhQawSqthZiQ3lpA1qAT5HS0sC98XpGcE18fXKs/edit#gid=0
 *
 * @param el
 */
function add_tel_event_tracking( el ) {

    var href = $(el).attr('href');
    var tel  = new RegExp("tel:[0-9]*");

    if( tel.test( href ) ) {

        $(el).on( 'click', function() {
            ga('bfa.send', 'event', 'link', 'call', href );
            ga('clientTracker.send', 'event', 'link', 'call', href );
        });
    }


}