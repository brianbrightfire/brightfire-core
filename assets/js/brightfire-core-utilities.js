var $ = jQuery.noConflict();

/** Is Element in Viewport **/
/** This could serve as a more universal controller for viewport detection which we could better hook and control features such as lazy loading with **/
$.fn.isInViewport = function() {

    var element = $(this);

    var elementTop = '';

    if ( element.is(":hidden") ) {
        element.show();
        elementTop = element.offset().top;
        element.hide();
    } else {
        elementTop = element.offset().top;
    }

    var elementBottom = elementTop + element.outerHeight();

    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    return elementBottom > viewportTop && elementTop < viewportBottom;
};