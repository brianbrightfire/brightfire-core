/*! BrigthFire Core - v1.0.6
 * https://www.brightfire.com
 * Copyright (c) 2019; * Licensed GPL-2.0+ */
var $ = jQuery.noConflict();

/** Is Element in Viewport **/
/** This could serve as a more universal controller for viewport detection which we could better hook and control features such as lazy loading with **/
$.fn.isInViewport = function() {

    var element = $(this);

    var elementTop = '';

    if ( element.is(":hidden") ) {
        element.show();
        elementTop = element.offset().top;
        element.hide();
    } else {
        elementTop = element.offset().top;
    }

    var elementBottom = elementTop + element.outerHeight();

    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    return elementBottom > viewportTop && elementTop < viewportBottom;
};
var $ = jQuery.noConflict();

/**
 * jQuery Ready Statement
 */
jQuery(document).ready(function() {
    REST_listener_load_categories();
});

/**
 * Create an Event Listener for Categories Widget to use REST API
 */
function REST_listener_load_categories() {

    $(document).on('click','.bf-rest-load-categories',function() {

        $(this).replaceWith('Loading...');

        var data = {
            'show_count' : $(this).attr('data-show-count')
        };

        // Run the call
        $.get( RESTurl + '/categories', data, function(response) {
            $('ul.categories-list').html(response);
        });

        $(this).remove();

    });

}
var $ = jQuery.noConflict();

$(document).ready( function() {

    // Things we want to do to EVERY <a> tag
    $('a').each( function() {

        set_external_to_new_window( this );
        add_rel_noopener( this );
        add_tel_event_tracking( this );

    });

});

/**
 * Adds target="_blank" to all <a> tags with external href values
 */
function set_external_to_new_window( el ) {

    var href = $(el).attr('href');
    var target = $(el).attr('target');

    // This is an anchor link, or has not external protocol, or has no href attribute
    if ( ! href || '#' === href.substr( 0, 1 ) || ! /^https?:$/.test( el.protocol ) ) {
        return true;
    }

    // Link host differs from current host
    if( el.hostname !== location.hostname ) {
        // External Link
        if( ! target ) {
            $(el).attr('target', '_blank');
        }
    }

    return true;

}

/**
 * Adds rel="noopener" to all <a> tags with target="_blank"
 */
function add_rel_noopener( el ) {

        if( '_blank' === $(el).attr('target') ) {

            var rel = $(el).attr('rel');

            if( rel ) {
                rel = rel.split(' ');
                rel.push("noopener");
            } else {
                rel = ["noopener"];
            }

            $(el).attr('rel', rel.join(' ') );
        }

}

/**
 * Adds event tracking to click for all tel:* links
 * @see https://docs.google.com/spreadsheets/d/1o0luxhQawSqthZiQ3lpA1qAT5HS0sC98XpGcE18fXKs/edit#gid=0
 *
 * @param el
 */
function add_tel_event_tracking( el ) {

    var href = $(el).attr('href');
    var tel  = new RegExp("tel:[0-9]*");

    if( tel.test( href ) ) {

        $(el).on( 'click', function() {
            ga('bfa.send', 'event', 'link', 'call', href );
            ga('clientTracker.send', 'event', 'link', 'call', href );
        });
    }


}
var $ = jQuery.noConflict();

/**
 * Global container for LazyLoad
 *
 * @type {null}
 */
var bfLazyLoad = null;

/**
 * Instantiate LazyLoad
 */
$(document).on( 'ready bf_selective_refresh_add bf_selective_refresh_rendered', function() {
    bf_lazyload();
});

/**
 * Attach LazyLoad to '.lazyload' selector
 */
function bf_lazyload() {
    bfLazyLoad = new LazyLoad({elements_selector: ".lazyload"});
}

$(document).on('ready', function () {

    // Check for IE browser
    if (navigator.userAgent.match(/Trident/)) {
       bf_ie_image_width();
    }

});

/**
 * Add image width attribute for all images that use scrset and sizes; Backwards compatibility for IE 11 and older
 */
function bf_ie_image_width() {

    var images = $('img[sizes]');

    var viewportWidth = $(window).width();

    $.each(images, function (index, el) {

        var sizes = $(el).attr('sizes');
        var width = 0;

        // Check for a string that
        // 1) ONLY contains an integer followed by "px" or "vw" and is at the END of the line
        var match = sizes.match('([0-9]+)(px|vw)$');

        // Remove empty array elements and nulls
        match = match.filter(function (e) {
            return e;
        });

        if (match) {

            if (match[2] === 'px') {
                width = match[1];
            } else {
                width = (match[1] * viewportWidth) / 100;
            }
        }

        // Add the width attribute with our calculated value
        $(el).attr('width', width);

    });

}
