/*! BrigthFire Core - v1.0.6
 * https://www.brightfire.com
 * Copyright (c) 2019; * Licensed GPL-2.0+ */
function init_checkboxes( target ) {

    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        }
    }

    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );
        parent_element.find('.bf-admin-api.checkboxes').each( function() {
            var self = $(this);
            var current_values = self.find('input.checkbox_multi').val();

            if( '' !== current_values ) {

                $.each(current_values.split(','), function () {
                    if( this.length !== 0 ) {
                        self.find('input[type=checkbox][value=' + this + ']').prop('checked', true);
                    }
                });

            }
        } );
    } );

    // Allow click events on checkbox labels to toggle the checkbox if not disabled
    $( document ).on( "click", "div.checkbox-option > span.checkbox-label", function() {

        var checkbox = $(this).siblings('input[type=checkbox]');

        if( checkbox.prop('disabled') ) {
            return false;
        }

        if( checkbox.prop('checked') ) {
            checkbox.prop('checked',false);
            checkbox.trigger('propertychange');
        } else {
            checkbox.prop('checked',true);
            checkbox.trigger('propertychange');
        }

    });

    // Any time a checkbox experiences a change or property change, update our hidden input
    $( document ).on( "change propertychange", "div.checkbox-option > input[type=checkbox]", function() {

        update_checkbox_field( $(this).closest('.bf-admin-api.checkboxes') );

    });

    $( document ).on( "change propertychange", "input.checkbox_multi", function() {

        update_checkbox_field( $(this).closest('.bf-admin-api.checkboxes') );

    } );

}

function update_checkbox_field( wrapper ) {

    var value = [];

    wrapper.find('input[type=checkbox]').each( function() {

        if( $(this).prop('checked') ) {
            value.push($(this).val());
        }

    });

    // Make a string
    var new_value = value.join( ',' );
    wrapper.find('input.checkbox_multi').val( new_value );

}
$.bf_codeblock = function( el ) {

    var editorSettings = wp.codeEditor.defaultSettings ? _.clone( wp.codeEditor.defaultSettings ) : {};
    editorSettings.codemirror = _.extend(
        {},
        editorSettings.codemirror,
        {
            indentUnit: 2,
            tabSize: 2

        }
    );
    var editor = wp.codeEditor.initialize( $(el), editorSettings );

    // Make sure we update the textarea value for AJAX changes
    editor.codemirror.on( 'change', function( cm ) {
        var textarea = cm.getTextArea();

        var current_value = cm.getValue();

        $(textarea).val( current_value );
    } );

};

$.fn.bf_codeblock = function() {

    return this.each(function() {
        new $.bf_codeblock(this);  // Instantiate a new repeater object for each output range slider
    });
};

function init_codeblock( target ) {


    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        }
    }

    // Init selectize on all selector arrays
    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );
        parent_element.find('.bf-admin-field.codeblock').bf_codeblock();
    } );

}
$(document).on('admin-api-ready', function(){
    $('body.post-php .bf-admin-field').each( function() {
        var element = $(this);
        element.on( 'change', function(){
            $(window).on( 'beforeunload.edit-post', function() {
                return true;
            });
        })
    });
});
function init_colorPickers( target ) {


    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        };
    }

    // Init color picker on all selector arrays
    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );
        parent_element.find( '.bf-admin-field.color-picker' ).wpColorPicker( { change: function(){ $(this).trigger('propertychange'); } } );

    } );
}

// Properly trigger clear
$(document).on( 'click', '.wp-picker-clear', function () {
    $(this).siblings( '.color-picker' ).trigger('change');
});

$(document).on( 'click', '.color-picker-toggle', function() {
    $(this).siblings('.color-picker-helper').toggle();
});

$(document).on( 'click', '.color-swatch', function() {
    var target = $(this).closest('.input-wrapper').find('input[type="text"]');
    $(target).val( $(this).css('background-color') ).trigger('change');

    $(this).closest('.color-picker-helper').toggle();
});
function init_datePickers( target ) {

    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        };
    }

    // Init color picker on all selector arrays
    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );

        // Basic Datepickers
        parent_element.find('.bf-admin-field.date').each(function () {

            // The attribute we want to check
            // FORMAT on atts for Admin API:
            // htmlentities( json_encode( array('minDate' => 0) ) )
            var attr = $(this).attr('datepicker-properties');

            // Check for atts
            if (typeof attr !== typeof undefined && attr !== false) {
                var properties = $(this).attr('datepicker-properties');
                $(this).datepicker($.parseJSON(properties));
            } else {
                $(this).datepicker();
            }

        });

        // DateRange - Start
        parent_element.find('.bf-admin-field.startdate').each(function () {

            var disabled = [];
            var disabledAttr = $(this).attr('datepicker-disable');

            if (typeof disabledAttr !== typeof undefined && disabledAttr !== false) {
                disabled = $.parseJSON(disabledAttr);
            }

            var endDate = $(this).parents('tr').find('.enddate');
            $(this).datepicker({
                minDate: 0,
                onSelect: function () {
                    $(this).change();
                },
                beforeShowDay: function (date) {
                    var string = $.datepicker.formatDate('mm-dd-yy', date);
                    if (disabled.indexOf(string) == -1) {
                        return [true];
                    } else {
                        return [false, "admin-api-date-scheduled", string];
                    }
                }
            });

            $(this).on('change', function () {
                var startDate = $(this).datepicker('getDate');

                // The minDate for our END DATE should be our START DATE
                endDate.datepicker('option', 'minDate', (startDate ? startDate : 0));

                // TO get the maxDate for our END DATE, let's run some logic to look at currently disabled dates
                endDate.datepicker('option', 'maxDate', getEndDateMax(startDate, disabled));
            });

        });

        // DateRange - End
        parent_element.find('.bf-admin-field.enddate').each(function () {

            var disabled = [];
            var disabledAttr = $(this).attr('datepicker-disable');

            if (typeof disabledAttr !== typeof undefined && disabledAttr !== false) {
                disabled = $.parseJSON(disabledAttr);
            }

            var startDate = $(this).parents('tr').find('.startdate');
            $(this).datepicker({
                minDate: 0, // Min date of today
                onSelect: function () {
                    $(this).change();
                },
                beforeShowDay: function (date) {
                    var string = $.datepicker.formatDate('mm-dd-yy', date);

                    if (disabled.indexOf(string) == -1) {
                        return [true]; // Allowed
                    } else {
                        return [false, "admin-api-date-scheduled", string]; // place 'scheduled' class on
                    }
                }
            });

            $(this).on('change', function () {
                var endDate = $(this).datepicker('getDate');

                // To calculate the minDate for our start date, run through some logic
                startDate.datepicker('option', 'minDate', getStartDateMin(endDate, disabled));

                // The maxDate for maxDate for our START DATE should be our END DATE
                startDate.datepicker('option', 'maxDate', endDate);
            });

        });

    } );

}

function getEndDateMax( startDate, disabled ) {

    if( ! startDate ) {
        return '';
    }

    // Sort our disabled dates in ASC order
    disabled.sort(function(a,b){return new Date(a) - new Date(b);});

    var final = ''; // default shouuld be NO MAX DATE for END DATE

    // Loop through our disabled dates
    $.each( disabled, function( index, disabledDate ) {

        // IF this disabled date is greater than our start date then it has to be the maxDate for our END DATE
        if( new Date( disabledDate ) > new Date( startDate ) ) {

            // Subtract one day for technical correctness
            var pos = new Date( disabledDate );
            var offset = pos.setDate( pos.getDate() - 1 );

            final = new Date( offset );
            return false; // leave $.each()
        }

    });

    return final;

}

function getStartDateMin( endDate, disabled ) {

    if( ! endDate ) {
        return 0;
    }

    // Sort our disabled dates in DESC order
    disabled.sort(function(a,b){return new Date(a) - new Date(b);}).reverse();
    endDate = new Date( endDate );

    var final = 0; // Default minDate for START DAY should be today

    // Loop through our disabled dates
    $.each( disabled, function( index, disabledDate ) {

        if( new Date( disabledDate ) < endDate ) {
            final = new Date( disabledDate );
            return false; // leave $.each()
        }

    });

    return final;

}
function init_imagePickers( target ) {

    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        };
    }

    // Init color picker on all selector arrays
    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );

        // Basic Datepickers
        parent_element.on( 'click', '.set-image' , function() {
            set_image( $(this) );
        } );

        parent_element.on( 'click', '.remove-image', function() {
            remove_image( $(this ) );
        } );

    } );
}

function set_image( container ) {
    row = container.parents('.sr-image');

    var image = wp.media({
        title: 'Upload Image',
        multiple: false
    }).open().on('select', function () {

        var uploaded_image = image.state().get('selection').first().toJSON(); // Image object

        if ( typeof uploaded_image.sizes !== 'undefined' && typeof uploaded_image.sizes.thumbnail !== 'undefined' ) {

            image_url = uploaded_image.sizes.thumbnail.url; // Thumbnail src

        } else {

            image_url = uploaded_image.url; // No thumbnail
        }

        container.css('background-image', 'url(' + image_url + ')');
        container.siblings('input').val(uploaded_image.id);
        container.siblings('input').trigger('propertychange');
        container.removeClass('set-image');
        container.addClass('remove-image');

    });
}

function remove_image( container ) {
    container.css('background-image', '');
    container.siblings('input').val('');
    container.siblings('input').trigger('propertychange');
    container.addClass('set-image');
    container.removeClass('remove-image');
}
function init_limitCounters( target ) {

    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        };
    }

    // Init color picker on all selector arrays
    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );
        parent_element.find("[data-limit!=''][data-limit]").each( function() { build_limited_field( $(this) ) })

    } );

}

function build_limited_field( el ) {

    // Count String Lenth (initial)
    var len = el.val().length;
    var lim = el.attr('data-limit');

    // Build Counter Text
    var countWrapper = $('<div>');
    countWrapper.addClass('input-counter');
    countWrapper.html('<span class="len">' + len + '</span> of ' + lim + ' available characters used');

    // Add our counter after the field
    el.after(countWrapper);

    // Build Event Listener
    el.on('keyup keydown focus blur', function() {

        var str = $(this).val();
        var len = str.length;
        var lim = $(this).attr('data-limit');

        // If we're over the limit, truncate
        if( len > lim ) {
            $(this).val( str.substring(0,lim) );
            $(this).siblings('.input-counter').children('span.len').html(lim).css('color','#bf1111');
        } else if ( len == lim ) {
            $(this).siblings('.input-counter').children('span.len').html(len).css('color','#bf1111');
        } else {
            $(this).siblings('.input-counter').children('span.len').html(len).removeAttr('style');
        }

    });

}
$.fn.bf_progress_bar = function( options ) {

    if( this.length > 1 ) {
        this.each( function() { $(this).bf_progress_bar( options ) } );
        return this;
    }

    var $el = $(this);
    var $container, $meter, $message;

    // Set the default options
    var defaults = {
        support_decimal: false,                       // Display percentage with or without decimals

        progress_complete: 50,                        // Items completed
        progress_total: 107,                          // Total Items to complete

        message : 'This is my progress bar at 46% (50 items of 107 to do completed)'
    };

    // Merge the user defined options with the default options
    options = $.extend(defaults, options);

    // Internal Plugin usage
    var settings = {
      progress_display: 46,
      progress_percentage: 46.72897196261682
    };

    /**
     * Builds our basic markup
     * @returns {$.fn.bf_progress_bar}
     */
    this.initialize = function() {

        // Initial Setup
        $el.addClass('bf-progress-bar-container');
        $el.append('<div class="bf-progress-bar-message">Progress Bar...</div><div class="bf-progress-bar"><span class="meter"></span></div>');
        $container = $el.find('div.bf-progress-bar');
        $meter = $el.find('span.meter');
        $message = $el.find('div.bf-progress-bar-message');

        // Initial Setup
        this.setProgress().running().update();

        // Return the object
        return this;

    };

    /**
     * Update all components
     * @returns {$.fn.bf_progress_bar}
     */
    this.update = function() {

        $meter.css( 'width', settings.progress_percentage + '%' );
        $meter.html( settings.progress_display + '%' );
        $message.html( options.message );

        return this;

    };

    /**
     * Set progress and update
     * @param progress
     * @returns {$.fn.bf_progress_bar}
     */
    this.setProgress = function( progress ) {

        if( progress ) {
            options.progress_complete = progress;
        }


        // Calculate Percentage
        settings.progress_percentage = parseFloat( ( options.progress_complete / options.progress_total ) * 100 );

        // Cap at 100
        settings.progress_percentage = Math.min( 100, settings.progress_percentage );

        if( options.support_decimal ) {
            settings.progress_display = settings.progress_percentage.toFixed(2);
        } else {
            settings.progress_display = settings.progress_percentage.toFixed(0);
        }

        this.update();

        return this;

    };

    /**
     * Set progress and update
     * @param progress
     * @returns {$.fn.bf_progress_bar}
     */
    this.setTotal = function( total ) {

        if( total ) {
            options.progress_total = total;
        }


        // Calculate Percentage
        settings.progress_percentage = parseFloat( ( options.progress_complete / options.progress_total ) * 100 );

        // Cap at 100
        settings.progress_percentage = Math.min( 100, settings.progress_percentage );

        if( options.support_decimal ) {
            settings.progress_display = settings.progress_percentage.toFixed(2);
        } else {
            settings.progress_display = settings.progress_percentage.toFixed(0);
        }

        this.update();

        return this;

    };

    /**
     * Set Message and update
     * @param message
     * @returns {$.fn.bf_progress_bar}
     */
    this.setMessage = function( message ) {

        options.message = message;
        this.update();

        return this;

    };

    /**
     * Set state to running
     * @returns {$.fn.bf_progress_bar}
     */
    this.running = function() {
        $meter.addClass('running').removeClass('error').removeClass('complete');

        return this;
    };

    /**
     * Set state to complete
     * @returns {$.fn.bf_progress_bar}
     */
    this.complete = function() {
        $meter.addClass('complete').removeClass('error').removeClass('waiting');

        return this;
    };

    /**
     * Set state to error
     * @returns {$.fn.bf_progress_bar}
     */
    this.error = function() {
        $meter.addClass('error').removeClass('complete').removeClass('waiting');

        return this;
    };

    /**
     * INIT THE PLUGIN
     */
    return this.initialize();
};
$.bf_range = function( el ) {
    var rangeSlider = $(el);
    rangeSlider.id = rangeSlider.attr('data-slider-id');
    rangeSlider.units = rangeSlider.attr('data-slider-units');
    rangeSlider.value = rangeSlider.val();
    rangeSlider.curLabel = $('span[data-slider-id="' + rangeSlider.id + '"]');

    var methods = {

        init: function() {

            rangeSlider.on('input change', function( event ) {
                rangeSlider.value = rangeSlider.val();
                methods.update();
            });

            methods.update();

        },

        update: function() {
            rangeSlider.curLabel.html( rangeSlider.value + ' ' + rangeSlider.units );
        }

    };

    methods.init();
};

$.fn.bf_range = function() {

    return this.each(function() {
        new $.bf_range(this);  // Instantiate a new repeater object for each output range slider
    });
};

function init_range( target ) {


    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        }
    }

    // Init selectize on all selector arrays
    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );
        parent_element.find('.bf-admin-field.range').bf_range();
    } );

}
$.bf_repeater = function( el ) {
    var repeater = $(el); // element class selector the $.bf_repeater function is called on
    var fieldsets = {}; // Holds a reference too all fieldsets and their parts

    // Private Methods
    var methods = {

        /**
         * MAKE IT SO
         */
        init: function() {
            methods.config();

            repeater.find('.bf-repeater-set').each( function() {

                var repeater_set = $(this);

                methods.renderFieldset( repeater_set );
            });

            methods.evh_Customizer();

        },

        /**
         * Configure our repeater object settings
         */
        config: function() {

            // PHP Callbacks (generates repeater-inside content)
            repeater.callbacks = repeater.data('callbacks');

            // JS Callbacks (fired on new repeater add)
            repeater.js_callbacks = repeater.data('js-callbacks');

            // Basic config data
            repeater.config = repeater.data('config');

            // collapse
            if( true === repeater.config.collapse ) {
                repeater.collapse = true;
            }

            // Sortable
            if( true === repeater.config.sortable ) {
                repeater.sortable = true;
            }

            // Oneopen
            if( true === repeater.config.oneopen ) {
                repeater.oneopen = true;
            }

            // Cleanup Markup
            repeater.removeAttr('data-config data-callbacks data-js-callbacks');

        },

        /**
         * Render Fieldset and Parts
         * @param el
         */
        renderFieldset: function( el ) {

            // Register Repeater Parts
            var this_set_id = methods.uuid();

            var fieldset = {};
                fieldset.parent = el;
                fieldset.top = el.find('.bf-repeater-top');
                fieldset.inputs = el.find('input, textarea, select');
                fieldset.inside = el.find('.bf-repeater-inside');

            // Build Repeater Actions
            var actions = $('<div></div>', {
                "class": 'bf-admin-api bf-repeater-actions'
            });
            actions.append('<a href="javascript: void(0);" class="repeater-button del-bf-repeater"><i class="fa fa-trash-o"></i></a>');
            actions.append('<a href="javascript: void(0);" class="repeater-button add-bf-repeater"><i class="fa fa-plus-square-o"></i></a>');

            fieldset.top.append( actions );
            fieldset.actions = el.find('.bf-repeater-actions');

            fieldset.add = actions.find('.add-bf-repeater').not('.disabled');
            fieldset.del = actions.find('.del-bf-repeater').not('.disabled');

            if( true === repeater.collapse ) {
                actions.prepend('<a href="javascript: void(0);" class="repeater-button collapse-bf-repeater"><i class="fa fa-sort-down"></i></a>');
                fieldset.collapse = actions.find('.collapse-bf-repeater').not('.disabled');
                fieldset.inside.hide();
            }

            if( true === repeater.sortable ) {
                fieldset.top.addClass('bf-repeater-handle');
            }

            // Register Repeater ID
            el.attr('data-fieldset-id', this_set_id );

            // Add to our fieldsets object
            fieldsets[this_set_id] = fieldset;

            methods.evh( fieldset.parent );

        },

        /**
         * EVENT HANDLERS
         * @param el
         */
        evh: function( el ) { // 'el' is the parent element to find evh within

            // Get our fieldset id
            var fieldset_id = methods.get_fieldset_id(el);
            var fieldset = fieldsets[fieldset_id];

            // Attach our Event handlers
            fieldset.add.on( 'click', function( event ) {
                event.preventDefault();
                methods.evh_add( fieldset.parent );
            });
            fieldset.del.on( 'click', function( event ) {
                event.preventDefault();
                if( ! $(this).hasClass('disabled') ) {
                    methods.evh_remove( fieldset.parent );
                }
            });
            fieldset.inputs.on( 'change propertychange', function( event ) {
                methods.evh_triggerPropertyChange( fieldset );
            });

            // If collapse
            if( true === repeater.collapse ) {
                fieldset.collapse.on('click', function (event) {
                    event.preventDefault();
                    methods.evh_collapse(fieldset.parent);
                });
            }

            // If sortable
            if( true === repeater.sortable ) {
                methods.evh_sort();
            }
            el.trigger('register-repeater-set');

        },

        /**
         * EVENTS: Customizer
         */
        evh_Customizer: function() {

            // Trigger a propertychange on our first repeater input to force a
            // refresh when repeaters are removed or sorted
            // we don't need one on add, because on initial add, we will not
            // have any data to display yet
            repeater.on('bf-repeater-sort bf-repeater-remove', function() {
                var triggerEle = repeater.find('input, select').filter(':first');
                triggerEle.trigger('propertychange');
            } );

        },

        /**
         * INIT: Sortable
         */
        evh_sort: function() {

            $(repeater).sortable({
                cursor: 'move',
                axis : 'y',
                handle : '.bf-repeater-handle',
                distance: 2,
                opacity: 0.8,
                tolerance: 'pointer',
                start: function(e, ui){
                    ui.placeholder.height(ui.item.height());
                },
                update: function(e,ui){
                    repeater.trigger('bf-repeater-sort');
                }
            });

        },

        /**
         * EVENT: Add Fieldset
         * @param el
         */
        evh_add: function( el ) {

            // Setup the PostVars for AJAX call
            var postVars = {
                action: 'admin_api_repeater_add',
                config: repeater.config,
                callbacks: repeater.callbacks
            };

            // Send the AJAX Request
            $.post( ajaxurl, postVars, function ( response ) {

                var newRepeater = $(response);

                // Put our new set of repeaters in
                if( el ) {
                    el.after(newRepeater); // Add after triggered element
                } else {
                    repeater.append(newRepeater); // append to set wrapper
                }

                // Call each of our js callbacks
                $.each( repeater.js_callbacks, function( index, func ){
                    if('' !== func && func.length !== 0 ) {
                        try {
                            window[func]();
                        }
                        catch(err) {
                            console.warn( err );
                        }
                    }
                });

                // Attach our handlers to the new element
                methods.renderFieldset( newRepeater );

                // Open the new one
                if( true === repeater.collapse ) {
                    methods.evh_collapse(newRepeater);
                }

            }).error( function( response ) {
                console.warn( response.responseText );
            });

            // Trigger Event
            repeater.trigger('bf-repeater-add');

        },

        /**
         * EVENT: Remove Fieldset
         * @param el
         */
        evh_remove: function( el ) {

            var set_id = methods.get_fieldset_id(el);

            // Cleanup fieldsets
            delete fieldsets[set_id];

            el.remove();
            repeater.trigger('bf-repeater-remove');
            if( ! methods.checkRepeaters() ) {
                methods.evh_add(); // Add a starting repeater if none left
            }

        },

        /**
         * EVENT: Collapse Fieldset
         * @param el
         */
        evh_collapse: function( el ) {

            // Get the fieldset
            var set_id = methods.get_fieldset_id(el);
            var fieldset = fieldsets[set_id];

            if( true === repeater.oneopen && ! fieldset.parent.hasClass( 'open' ) ) {

                var openRepeaters = repeater.find('.bf-repeater.open');

                openRepeaters.each( function() {
                    var oRepeaterId = $(this).attr('data-fieldset-id');
                    var oRepeater = fieldsets[oRepeaterId];
                        oRepeater.collapse.html('<i class="fa fa-sort-down"></i>');
                        oRepeater.inside.slideUp( 150 );
                        oRepeater.parent.removeClass('open');
                });

            }

            if( fieldset.parent.hasClass('open') ) {
                fieldset.inside.slideUp( 150 );
                fieldset.collapse.html('<i class="fa fa-sort-down"></i>');
                fieldset.parent.removeClass('open');
            } else {
                fieldset.inside.slideDown(150);
                fieldset.collapse.html('<i class="fa fa-sort-up"></i>');
                fieldset.parent.addClass('open');
            }
        },


        /**
         * Counts active fieldsets
         * @returns {*}
         */
        checkRepeaters: function() {
            return Object.keys(fieldsets).length;
        },

        evh_triggerPropertyChange: function( fieldset ) {
            fieldset.parent.trigger('bf-repeater-update');
        },

        /**
         * Generate a Random 16-char UUID
         * @returns {*}
         */
        uuid: function() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + s4() + s4();
        },

        /**
         * Gets the Fieldset ID Index of an element
         * @param el
         * @returns {*}
         */
        get_fieldset_id: function( el ) {
            return el.attr('data-fieldset-id');
        }

    };

    // Initialize the Repeater
    methods.init();

};

$.fn.bf_repeater = function() {

    return this.each(function() {
        new $.bf_repeater(this);  // Instantiate a new repeater object for each output repeater
    });
};

function init_repeaters( target ) {

    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        }
    }

    // Init selectize on all selector arrays
    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );
        parent_element.find('.bf-admin-api.bf-repeater-container').bf_repeater();
    } );

}
var bf_selectize = {};

/**
 * Creates a GUID to be used
 *
 * TODO: move into js assets or utilities
 *
 * @returns {*}
 */
function makeGUID() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + s4() + s4();
}

/**
 * Helper function to retrive a selectize JS object (instance)
 *
 * @param el : element to return instance for
 * @returns {*}
 */
function bf_selectize_get_instance( el ) {

    return bf_selectize[ el.attr('data-instance-index') ];

}

function add_instance_to_global( instance ) {
    if( instance.length ) { // If any were created, add them to our global
        $.each( instance, function( index, instance ) {
            var instance_id = instance.selectize.$input.attr('data-instance-index');
            bf_selectize[instance_id] = instance;
        });
    }
}

/** SELECTIZE fields **/
$.fn.bf_selectize = function() {
    var this_selectize = $(this).selectize({
        plugins: ['remove_button', 'drag_drop'],
        allowEmptyOption: true,
        onInitialize: function() {
            this.$input.attr( 'data-instance-index', makeGUID() );
            this.$input.trigger( 'selectizeRendered' );
        },
        onChange: function( value ) {
            this.$input.trigger( 'propertychange' );
            if( ! this.$input.val() ) {
                this.$input.html('<option value="" selected="selected"></option>');
            }
        },
        render: {
            option: function (data, escape) {

                var markup = '<div class="selectize-rich-option">';

                if( '' !== data.swatch && typeof data.swatch !== 'undefined' ) {
                    markup += '<div class="selectize-swatch background-' + data.swatch + '"></div>';
                }

                if( '' !== data.preview ) {
                    markup += '<div class="selectize-preview background-' + data.preview +'">' +
                        '<span class="selectize-name">' + escape(data.name) + ': This is a preview of your text</span>' +
                        '<span class="selectize-desc">' + data.desc + '</span>' +
                        '</div>';
                    markup += '</div>'; // selectize-rich-option

                    return markup;
                }

                markup += '<span class="selectize-name">' + escape(data.name) + '</span>' +
                          '<span class="selectize-desc">' + data.desc + '</span>' +
                          '</div>';

                return markup;
            },
            item: function( data, escape ) {

                var category_class = '';
                if( '' !== data.category && typeof data.category !== 'undefined' ) {
                    category_class = 'category ' + data.category;
                }

                return '<div class="selectize-rich-item ' + category_class + '">' + escape(data.name) + '</div>';
            }
        }
    });

    add_instance_to_global( this_selectize );
};

$.fn.fa_selectize = function () {
    var this_selectize = $(this).selectize({
        preload: 'focus',
        onChange: function( value ) {
            this.$input.trigger('propertychange');
        },
        valueField: 'id',
        labelField: 'name',
        searchField: ['name', 'filter'],
        allowEmptyOption: true,
        onInitialize: function() {
            this.$input.attr( 'data-instance-index', makeGUID() );
        },
        render: {
            option: function (data, escape) {
                return '<div class="fontawesome-select"><i class="fa-' + escape(data.id) + ' fa"></i> ' + escape(data.name) + '</div>';
            },
            item: function (data, escape) {
                return '<div class="fontawesome-select"><i class="fa-' + escape(data.id) + ' fa"></i> ' + escape(data.name) + '</div>';
            }
        },
        load: function (query, callback) {
            $.ajax({
                url: '/wp-content/mu-plugins/brightfire-core/assets/fontawesome.json',
                type: 'GET',
                error: function () {
                    callback();
                },
                success: function (res) {

                    // Sort icons in alpha order by name
                    res.icons.sort( function( a, b ) {
                        a = a.name.toLowerCase();
                        b = b.name.toLowerCase();

                        return a < b ? -1 : a > b ? 1 : 0;
                    });

                    callback(res.icons);
                }
            });
        }
    });

    add_instance_to_global( this_selectize );
};

$.fn.url_selectize = function() {
    var this_selectize = $(this).selectize({
        preload: 'focus',
        onChange: function( value ) {
            this.$input.trigger('propertychange');
        },
        valueField: 'id',
        labelField: 'name', 
        searchField: ['name','link','id'],
        allowEmptyOption: true,
        onInitialize: function() {
            this.$input.attr( 'data-instance-index', makeGUID() );
        },
        render: {
            option: function (data, escape) {
                return '<div class="wp-page-select">' + escape(data.name) + '</div>';
            },
            item: function (data, escape) {
                return '<div class="wp-page-select">' + escape(data.name) + '</div>';
            }
        },
        load: function (query, callback) {
            // Send the AJAX Request
            $.post( ajaxurl, {action: 'admin_api_load_posts_pages'}, function ( response ) {
                callback(response);
            })
            .error( function( res ) {
                callback();
            });
        }
    });

    add_instance_to_global( this_selectize );
};

function init_selectize( target ) {

    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#customize-theme-controls",
                not: ""
            },
            2: {
                selector: "#widgets-right",
                not: ""
            }

        }
    }

    // Init selectize on all selector arrays
    $.each( selector_array, function( index, object ) {

        var parent_element = $( object.selector ).not( object.not );
        parent_element.find( 'select.selectize' ).bf_selectize();
        parent_element.find('select.fontawesome-select').fa_selectize();
        parent_element.find('select.wp-page-select').url_selectize();

    } );

}
function init_child_selectize( parent ) {
    parent.find( 'select.selectize' ).bf_selectize();
    parent.find('select.fontawesome-select').fa_selectize();
    parent.find('select.wp-page-select').url_selectize();
}
$.wp_url_search = function( el ) {

    var search = $(el);
    search.delay = 0;
    search.postsLoaded = false;
    search.value = search.val();
    search.userInput = search.value;

    var structure = {};

    var methods = {

        init: function() {

            // Our Control Wrapper
            structure.wrapper = $('<div/>');
            structure.wrapper.addClass('wp-url-search-control');

            // Our User Input
            structure.input = $('<input/>');
            structure.input
                .addClass('wp-url-user-input')
                .addClass('widefat')
                .addClass('bf-admin-field')
                .attr('type','text')
                .val( search.value );

            // Our Dropdown
            structure.dd = $('<div/>');
            structure.dd
                .addClass('wp-url-search-dropdown')
                .hide()
                .html('my dropdown');

            // Loading
            structure.loading = $('<div/>');
            structure.loading
                .addClass('wp-url-search-loading')
                .addClass('loading')
                .html('<div class="text"><span class="spinner spinner-dark is-active"></span> Loading Posts and Pages</div>');

            // Add all the lovely markup
            structure.input.appendTo(structure.wrapper);
            structure.dd.appendTo(structure.wrapper);
            structure.loading.appendTo(structure.wrapper);
            search.after(structure.wrapper);

            // Hide our original input
            search.attr('type','hidden');

            // engage our event handlers
            methods.evh();

            // load posts and pages
            methods.loadPosts();

        },

        evh: function() {

            // Attach our Event handlers
            structure.input.on( 'keyup focus', function( event ) {

                var value = $(this).val();

                search.userInput = value;

                methods.delay( function() {
                    methods.spotlight( value );
                }, 200);

            } );

            // Hide the dropdown on blur
            structure.input.on( 'blur', function( event ) {
                methods.delay( function() {
                    structure.dd.hide();
                    methods.sanitize();
                }, 200 );

            } );

            // Data-Link click
            $(document).on( 'click', '.wp-url-value-link', function( event ) {

                var link = $(this).attr('data-link');
                var name = $(this).attr('data-name');

                var translator = $('<div/>');
                translator.html(link).text();

                var decodedLink = translator.text();

                var container = $(this).parents('div.wp-url-search-control');
                container.find('input.wp-url-user-input').val( decodedLink );

                search.userInput = decodedLink;
                methods.sanitize();


            } );

        },

        delay: function( callback, ms ) {
            clearTimeout ( search.delay );
            search.delay = setTimeout( callback, ms );
        },

        spotlight: function( val ) {

            if( search.postsLoaded ) {

                search.results = [];

                // Do the search
                for( var i = 0; i < search.posts.length; i++ ) {
                    // Search Name
                    if( search.posts[i].name.toLowerCase().indexOf( val.toLowerCase() ) >= 0 || search.posts[i].link.toLowerCase().indexOf( val.toLowerCase() ) >= 0 ) {
                        search.results.push( search.posts[i] );
                    }
                }

                methods.sortResults();

                // Output Results
                var searchResults = $('<div/>');
                searchResults.addClass('results');

                for( var j = 0; j < search.results.length; j++ ) {

                    var icon = '';
                    if( 'page' == search.results[j].type ) {
                        icon = '<span class="dashicons dashicons-admin-page"></span> ';
                    } else {
                        icon = '<span class="dashicons dashicons-admin-post"></span> ';
                    }

                    var newLink = $('<a/>');
                    newLink.attr('data-link',search.results[j].link);
                    newLink.attr('data-name',search.results[j].name);
                    newLink.html( icon + search.results[j].name/* + '<i>' + search.results[j].link + '</i>'*/).text();
                    newLink.addClass('wp-url-value-link');

                    newLink.appendTo(searchResults);

                }

                // Update Dropdown and show it
                structure.dd.html(searchResults);
                structure.dd.show();

            } else {
                console.warn('Hold on a second, hoss - still loading posts and pages');
            }

        },

        loadPosts: function() {

            // Setup the PostVars for AJAX call
            var postVars = {
                action: 'admin_api_load_posts_pages'
            };

            // Send the AJAX Request
            $.post( ajaxurl, postVars, function ( response ) {

                search.posts = response;
                search.postsLoaded = true;
                structure.loading.fadeOut();

            })
                .error( function( response ) {
                    console.warn( response.responseText );
                });

        },

        sortResults: function( field, reverse, primer ) {

            var sort_by = function(field, reverse, primer){

                var key = primer ?
                    function(x) { return primer(x[field]); } :
                    function(x) { return x[field]; };

                reverse = !reverse ? 1 : -1;

                return function (a, b) {
                    return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
                };
            };

            search.results.sort(sort_by('name', false, function(a){ return a.toUpperCase(); }));

        },

        sanitize: function() {

            // REGEX URL Validation
            if(/^([a-z]([a-z]|\d|\+|-|\.)*):(\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?((\[(|(v[\da-f]{1,}\.(([a-z]|\d|-|\.|_|~)|[!\$&'\(\)\*\+,;=]|:)+))\])|((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=])*)(:\d*)?)(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*|(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)){0})(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(search.userInput)) {
                search.val(search.userInput);
            } else if( '' === search.userInput ) {
                search.val('');
            } else {
                // Invalid URL
                structure.input.val(search.val());
            }

            search.trigger('propertychange');

        }

    };

    methods.init();

};

$.fn.wp_url_search = function() {

    return this.each(function() {
        new $.wp_url_search(this);  // Instantiate a new repeater object for each output repeater
    });
};

function init_bf_url( target ) {

    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        }
    }

    // Init selectize on all selector arrays
    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );
        parent_element.find('.bf-admin-field.wp_url').wp_url_search();
    } );
}
var $ = jQuery.noConflict();

/* Loads ALL Admin API jQuery instantiations on document.ready() */
$(document).ready(function() {

    $.when(

        init_codeblock(),

        init_colorPickers(),

        init_datePickers(),

        init_limitCounters(),

        init_imagePickers(),

        init_bf_url(),

        init_selectize(),

        init_repeaters(),

        init_range(),

        init_checkboxes()

    ).then( function() {

        $(document).trigger('admin-api-ready');

    });

});