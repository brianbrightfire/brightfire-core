<?php
/**
 * BrightFire Core Blog Templates Actions.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BlogTemplates;

/**
 * Adds original site ID to new site after it has been copied
 *
 * @param array $template Template.
 */
function copied_from_blog_id( $template ) {
	update_option( 'copied_from_blog_id', $template['blog_id'] );
}
