<?php
/**
 * BrightFire Core Blog Templates Load.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BlogTemplates;

require_once dirname( __FILE__ ) . '/actions.php';

/**
 * Adds 'copied_from_blog_id' option to new sites created from blog templates
 *
 * @uses copied_from_blog_id()
 */
add_action( 'blog_templates-copy-after_copying', __NAMESPACE__ . '\copied_from_blog_id', 10 );
