<?php
/**
 * BrightFire Core Plugins SVG Support Filters.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\SVGSupport;

/**
 * Allow SVG uploads for supers.
 *
 * @param array $mimes Mimes.
 *
 * @return array
 */
function bf_svgs_upload_mimes( $mimes = array() ) {

	if ( current_user_can( 'manage_network' ) ) {

		// Allow SVG file upload.
		$mimes['svg']  = 'image/svg+xml';
		$mimes['svgz'] = 'image/svg+xml';

		return $mimes;

	} else {

		return $mimes;
	}
}
