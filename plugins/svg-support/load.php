<?php
/**
 * Loads required supporting files and adds specific actions and filters.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\SVGSupport;

require_once dirname( __FILE__ ) . '/filters.php';

/**
 * Wait for plugins to be loaded first
 */
add_action(
	'plugins_loaded',
	function() {

		/**
		 * Remove the filter that allows for upload of SVGs so we can fine tune for ourselves
		 */
		remove_filter( 'upload_mimes', 'bodhi_svgs_upload_mimes' );

		/**
		 * Allow Super Admins ability to upload SVG files
		 *
		 * @uses bf_svgs_upload_mimes()
		 */
		add_filter( 'upload_mimes', __NAMESPACE__ . '\bf_svgs_upload_mimes' );
	}
);
