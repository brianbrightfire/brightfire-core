<?php
/**
 * WP Smush Pro plugin specific actions and filters
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\WPSmushPro;

/**
 * Use fontawesome icon in place of linkedin text.
 */
function remove_admin_menu() {

	if ( ! current_user_can( 'manage_network' ) ) {

		remove_submenu_page( 'upload.php', 'wp-smush-bulk' );

	}

}
