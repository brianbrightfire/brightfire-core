<?php
/**
 * WP Smush Pro plugin specific actions and filters
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\WPSmushPro;

/**
 * Loads custom functions after plugins loaded
 *
 * @uses load
 */
add_action( 'plugins_loaded', __NAMESPACE__ . '\load' );

/**
 * Loads custom functions after plugins loaded.
 */
function load() {

	require_once dirname( __FILE__ ) . '/actions.php';

	add_action( 'admin_menu', __NAMESPACE__ . '\remove_admin_menu', 999 );
}
