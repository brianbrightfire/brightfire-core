<?php
/**
 * BrightFire Yoast Breadcrumbs Widget.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Yoast;

/**
 * BrightFire Yoast Breadcrumbs widget class
 */
class BrightFire_Yoast_Breadcrumbs_Widget extends \WP_Widget {

	/**
	 * BrightFire_Yoast_Breadcrumbs_Widget constructor.
	 */
	public function __construct() {

		$widget_ops = array(
			'classname'                   => 'brightfire-yoast-breadcumbs-widget',
			'description'                 => __( 'Breadcrumbs widget.' ),
			'customize_selective_refresh' => true,
		);

		parent::__construct( 'brightfire_yoast_breadcrumbs_widget', __( 'BrightFire Yoast Breadcrumbs Widget' ), $widget_ops );
	}

	/**
	 * Front end display.
	 *
	 * @param array $args Arguments.
	 * @param array $instance Instance.
	 */
	public function widget( $args, $instance ) {

		echo $args['before_widget'];

		echo \BrightFireCore\Yoast\bf_yoast_breadcrumbs();

		echo $args['after_widget'];

	}

	/**
	 * Admin display.
	 *
	 * @param array $instance Instance.
	 * @return string|void
	 */
	public function form( $instance ) {
		echo '<p>There are no options for this widget.</p>';
	}
}
