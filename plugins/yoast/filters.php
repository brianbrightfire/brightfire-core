<?php
/**
 * Yoast action callbacks.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Yoast;

/**
 * Adds the default image field.
 *
 * @param array $fields - our BF Setting Fields.
 *
 * @return mixed
 */
function og_default_image_field( $fields ) {

	$og_image = array(
		'bf_setting_og_default_image' => array(
			'label'       => 'Default Facebook Open Graph Image',
			'description' => 'The recommended image size for Facebook is 1200 by 630 pixels.',
			'type'        => 'wp_image',
		),
	);

	return $fields + $og_image;
}
