<?php
/**
 * Yoast filters and actions
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Yoast;

/** Include Files */
require_once dirname( __FILE__ ) . '/filters.php';
require_once dirname( __FILE__ ) . '/class-brightfire-yoast-breadcrumbs-widget.php';
require_once dirname( __FILE__ ) . '/shortcode-bf-yoast-breadcrumbs.php';

/**
 * Register BrightFire_Footer_Credit_Widget
 */
function register_bf_yoast_breadcrumbs() {
	register_widget( __NAMESPACE__ . '\BrightFire_Yoast_Breadcrumbs_Widget' );
}
add_action( 'widgets_init', __NAMESPACE__ . '\register_bf_yoast_breadcrumbs' );

/** Add Shortcode(s) */
add_shortcode( 'bf_yoast_breadcrumbs', __NAMESPACE__ . '\bf_yoast_breadcrumbs' );

/**
 * Add Yoast fields to BrightFire Site Settings page
 *
 * @uses og_default_image_field()
 */
add_filter( 'bf_general_settings_fields', __NAMESPACE__ . '\og_default_image_field' );

/**
 * Remove default og:image, og:image_secure_url, og:height, and og:width action trigger
 * so we can handle this ourselves.
 */
add_action(
	'wpseo_head',
	function() {
		global $wpseo_og;
		remove_action( 'wpseo_opengraph', array( $wpseo_og, 'image' ), 30 );
		add_action( 'wpseo_opengraph', __NAMESPACE__ . '\og_image', 30 );
	},
	10
);

/**
 * Generates our og:image meta tags.
 */
function og_image() {

	$post_id = get_queried_object_id();

	$image = false;

	if ( has_post_thumbnail( $post_id ) ) {
		$image_id = get_post_thumbnail_id( $post_id );
	} else {
		$image_id = get_option( 'bf_setting_og_default_image' );
	}

	if ( $image_id ) {
		$image = wp_get_attachment_image_src( $image_id, 'full' );
	}

	$image = apply_filters( 'bf_og_image', $image );

	if ( $image ) {

		echo '<meta property="og:image" content="', esc_url( $image[0] ), '" />', "\n";
		echo '<meta property="og:image_secure_url" content="', esc_url( $image[0] ), '" />', "\n";
		echo '<meta property="og:width" content="', esc_attr( $image[1] ), '" />', "\n";
		echo '<meta property="og:height" content="', esc_attr( $image[2] ), '" />', "\n";

	}

}
