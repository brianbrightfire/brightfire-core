<?php
/**
 * Yoast Breadcrumbs Shortcode.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Yoast;

/**
 * Turns on Yoast Breadcrumbs when used.
 *
 * @return string
 */
function bf_yoast_breadcrumbs() {

	// Wrap breadcrumbs in schema with a class.
	if ( function_exists( 'yoast_breadcrumb' ) ) {

		// Turn on breadcrumbs no matter what the setting in the admin.
		add_theme_support( 'yoast-seo-breadcrumbs' );

		// Wrap the breadcrumbs in schema and give a class.
		$output = yoast_breadcrumb( '<div class="breadcrumb" itemprop="breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">', '</div>', false );

	} else {
		$output = 'You must have Yoast - WordPress SEO plugin activated.';
	}

	return $output;
}
