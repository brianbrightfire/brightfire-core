<?php
/**
 * BrightFire Landing Pages Filters.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\LandingPages;

/**
 * Adds BrightFire Landing Pages - "bflp" post type to Stellar required assignments.
 *
 * @param array $types Types.
 *
 * @return array
 */
function add_bflp_template_assignment( $types ) {

	$types = array_merge( $types, [ 'bflp' => 'BF Landing Pages' ] );
	return $types;
}

/**
 * Adds gravity forms post meta settings to edit screen.
 *
 * @param array $settings Settings.
 * @param array $post_meta Post Meta.
 *
 * @return string
 */
function add_bflp_gravity_meta_settings( $settings, $post_meta ) {

	global $bf_admin_api;

	$gravity_settings = $bf_admin_api->fields->build(
		array(
			'fields'     => array(
				'admin_notification_recipients' => array(
					'label'         => 'Admin Notification Recipients',
					'description'   => 'Comma delimited.',
					'type'          => 'text',
					'default_value' => '',
					'sanitize'      => '',
				),
				'submit_button_text'            => array(
					'label'         => 'Submit Button Text',
					'type'          => 'text',
					'default_value' => '',
					'sanitize'      => '',
				),
			),
			'instance'   => $post_meta,
			'display'    => 'table',
			'section_id' => 'bflp_meta',
			'echo'       => false,
		)
	);

	return $gravity_settings . $settings;
}

/**
 * Filter text output for gravity forms submit button
 *
 * @param string $button Button.
 * @param array  $form Form.
 *
 * @return string
 */
function bflp_submit_button( $button, $form ) {

	if ( is_singular( 'bflp' ) ) {

		$gf   = $form['button']['text'];
		$bflp = \BrightFire\Plugin\LandingPages\bflp_shortcode( [ 'field' => 'submit_button_text' ] );

		$button_text = ( ! empty( $bflp ) ) ? $bflp : $gf;

		return '<input type="submit" class="gform_button button" value="' . $button_text . '" id="gform_submit_button_' . $form['id'] . '">';

	} else {

		return $button;
	}
}

/**
 * Filter gravity forms Admin Notification recipients.
 *
 * @param array $notification Notification.
 * @param array $form Form.
 * @param array $entry Entry.
 *
 * @return mixed
 */
function bflp_gravity_form_recipients( $notification, $form, $entry ) {

	if ( is_singular( 'bflp' ) ) {

		if ( 'Admin Notification' === $notification['name'] ) {

			$meta = get_post_meta( get_the_ID(), 'bflp_meta', true );

			if ( isset( $meta['admin_notification_recipients'] ) && ! empty( $meta['admin_notification_recipients'] ) ) {

				$notification['to'] = $meta['admin_notification_recipients'];

			}
		}
	}

	return $notification;
}

/**
 * Adds brightfire employees post meta settings to edit screen
 *
 * @param array $settings Settings.
 * @param array $post_meta Post Meta.
 *
 * @return string
 */
function add_bflp_employee_meta_settings( $settings, $post_meta ) {

	global $bf_admin_api;

	$employee_choices = \BrightFire\Plugin\Agency\bf_employees_get_choices( true, false );

	$employee_settings = $bf_admin_api->fields->build(
		array(
			'fields'     => array(
				'employee_id' => array(
					'type'          => 'selectize',
					'label'         => 'Employee',
					'choices'       => $employee_choices,
					'default_value' => 'no',
				),
			),
			'instance'   => $post_meta,
			'display'    => 'table',
			'section_id' => 'bflp_meta',
			'echo'       => false,
		)
	);

	return $employee_settings . $settings;
}


/**
 * Use saved employee ID for employee shortcode when on a single "blfp" page
 *
 * @param array $out Out.
 *
 * @return mixed
 */
function bflp_employee( $out ) {

	if ( is_singular( [ 'bflp' ] ) ) {

		$out['employee_id'] = \BrightFire\Plugin\LandingPages\bflp_shortcode( [ 'field' => 'employee_id' ] );
	}

	return $out;
}
