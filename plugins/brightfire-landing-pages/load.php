<?php
/**
 * BrightFire Landing Pages Load.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\LandingPages;

/**
 * Load integrations for BrightFire Landing Pages
 */
add_action( 'bflp_loaded', __NAMESPACE__ . '\load' );

/**
 * Load.
 */
function load() {

	require_once dirname( __FILE__ ) . '/filters.php';

	/**
	 * STELLAR
	 */
	if ( \BrightFireCore\is_brightfire_stellar_theme() ) {
		add_filter( 'stellar_required_assignment_types', __NAMESPACE__ . '\add_bflp_template_assignment' );
		add_filter( 'theme_bflp_templates', '\BrightFire\Theme\Stellar\filter_page_templates' );    }

	/**
	 * GRAVITY FORMS
	 */
	if ( class_exists( 'GFAPI' ) ) {
		add_filter( 'gform_submit_button', __NAMESPACE__ . '\bflp_submit_button', 10, 2 );
		add_filter( 'bflp_meta_settings', __NAMESPACE__ . '\add_bflp_gravity_meta_settings', 10, 2 );
		add_filter( 'gform_notification', __NAMESPACE__ . '\bflp_gravity_form_recipients', 11, 3 );
	}

	/**
	 * BF EMPLOYEES
	 */
	if ( class_exists( '\BrightFire\Plugin\Agency\BrightFire_Agency_Employees_CPT' ) ) {
		add_filter( 'bflp_meta_settings', __NAMESPACE__ . '\add_bflp_employee_meta_settings', 10, 2 );
		add_filter( 'shortcode_atts_bf_employees', __NAMESPACE__ . '\bflp_employee', 10, 3 );
		add_filter(
			'agency_employee_id',
			function() {
				return \BrightFire\Plugin\LandingPages\bflp_shortcode( [ 'field' => 'employee_id' ] );
			}
		);
	}
}
