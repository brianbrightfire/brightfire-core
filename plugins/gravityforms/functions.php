<?php
/**
 * BrightFire Core Gravity Forms Functions.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\GravityForms;

/**
 * Returns a formatted array of gravity form ids for use in select inputs
 *
 * @return array
 */
function get_gf_forms() {

	$rtn_forms = array();

	if ( class_exists( 'GFAPI' ) ) {

		$forms = \GFAPI::get_forms();

		foreach ( $forms as $form ) {
			$rtn_forms[ $form['id'] ] = $form['title'];
		}

		$rtn_forms[0] = '-- Select --';

		asort( $rtn_forms );
	}

	return $rtn_forms;
}
