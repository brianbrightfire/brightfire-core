<?php
/**
 * BrightFire Core Gravity Forms Load.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\GravityForms;

require_once dirname( __FILE__ ) . '/functions.php';
require_once dirname( __FILE__ ) . '/actions.php';

/** GFExport Interface and Workers */
require_once dirname( __FILE__ ) . '/exports/load.php';

/**
 * Kills all rules GF would otherwise try to write our the root .htaccess file
 * WPEngine does not allow guest writing of ANY *.php or .htaccess files, so this
 * will never work ( Throws 'Permisssion Denied' E_WARNING )
 */
add_filter( 'gform_upload_root_htaccess_rules', '__return_false' );
