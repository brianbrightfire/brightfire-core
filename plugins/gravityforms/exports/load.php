<?php
/**
 * BrightFire Plugins Gravity Forms Exports Load.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\GravityForms\Exports;

require_once dirname( __FILE__ ) . '/class-gravity-forms-export-csv.php';
require_once dirname( __FILE__ ) . '/class-gravity-forms-export-ui.php';

add_action(
	'plugins_loaded',
	function() {
		new Gravity_Forms_Export_UI();
	}
);
