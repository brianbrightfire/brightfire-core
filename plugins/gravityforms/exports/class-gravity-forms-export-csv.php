<?php
/**
 * BrightFire Plugins Gravity Forms Export CSV.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\GravityForms\Exports;

/**
 * Class Gravity_Forms_Export_CSV
 */
class Gravity_Forms_Export_CSV {

	/**
	 * Forms.
	 *
	 * @var $forms
	 */
	private $forms;

	/**
	 * Parent Blog.
	 *
	 * @var bool $parent_blog
	 */
	private $parent_blog;

	/**
	 * Prefix.
	 *
	 * @var string $prefix
	 */
	private $prefix;

	/**
	 * Gravity_Forms_Export_CSV constructor.
	 *
	 * @param string $prefix Prefix.
	 * @param bool   $parent_blog Parent Blog.
	 */
	public function __construct( $prefix = 'backup', $parent_blog = false ) {

		$this->parent_blog = $parent_blog;
		$this->prefix      = $prefix;

		add_filter(
			'gform_export_max_execution_time',
			function() {
				return 3;
			}
		);
	}

	/**
	 * Builds entry archives for every form that has entries
	 * Automatically places into RGFormsModel::get_upload_root() /export
	 */
	public function run() {

		if ( $this->parent_blog && '1601' !== $this->parent_blog ) {
			switch_to_blog( $this->parent_blog );
		}

		$export_dir = \RGFormsModel::get_upload_root() . 'export/';

		// Requested Export Pattern.
		$pattern = "/export-{$this->prefix}-[a-z0-9-]*.csv/";

		if ( file_exists( $export_dir ) ) {
			$files = scandir( $export_dir );

			foreach ( $files as $index => $file ) {
				if ( preg_match( $pattern, $file ) ) {
					unlink( $export_dir . '/' . $file );
				}
			}
		}

		// We need GF export scripts for this.
		require_once \GFCommon::get_base_path() . '/export.php';

		// Get all forms.
		$this->forms = \GFAPI::get_forms( true, false );

		$form_count = 0;

		// Move through each form.
		foreach ( $this->forms as $form ) {

			// Get Entries for this form.
			$entries   = \GFAPI::get_entries( $form['id'] );
			$form_send = \RGFormsModel::get_form_meta( $form['id'] );

			// If we have entries, we need to build an export file for them.
			if ( ! empty( $entries ) ) {

				// What our file should be called.
				$export_file_name = "{$this->prefix}-" . sanitize_title( $form['title'] );

				$export_field = array();
				foreach ( $form['fields'] as $field ) {
					$export_field[] = $field['id'];
				}

				$_POST['export_field'] = $export_field;
				$this->recurse( $form_send, 0, $export_file_name );

				$form_count++;
			}
		}

		if ( $this->parent_blog ) {
			restore_current_blog();
			$dst = \RGFormsModel::get_upload_root() . 'export/';
			$this->recurse_copy( $export_dir, $dst );
		}

		return $form_count;
	}

	/**
	 * Recurse.
	 *
	 * @param array   $form Form.
	 * @param integer $offset Offset.
	 * @param string  $export_id Export ID.
	 * @return bool
	 */
	public function recurse( $form, $offset, $export_id ) {

		$response = \GFExport::start_export( $form, $offset, $export_id );

		if ( 'in_progress' === $response['status'] ) {
			$this->recurse( $form, $response['offset'], $export_id );
		} elseif ( 'complete' === $response['status'] ) {
			return true;
		}

	}

	/**
	 * Copies ALL contents of $src directory to $dst directory recursively.
	 *
	 * @param string $src Source.
	 * @param string $dst Destination.
	 *
	 * @return bool
	 */
	private function recurse_copy( $src, $dst ) {

		$dir = @opendir( $src ); // phpcs:ignore

		if ( false === $dir ) {
			return false;
		}

		@mkdir( $dst, 0775, true ); // phpcs:ignore
		while ( false !== ( $file = readdir( $dir ) ) ) { // phpcs:ignore
			if ( ( '.' !== $file ) && ( '..' !== $file ) ) {
				if ( is_dir( $src . $file ) ) {
					$this->recurse_copy( $src . $file, $dst . $file );
				} else {
					copy( $src . $file, $dst . $file );
				}
			}
		}
		closedir( $dir );
		return true;
	}

}
