<?php
/**
 * BrightFire Plugins Gravity Forms Export UI.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\GravityForms\Exports;

/**
 * Class Gravity_Forms_Export_UI
 */
class Gravity_Forms_Export_UI {

	/**
	 * Parent Blog ID.
	 *
	 * @var bool|int $parent_blog_id
	 */
	private $parent_blog_id;

	/**
	 * Parent Blog Exists.
	 *
	 * @var bool $parent_blog_exists
	 */
	private $parent_blog_exists = false;

	/**
	 * Exports.
	 *
	 * @var array $exports
	 */
	private $exports = array();

	/**
	 * Skip Parents.
	 *
	 * @var array $skip_parents
	 */
	private $skip_parents = array( '1601', '1123' );

	/**
	 * Gravity_Forms_Export_UI constructor.
	 * Starts our menu item chain if we have a parent blog id
	 */
	public function __construct() {

		// If this site has a prent blog id (was an upgrade).
		$this->parent_blog_id = self::has_parent();
		if ( $this->parent_blog_id ) {
			add_filter( 'gform_addon_navigation', array( $this, 'add_menu_item' ) );
			add_action( 'wp_ajax_gf_download_export', array( __NAMESPACE__ . '\\Gravity_Forms_Export_UI', 'gf_download_export' ) );

			if ( switch_to_blog( $this->parent_blog_id ) ) {
				$this->parent_blog_exists = true;
				restore_current_blog();
			}
		}
	}

	/**
	 * If we have a parent blog id in our options
	 *
	 * @return int|boolean
	 */
	private function has_parent() {
		$option = get_option( 'copied_from_blog_id', false );

		if ( in_array( $option, $this->skip_parents, true ) ) {
			return false;
		} else {
			return $option;
		}
	}

	/**
	 * Adds menu item to GF Admin Menu
	 *
	 * @param array $menus Menus.
	 *
	 * @return array
	 */
	public function add_menu_item( $menus ) {

		$menus[] = array(
			'name'     => 'legacy_archives',
			'label'    => __( 'Archive Form Data' ),
			'callback' => array( $this, 'export_screen_handler' ),
		);
		return $menus;

	}

	/**
	 * Export Screen Handler.
	 */
	public function export_screen_handler() {

		// Handle any requests.
		// TODO: SECURITY - Need nonce verification.
		if ( isset( $_GET['export'] ) ) { // phpcs:ignore

			// TODO: SECURITY - Need nonce verification.
			$export     = new Gravity_Forms_Export_CSV( $_GET['export'], $this->parent_blog_id ); // phpcs:ignore
			$form_count = $export->run();

			if ( ! empty( $form_count ) ) {
				echo '<div class="notice notice-success is-dismissible">';
				echo "<p>Export successful! {$form_count} form(s) available to download.</p>";
				echo '</div>';
			}
		}

		// Do our output.
		$this->screen_output();

	}

	/**
	 * Output for our custom admin screen.
	 */
	private function screen_output() {

		$this->get_exports();

		echo '<h1>Parent Blog Exports</h1>';

		if ( array_key_exists( 'parent', $this->exports ) && ! empty( $this->exports['parent'] ) ) {
			$this->list_exports( 'parent' );
		} else {
			echo '<p><i>There are currently no exports available for download.</i></p>';
		}

		if ( $this->parent_blog_exists ) {
			echo '<p><a href="' . admin_url() . '/admin.php?page=legacy_archives&export=parent" class="button button-primary">Export Entries From Parent Site</a></p>';
		} else {
			echo '<div class="notice notice-warning is-dismissible">';
			echo '<p>Parent site no longer exists.</p>';
			echo '</div>';
		}

		?>
		<script type="text/javascript">
			$("a.download-export").on( "click", function() {

				var filename = $(this).data('filename');
				var path = $(this).data('path');

				var url = ajaxurl + '?action=gf_download_export&_wpnonce=<?php echo wp_create_nonce( 'gform_download_export' ); ?>&filename=' + filename + '&path=' + path;
				window.open(url);

			});
		</script>
		<?php

	}

	/**
	 * Get Exports.
	 */
	private function get_exports() {

		$dir = \RGFormsModel::get_upload_root() . 'export/';

		if ( file_exists( $dir ) ) {
			$files = scandir( $dir );

			foreach ( $files as $index => $file ) {

				// Default Backups.
				$pattern = '/export-parent-[a-z0-9-]*.csv/';
				if ( preg_match( $pattern, $file ) ) {
					$this->exports['parent'][] = $file;
				}

				// 2.3 Backup Exports.
				$pattern = '/export-2-3-[a-z0-9-]*.csv/';
				if ( preg_match( $pattern, $file ) ) {
					$this->exports['2-3'][] = $file;
				}
			}
		}
	}

	/**
	 * List Exports.
	 *
	 * @param string $prefix Prefix.
	 */
	private function list_exports( $prefix ) {

		$path = base64_encode( \RGFormsModel::get_upload_root() . 'export/' ); // phpcs:ignore

		echo '<ul>';
		foreach ( $this->exports[ $prefix ] as $index => $file_name ) {

			$gui_filename = str_replace( "export-{$prefix}-", '', $file_name );
			$gui_filename = str_replace( '.csv', '', $gui_filename );
			$gui_filename = ucwords( str_replace( '-', ' ', $gui_filename ) );

			echo '<li>';
			echo "<a href='javascript: void(0);' data-path='{$path}' data-filename='{$file_name}' class='button download-export'><i class=\"fa fa-download\"></i> {$gui_filename}</a>";
			echo '</li>';

		}
		echo '</ul>';

	}

	/**
	 * Gravity Forms Download Export.
	 */
	public static function gf_download_export() {
		check_admin_referer( 'gform_download_export' );

		$path     = base64_decode( $_GET['path'] ); // phpcs:ignore
		$filename = $_GET['filename'];
		$file     = $path . $filename;

		header( "Content-Disposition: attachment; filename=$filename" );
		header( 'Content-Type: text/csv;' );

		// TODO: Need to refactor to utilize WP_Filesystem.
		readfile( $file ); // phpcs:ignore
	}

}
