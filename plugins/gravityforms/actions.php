<?php
/**
 * BrightFire Core Gravity Forms Actions.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\GravityForms;

/**
 * Forces file download
 *
 * @see GFExport::ajax_download_export()
 */
function gf_download_legacy_export() {
	check_admin_referer( 'gform_download_legacy_export' );

	$path     = base64_decode( $_GET['path'] ); // phpcs:ignore
	$filename = $_GET['filename'];
	$file     = $path . $filename;

	header( "Content-Disposition: attachment; filename=$filename" );
	header( 'Content-Type: text/csv;' );

	// TODO: Need to refactor to utilize WP_Filesystem.
	readfile( $file ); // phpcs:ignore
}
