<?php
/**
 * BrightFire Core Callout.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Callout;

/**
 * Wrap content in a div with the class of callout
 *
 * @param array $atts Placeholder for future use.
 * @param null  $content The wrapped content.
 *
 * @return string
 */
function bf_callout( $atts, $content = null ) {

	return '<div class="callout">' . do_shortcode( $content ) . '</div>';

}
