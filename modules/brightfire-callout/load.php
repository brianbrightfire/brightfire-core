<?php
/**
 * BrightFIre Callout
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Callout;

/** Include Files */
require_once dirname( __FILE__ ) . '/shortcode-bf-callout.php';


/** Add Shortcode(s) */
add_shortcode( 'bf_callout', __NAMESPACE__ . '\bf_callout' );
