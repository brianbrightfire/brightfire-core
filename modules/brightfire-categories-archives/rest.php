<?php
/**
 * BrightFire Core Categories Archives REST API functions.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Categories_Archives;

use BrightFireCore\Content_Marketing\CM_Terms;

/**
 * REST: Loads all categories and returns as <li> List items
 * URI: /brightfire/v2/categories
 *
 * @param array $data Data.
 *
 * @return false|string
 */
function REST_load_categories( $data ) { // phpcs:ignore

	$query      = new CM_Terms();
	$categories = $query->get_terms( 'category' );

	$output[] = '<ul class="categories-list">';

	foreach ( $categories as $category ) {

		$link = site_url( "category/{$category->slug}" );

		$output[] = '<li>';
		$output[] = "<a href=\"{$link}\">{$category->name}</a>";

		if ( $data['show_count'] ) {
			$output[] = "({$category->total})";
		}

		$output[] = '</li>';

	}

	return implode( "\n", $output );
}

/**
 * Register Endpoint Routes.
 */
function REST_load_categories_route() { // phpcs:ignore
	$namespace = 'brightfire/v2';
	$route     = '/categories';
	$config    = array(
		'methods'  => 'GET',
		'callback' => __NAMESPACE__ . '\REST_load_categories',
	);
	register_rest_route( $namespace, $route, $config );
}
