<?php
/**
 * BrightFire Category Archives Shortcode.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Categories_Archives;

/**
 * BF Archives Shortcode.
 *
 * @param array $atts Attributes.
 * @return string
 */
function bf_archives( $atts ) {

	$instance = shortcode_atts(
		array(
			'title'           => 'Archives',
			'format'          => 'html',
			'show_post_count' => false,
			'list_by'         => 'yearly',
		),
		$atts,
		'bf_archives'
	);

	$output = '';

	$output .= ( 'option' === $instance['format'] ? '<select onchange="javascript: document.location.href=this.options[this.selectedIndex].value;">' : '<ul class="archives-list">' );
	$output .= ( 'option' === $instance['format'] ? '<option>Select</option>' : '' );

	$args    = array(
		'type'            => $instance['list_by'],
		'limit'           => 10,
		'format'          => $instance['format'],
		'show_post_count' => $instance['show_post_count'],
		'echo'            => false,
		'order'           => 'DESC',
		'post_type'       => 'post',
	);
	$output .= wp_get_archives( $args );

	$output .= ( 'option' === $instance['format'] ? '</select>' : '</ul>' );

	return $output;

}
