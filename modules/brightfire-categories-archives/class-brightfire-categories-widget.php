<?php
/**
 * BrightFire Categories Widget.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Categories_Archives;

/**
 * Class Brightfire_Categories_Widget
 */
class BrightFire_Categories_Widget extends \WP_Widget {

	/**
	 * Defaults.
	 *
	 * @var array $defaults
	 */
	public $defaults;

	/**
	 * Brightfire_Categories_Widget constructor.
	 */
	public function __construct() {
		$this->defaults = array(
			'title'      => 'Categories',
			'show_count' => false,
		);
		$widget_ops     = array(
			'classname'                   => 'brightfire-categories-widget',
			'description'                 => __( 'Categories Widget Display' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct( 'categories', __( 'BrightFire Categories' ), $widget_ops );
	}

	/**
	 * Outputs our widget instance
	 *
	 * @param array $args Arguments.
	 * @param array $instance Instance.
	 */
	public function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );
		$title    = trim( $instance['title'] );

		echo $args['before_widget'];
		echo ( ! empty( $title ) ? $args['before_title'] . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $args['after_title'] : '' );

		echo bf_categories( $instance );

		echo $args['after_widget'];

	}

	/**
	 * Displays our widget admin interface
	 *
	 * @param array $instance Instance.
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		$fields = array(
			'title'      => array(
				'type'  => 'text',
				'label' => 'Title',
			),
			'show_count' => array(
				'type'    => 'selectize',
				'label'   => 'Show Post Count:',
				'choices' => 'yesno',
			),
		);

		// Build our widget form.
		$args = array(
			'fields'   => $fields,
			'instance' => $instance,
			'widget'   => $this,
			'display'  => 'basic',
			'echo'     => true,
		);

		global $bf_admin_api;
		$bf_admin_api->fields->build( $args );

	}

	/**
	 * Save the widget instance
	 *
	 * @param array $new_instance New Instance.
	 * @param array $old_instance Old Instance.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['title'] = $new_instance['title'];

		$instance['show_count'] = ( ! empty( $new_instance['show_count'] ) ) ? true : false;

		return $instance;
	}

	/**
	 * Get an admin URL that plays nicely with domain mapping
	 *
	 * @param string $path Path.
	 *
	 * @return string|void
	 */
	public function wp_admin_ajax_url( $path = '' ) {
		if ( is_admin() ) {
			$url = admin_url( 'admin-ajax.php' );
		} else {
			$url = home_url( 'wp-admin/admin-ajax.php' );
		}

		$url .= ltrim( $path, '/' );

		return $url;
	}

}
