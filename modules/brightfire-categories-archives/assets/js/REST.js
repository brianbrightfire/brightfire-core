/** REST_event_listeners.js **/
var $ = jQuery.noConflict();

/**
 * jQuery Ready Statement
 */
jQuery(document).ready(function() {
    REST_listener_load_categories();
});

/**
 * Create an Event Listener for Categories Widget to use REST API
 */
function REST_listener_load_categories() {

    $(document).on('click','.bf-rest-load-categories',function() {

        $(this).replaceWith('Loading...');

        var data = {
            'show_count' : $(this).attr('data-show-count')
        };

        // Run the call
        $.get( RESTurl + '/categories', data, function(response) {
            $('ul.categories-list').html(response);
        });

        $(this).remove();

    });

}