<?php
/**
 * BrightFire Archives Widget.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Categories_Archives;

use function BrightFireCore\prettyprintr;

/**
 * Class Brightfire_Archives_Widget
 */
class BrightFire_Archives_Widget extends \WP_Widget {

	/**
	 * Defaults.
	 *
	 * @var array $defaults
	 */
	private $defaults;

	/**
	 * Brightfire_Archives_Widget constructor.
	 */
	public function __construct() {
		$this->defaults = array(
			'title'           => 'Archives',
			'format'          => 'html',
			'show_post_count' => false,
			'list_by'         => 'yearly',
		);
		$widget_ops     = array(
			'classname'                   => 'brightfire-archives-widget',
			'description'                 => __( 'Archives Widget Display' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct( 'archives', __( 'BrightFire Archives' ), $widget_ops );
	}

	/**
	 * Outputs our widget instance
	 *
	 * @param array $args Arguments.
	 * @param array $instance Instance.
	 */
	public function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );
		$title    = trim( $instance['title'] );

		echo $args['before_widget'];
		echo ( ! empty( $title ) ? $args['before_title'] . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $args['after_title'] : '' );

		echo bf_archives( $instance );

		echo $args['after_widget'];

	}

	/**
	 * Displays our widget admin interface
	 *
	 * @param array $instance Instance.
	 */
	public function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		$fields = array(
			'title'           => array(
				'type'  => 'text',
				'label' => 'Title',
			),
			'list_by'         => array(
				'type'    => 'selectize',
				'label'   => 'List By:',
				'choices' => array(
					'yearly'  => 'Yearly',
					'monthly' => 'Monthly',
				),
			),
			'show_post_count' => array(
				'type'    => 'selectize',
				'label'   => 'Show Post Count',
				'choices' => 'yesno',
			),
			'format'          => array(
				'type'    => 'selectize',
				'label'   => 'Display as Dropdown',
				'choices' => 'yesno',
			),
		);

		// Build our widget form.
		$args = array(
			'fields'   => $fields,
			'instance' => $instance,
			'widget'   => $this,
			'display'  => 'basic',
			'echo'     => true,
		);

		global $bf_admin_api;
		$bf_admin_api->fields->build( $args );

	}

	/**
	 * Save the widget instance
	 *
	 * @param array $new_instance New Instance.
	 * @param array $old_instance Old Instance.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['title']   = $new_instance['title'];
		$instance['list_by'] = $new_instance['list_by'];

		$instance['show_post_count'] = ( ! empty( $new_instance['show_post_count'] ) ) ? true : false;
		$instance['format']          = ( ! empty( $new_instance['format'] ) ) ? 'option' : 'html';

		return $instance;
	}

}
