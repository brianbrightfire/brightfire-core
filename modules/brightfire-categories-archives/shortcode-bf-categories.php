<?php
/**
 * BrightFire BF Categories Shortcode.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Categories_Archives;

use BrightFireCore\Content_Marketing\CM_Terms;

/**
 * BF Categories Shortcode.
 *
 * @param array $atts Attributes.
 * @return string
 */
function bf_categories( $atts ) {

	$instance = shortcode_atts(
		array(
			'title'      => 'Categories',
			'show_count' => false,
		),
		$atts,
		'bf_categories'
	);

	$query      = new CM_Terms( 10 );
	$categories = $query->get_terms( 'category' );

	$output[] = '<ul class="categories-list">';

	foreach ( $categories as $category ) {

		$link = site_url( "category/{$category->slug}" );

		$output[] = '<li>';
		$output[] = "<a href=\"{$link}\">{$category->name}</a>";

		if ( $instance['show_count'] ) {
			$output[] = "({$category->total})";
		}

		$output[] = '</li>';

	}

	$output[] = "<li style=\"text-align: center;\"><a href=\"javascript: void(0);\" class=\"bf-rest-load-categories\" data-show-count=\"{$instance['show_count']}\">View All</a></li>";

	$output[] = '</ul>';

	// Local Site REST Endpoint.
	$output[] = '<script type="text/javascript">';
	$output[] = 'var RESTurl = "' . site_url() . '/wp-json/brightfire/v2";';
	$output[] = '</script>';

	return implode( "\n", $output );

}
