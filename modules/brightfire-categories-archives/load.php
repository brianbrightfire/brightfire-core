<?php
/**
 * BrightFire Core Categories Archives.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Categories_Archives;

/** --------------------------------------------------------------------- */
/** Include Files                                                         */
/** --------------------------------------------------------------------- */
require_once dirname( __FILE__ ) . '/class-brightfire-archives-widget.php';
require_once dirname( __FILE__ ) . '/shortcode-bf-archives.php';
require_once dirname( __FILE__ ) . '/class-brightfire-categories-widget.php';
require_once dirname( __FILE__ ) . '/shortcode-bf-categories.php';
require_once dirname( __FILE__ ) . '/rest.php';


/** --------------------------------------------------------------------- */
/** Register Widgets                                                      */
/** --------------------------------------------------------------------- */

/**
 * Register BF Archives Widget.
 */
function register_bf_archives() {
	unregister_widget( 'WP_Widget_Archives' );
	register_widget( __NAMESPACE__ . '\BrightFire_Archives_Widget' );
}
add_action( 'widgets_init', __NAMESPACE__ . '\register_bf_archives' );


/**
 * Register BF Categories Widget.
 */
function register_bf_categories() {
	unregister_widget( 'WP_Widget_Categories' );
	register_widget( __NAMESPACE__ . '\BrightFire_Categories_Widget' );
}
add_action( 'widgets_init', __NAMESPACE__ . '\register_bf_categories' );


/** --------------------------------------------------------------------- */
/** Add Shortcode(s)                                                      */
/** --------------------------------------------------------------------- */
add_shortcode( 'bf_archives', __NAMESPACE__ . '\bf_archives' );
add_shortcode( 'bf_categories', __NAMESPACE__ . '\bf_categories' );


/** --------------------------------------------------------------------- */
/** Custom REST Routes                                                    */
/** --------------------------------------------------------------------- */
add_action( 'rest_api_init', __NAMESPACE__ . '\REST_load_categories_route' );

/** --------------------------------------------------------------------- */
/** SASS Registration                                                     */
/** --------------------------------------------------------------------- */
global $bf_sass;
$bf_sass->register_path( 'bf-core', dirname( __FILE__ ) . '/assets/_brightfire_categories_archives_widget.scss' );
