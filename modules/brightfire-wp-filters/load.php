<?php
/**
 * BrightFire WP Filters.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\WP_Content_Filters;

/** Require Files */
require_once dirname( __FILE__ ) . '/filters.php';

/**
 * Filters the title and allows bf_ins shortcodes to be parsed
 *
 * @uses allow_bf_shortcodes_in_titles
 */
add_filter( 'the_title', 'do_shortcode' );

/**
 * Allow shortcodes in Text Widget
 *
 * @uses do_shortcode
 */
add_filter( 'widget_text', 'do_shortcode' );

/**
 * Allow shortcodes in widget titles
 *
 * @uses do_shortcode
 */
add_filter( 'widget_title', 'do_shortcode' );

/**
 * Customize the Author link
 */
add_filter( 'author_link', __NAMESPACE__ . '\bf_custom_author_uri' );

/**
 * Filter Caption shortcode attributes to enable the [shortcode] shortcode inside caption text
 *
 * @uses filter_shortcode_atts_caption()
 */
add_filter( 'shortcode_atts_caption', __NAMESPACE__ . '\filter_shortcode_atts_caption', 10, 3 );

/**
 * Excerpts the post if in the loop or ERIE post
 *
 * @uses excerpt_the_content
 */
add_filter( 'the_content', __NAMESPACE__ . '\excerpt_the_content', 999 );

/**
 * Adds SMS to WP allowed protocols
 *
 * @user custom_allowed_protocols
 */
add_filter( 'kses_allowed_protocols', __NAMESPACE__ . '\custom_allowed_protocols' );
