<?php
/**
 * BrightFire WP Filters.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\WP_Content_Filters;

/**
 * Replace Author URL with the site_url
 *
 * @param string $author_uri Author URI.
 *
 * @return string
 */
function bf_custom_author_uri( $author_uri ) {
	return site_url();
}

/**
 * Filter Caption shortcode attributes to enable the [shortcode] shortcode inside caption text
 *
 * WP doesn't run do_shortcode on the 'caption' text value parsed out of [caption], which means
 * the [shortcode] shortcode doesn't work.
 *
 * @param array $out atts array as determined by WP to be returned after filtering.
 * @param array $pairs Pairs.
 * @param array $atts Atts.
 * @return array
 */
function filter_shortcode_atts_caption( $out, $pairs, $atts ) {
	// OPTIONAL: Look for a specific shortcode before running do_shortcode
	// if (has_shortcode($out['caption'], 'shortcode_to_look_for')).
	$out['caption'] = do_shortcode( $out['caption'] );

	return $out;
}

/**
 * Truncates the content and adds featured image.
 *
 * TODO: Revisit where we are calling this; See shortcode-bf-recent-blog-posts.php;
 *
 * @param string $content Content.
 * @param string $image_align Image Align.
 *
 * @uses update_excerpt_more_link
 *
 * @adds bf_loop_featured_image_src filter
 * @adds bf_loop_featured_image_link filter
 * @adds bf_read_more_link filter
 *
 * @return string
 */
function excerpt_the_content( $content, $image_align = '' ) {

	// Bail if it's not a post, not in the loop, or we're dealing with single page or feed.
	if ( 'post' !== get_post_type() || ! in_the_loop() || is_feed() || is_single() ) {
		return $content;
	}

	// Remove image captions.
	add_filter(
		'img_caption_shortcode',
		( function() {
			return '';
		} ),
		10,
		2
	);

	// Parse shortcodes here so we can extract the content properly.
	$content = do_shortcode( $content );

	$max_length      = 65;
	$featured_image  = '';
	$trimmed_content = '';
	$image_url       = '';
	$link            = get_permalink();
	$link_target     = \BrightFireCore\is_external_link( $link ) ? 'target="_blank"' : '';
	$link_icon       = \BrightFireCore\is_external_link( $link ) ? '<i class="glyphicons new_window"></i>' : '';

	// Use the featured image or get an image from the content.
	if ( has_post_thumbnail() ) {
		$image_attachment = wp_get_attachment_image_src( get_post_thumbnail_id(), array( 500, 500 ) );
		$image_url        = $image_attachment[0];
	} else {
		$image_url = \BrightFireCore\extract_image_url_from_html( $image_url, $content );
	}

	// Allow filtering of the featured image to be used in the excerpt.
	$image_url = apply_filters( 'bf_loop_featured_image_src', $image_url, $content );

	if ( ! empty( $image_url ) && ! is_feed() ) {
		// phpcs:ignore
		$featured_image = apply_filters( 'bf_loop_featured_image_link', sprintf( '<a href="%s" %s class="bf-loop-featured-image %s" style="background-image: url(%s);"></a>', $link, $link_target, $image_align, $image_url ) );
	} elseif ( ! empty( $image_url ) && is_feed() ) {
		$featured_image = apply_filters( 'bf_loop_featured_image_link', sprintf( '<img src="%s" />', $image_url ) );
	}

	// Filter to allow us to change the read more link.
	$post_link = apply_filters( 'bf_read_more_link', '... <a class="read-more" title="Read Full Article" href="' . $link . '"  ' . $link_target . '>Read&nbsp;Article' . $link_icon . '</a>' );

	// Truncate content if over $max_length otherwise use the entire content and include read more links for either.
	if ( str_word_count( wp_strip_all_tags( $content ) ) > $max_length ) {

		$trimmed_content = wp_trim_words( $content, $max_length, $post_link );

	} else {

		$trimmed_content = wp_strip_all_tags( $content ) . $post_link;

	}

	return $featured_image . $trimmed_content;
}

/**
 * Adds custom protocols to WPs allowed protocols via filter
 *
 * @param array $protocols Protocols.
 *
 * @return array
 */
function custom_allowed_protocols( $protocols ) {

	$custom = array(
		'sms',
	);

	$protocols_new = array_merge( $protocols, $custom );

	return $protocols_new;

}
