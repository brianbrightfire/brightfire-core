<?php
/**
 * BrightFire Recent Blog Posts.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Recent_Blog_Posts;

use BrightFireCore\Content_Marketing\CM_Post_Formatter;
use BrightFireCore\Content_Marketing\CM_Posts;


/**
 * Recent blog post with title/link/image/excerpt
 *
 * @param array $atts Attributes.
 *          limit: the quantity of posts to display.
 *          category: a specific category of posts ot display.
 *          image_align: the alignment of the image if post has one.
 *
 * @return string
 */
function bf_recent_blog_posts( $atts ) {

	$atts = shortcode_atts(
		array(
			'limit'       => 3,
			'offset'      => 0,
			'category'    => 0,
			'tag'         => 0,
			'image_align' => 'right',
			'paged'       => false,
			'year'        => '',
			'arrangement' => array( 'title', 'info', 'image', 'excerpt' ),
		),
		$atts,
		'bf_recent_blog_posts'
	);

	// Image align classes.
	switch ( $atts['image_align'] ) {
		case 'right':
			$image_align = 'alignright';
			break;
		case 'left':
			$image_align = 'alignleft';
			break;
		case 'center':
			$image_align = 'aligncenter';
			break;
		case 'none':
			$image_align = 'alignnone';
	}

	// Sort for arrangement.
	$arrangement = apply_filters( 'bf_recent_blog_posts_output_arrangement', $atts['arrangement'] );

	// Create array keys from filtered arrangement.
	if ( ! is_array( $arrangement ) ) {
		$arrangement = explode( ',', $arrangement );
	}

	/**
	 * Begin output
	 */
	// Open main container.
	$output = '<div class="recent-blog-posts ' . $atts['image_align'] . '">';

	$term_name = false;

	// Category Filter (Local Cats).
	if ( 0 !== (int) $atts['category'] && is_integer( (int) $atts['category'] ) ) {

		$category  = get_category( (int) $atts['category'] );
		$term_name = $category->slug;

	}

	// Tag Filter (Local Tags).
	if ( 0 !== (int) $atts['tag'] && is_integer( (int) $atts['tag'] ) ) {

		$tag       = get_tag( $atts['tag'] );
		$term_name = $tag->slug;

	}

	// Arguments for getting the posts.
	$query_args = array(
		'posts_per_page' => (int) $atts['limit'],
		'offset'         => (int) $atts['offset'],
		'term_name'      => (string) $term_name,
	);

	// Query.
	$query   = new CM_Posts( $query_args );
	$results = $query->get_posts();

	if ( 0 === count( $results ) ) {
		return 'No posts found.';
	}

	// The Loop.
	foreach ( $results as $result ) {

		$item_output = array();

		/** Globals for the Loop */
		$link = CM_Post_Formatter::link( $result );

		/**
		 * Post Title
		 */
		$title = apply_filters( 'bf_recent_blog_posts_title', '<h3><a href="' . $link . '">' . CM_Post_Formatter::title( $result ) . '</a></h3>' );

		/**
		 * Post Info
		 */
		$info = apply_filters( 'bf_recent_blog_posts_info', '<p class="post-info">Posted: ' . CM_Post_Formatter::date( $result ) . '</p>' );

		/**
		 * Post Excerpt
		 */
		$excerpt = apply_filters( 'bf_recent_blog_posts_content', CM_Post_Formatter::excerpt( $result ) );

		/**
		 * Post Image
		 */
		$featured_image  = '';
		$has_image_class = '';
		$image_url       = '';

		$image_src = apply_filters( 'bf_recent_blog_posts_image', CM_Post_Formatter::image_src( $result ) );

		if ( ! empty( $image_src ) && ! is_feed() && in_array( 'image', $arrangement, true ) ) {
			$featured_image  = apply_filters( 'bf_loop_featured_image_link', sprintf( '<a href="%s" class="bf-loop-featured-image %s" style="background-image: url(\'%s\');"></a>', $link, $image_align, $image_src ) );
			$has_image_class = 'bf-loop-has-featured-image';
		} elseif ( ! empty( $image_src ) && is_feed() ) {
			$featured_image = apply_filters( 'bf_loop_featured_image_link', sprintf( '<img src="%s" />', $image_src ) );
		}

		$image = apply_filters( 'bf_recent_blog_posts_image', $featured_image );

		/**
		 * Loop Item Output
		 */
		$item_output[] = '<div class="' . $has_image_class . ' post post-' . $result->ID . '">';

		// Place each item based on arrangement.
		foreach ( $arrangement as $item ) {
			$item_output[] = $$item;
		}

		$item_output[] = '<div style="clear:both;"></div></div>';

		$output .= implode( "\n", $item_output );

	}

	// Close main container.
	$output .= '</div>';

	if ( $atts['paged'] ) {
		$output .= $query->get_pagination();
	}

	return $output;
}
