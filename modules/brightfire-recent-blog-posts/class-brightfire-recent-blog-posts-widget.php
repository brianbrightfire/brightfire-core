<?php
/**
 * BrightFire Recent Blog Posts.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Recent_Blog_Posts;

/**
 * BrightFire Recent Blog Posts widget class
 */
class BrightFire_Recent_Blog_Posts_Widget extends \WP_Widget {

	/**
	 * Defaults.
	 *
	 * @var array $defaults
	 */
	private $defaults;

	/**
	 * BrightFire_Recent_Blog_Posts_Widget constructor.
	 */
	public function __construct() {

		$this->defaults = array(
			'title'       => '',
			'category'    => '0',
			'limit'       => '3',
			'offset'      => '0',
			'arrangement' => array( 'title', 'info', 'image', 'excerpt' ),
			'image_align' => 'right',
		);

		$widget_ops = array(
			'classname'                   => 'bf-recent-blog-posts',
			'description'                 => __( 'Show recent blog posts.' ),
			'customize_selective_refresh' => true,
		);

		parent::__construct( 'brightfire_recent_posts', __( 'BrightFire Recent Blog Posts' ), $widget_ops );
	}

	/**
	 * Front end display.
	 *
	 * @param array $args Arguments.
	 * @param array $instance Instance.
	 */
	public function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		// Default is instance title.
		$title = trim( $instance['title'] );

		$title = apply_filters( 'recent_blog_posts_widget_title', $title );

		if ( ! empty( $title ) ) {
			$title = $args['before_title'] . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $args['after_title'];
		}

		// Apply Filters and Print Widget.
		echo $args['before_widget'];

		echo $title;

		echo bf_recent_blog_posts( $instance );

		echo $args['after_widget'];

	}

	/**
	 * Admin display.
	 *
	 * @param array $instance Instance.
	 * @return string|void
	 */
	public function form( $instance ) {
		// * Merge with defaults.
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		$categories       = get_categories();
		$category_choices = array();

		foreach ( $categories as $category ) {
			$category_choices[ $category->term_id ] = $category->name;
		}

		$category_choices = array( '0' => 'All Categories' ) + $category_choices;

		$fields = array(
			'title'       => array(
				'type'  => 'text',
				'label' => 'Title',
			),
			'category'    => array(
				'type'    => 'selectize',
				'label'   => 'Category',
				'choices' => $category_choices,
			),
			'limit'       => array(
				'type'          => 'number',
				'label'         => 'Number of Posts',
				'default_value' => 3,
			),
			'offset'      => array(
				'type'          => 'number',
				'label'         => 'Offset',
				'default_value' => 0,
			),
			'image_align' => array(
				'type'    => 'selectize',
				'label'   => 'Image Alignment',
				'choices' => array(
					'none'   => 'None',
					'right'  => 'Right',
					'left'   => 'Left',
					'center' => 'Center',
				),
			),
			'arrangement' => array(
				'type'    => 'selectize',
				'label'   => 'Arrangement',
				'choices' => array(
					'title'   => 'Title',
					'info'    => 'Info',
					'image'   => 'Image',
					'excerpt' => 'Excerpt',
				),
				'atts'    => array(
					'multiple' => 'multiple',
				),
			),
		);

		// Build our widget form.
		$args = array(
			'fields'   => $fields,
			'instance' => $instance,
			'widget'   => $this,
			'display'  => 'basic',
			'echo'     => true,
		);

		global $bf_admin_api;
		$bf_admin_api->fields->build( $args );

	}
}
