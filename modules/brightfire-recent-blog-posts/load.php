<?php
/**
 * BrightFire Recent Blog Posts.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Recent_Blog_Posts;

/** Include Files */
require_once dirname( __FILE__ ) . '/class-brightfire-recent-blog-posts-widget.php';
require_once dirname( __FILE__ ) . '/shortcode-bf-recent-blog-posts.php';


/**
 * Register BrightFire_Footer_Credit_Widget
 */
function register_bf_recent_blog_posts() {
	register_widget( __NAMESPACE__ . '\BrightFire_Recent_Blog_Posts_Widget' );
}
add_action( 'widgets_init', __NAMESPACE__ . '\register_bf_recent_blog_posts' );

/** Add Shortcode(s) */
add_shortcode( 'bf_recent_blog_posts', __NAMESPACE__ . '\bf_recent_blog_posts' );

global $bf_sass;
$bf_sass->register_path( 'bf-core', dirname( __FILE__ ) . '/_brightfire_recent_blog_posts.scss' );
