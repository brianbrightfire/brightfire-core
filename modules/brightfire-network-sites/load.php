<?php
/**
 * BrightFIre Network Sites.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Network_Sites;

require_once dirname( __FILE__ ) . '/class-network-sites-table.php';

/** Remove Featured Image for bf_editors */

add_action( 'init', array( __NAMESPACE__ . '\\Network_Sites_Table', 'init' ) );
