<?php
/**
 * BrightFire Network Sites Table.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Network_Sites;

/**
 * Class Network_Sites_Table
 *
 * @package BrightFireCore\Network_Sites
 */
class Network_Sites_Table {

	/**
	 * Callback to start things - instantiates the class
	 */
	public static function init() {
		$class = __CLASS__;
		if ( empty( $GLOBALS[ $class ] ) ) {
			$GLOBALS[ $class ] = new $class();
		}
	}

	/**
	 * Network_Sites_Table constructor.
	 */
	public function __construct() {
		add_filter( 'wpmu_blogs_columns', array( $this, 'get_id' ) );
		add_action( 'manage_sites_custom_column', array( $this, 'add_columns' ), 10, 2 );
		add_action( 'manage_blogs_custom_column', array( $this, 'add_columns' ), 10, 2 );
		add_action( 'admin_footer', array( $this, 'add_style' ) );

		// Remove WPEngine's Blog ID Column - SO we have no conflicts.
		remove_filter( 'wpmu_blogs_columns', 'wpe_site_id' );
		remove_action( 'manage_sites_custom_column', 'wpe_site_id_columns', 10 );
		remove_action( 'manage_blogs_custom_column', 'wpe_site_id_columns', 10 );
	}

	/**
	 * Adds column content
	 *
	 * @param string  $column_name Column Name.
	 * @param integer $blog_id Blog ID.
	 * @return mixed
	 */
	public function add_columns( $column_name, $blog_id ) {

		if ( 'site_id' === $column_name ) {

			echo $blog_id;

		} elseif ( 'theme' === $column_name ) {

			switch_to_blog( $blog_id );
			$theme = wp_get_theme();
			echo $theme->get( 'Name' );
			restore_current_blog();

		}
		return $column_name;
	}

	/**
	 * Add column headers
	 *
	 * @param array $columns Columns.
	 * @return mixed
	 */
	public function get_id( $columns ) {

		$columns['site_id'] = __( 'ID' );
		$columns['theme']   = __( 'Theme' );

		return $columns;
	}

	/**
	 * Style for smaller col
	 */
	public function add_style() {
		echo '<style>#blog_id { width:7%; }</style>';
	}

}
