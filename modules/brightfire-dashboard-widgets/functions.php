<?php
/**
 * BrightFire Dashboard Widgets
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Dashboard_Widgets;

/**
 * Displays support link and verbiage
 */
function dashboard_support_widget() {

	echo '<style>
			#support-center-widget {
			 	text-align:center;
			 	padding: 20px;
			}
			#support-center-widget img {
			 	margin-bottom: 10px;
			}
			#support-center-widget a:hover {
			 	opacity: 0.8;
			 	transition: all 200ms;
			}
			#support-center-widget a:focus {
			 	box-shadow: none;
			}
		 </style>';

	echo '<p id="support-center-widget"><a href="http://www.brightfiresupport.com" target="_blank" title="BrightFire Support Center"><img alt="BrightFire Support Center" src="' . BF_CORE_ASSETS . 'images/bf-supportcenter.png" /></a><br>';
	echo 'For website support and maintenance requests, please visit the <a href="http://www.brightfiresupport.com" target="_blank" title="BrightFire Support Center">BrightFire Support Center</a>.</p>';
}

/**
 * Displays 3 most recent blog posts from inswebsites.com
 *
 * TODO: Should we move this feed widget to BrightFireCore and add a filter for the feed URL?
 */
function dashboard_news_widget() {

	$feed_base_url = apply_filters( 'bf_news_feed_base_url', '' );

	if ( empty( $feed_base_url ) ) {
		return false;
	}

	echo '<style>
			.brightfire-post {
			 	margin-bottom: 2em;
			 	border-bottom: 1px solid #ccc;
			}
			.postbox .inside .brightfire-post h2{
			 	margin-bottom: .5em;
			}
			.brightfire-post img{
			 	max-width: 100%;
			}
		 </style>';

	$posts_rtn = wp_remote_get( 'https://' . $feed_base_url . '/wp-json/wp/v2/posts/?per_page=3' );

	if ( ! is_array( $posts_rtn ) ) {
		echo '<p>';
		echo '<i>Failed to complete posts request.</i>';
		echo '</p>';
		return false;
	}

	$posts = json_decode( $posts_rtn['body'] );

	foreach ( $posts as $post ) {

		// Get the featured image.
		$image_rtn = wp_remote_get( 'https://' . $feed_base_url . '/wp-json/wp/v2/media/' . $post->featured_media );

		if ( is_array( $image_rtn ) ) {
			$image      = json_decode( $image_rtn['body'] );
			$image_url  = $image->media_details->sizes->full->source_url;
			$image_html = '<a href="' . $post->link . '" target="_blank"><img src="' . $image_url . '" /></a>';
		} else {
			$image_html = '';
		}

		echo '<div class="brightfire-post">';
		echo '<h2><a href="' . $post->link . '" target="_blank">' . $post->title->rendered . '</a></h2>';
		echo '<p>Posted: ' . date( 'l, F d, Y', strtotime( $post->date ) ) . '</p>';
		echo $image_html;
		echo preg_replace( '/<a(.*?)>/', '<a$1 target="_blank">', $post->excerpt->rendered );
		echo '</div>';
	}
}
