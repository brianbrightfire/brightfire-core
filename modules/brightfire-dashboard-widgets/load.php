<?php
/**
 * BrightFire Dashboard Widgets
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Dashboard_Widgets;

/** Require Files */
require_once dirname( __FILE__ ) . '/functions.php';
require_once dirname( __FILE__ ) . '/actions.php';

/**
 * Adds BrightFire dashboard widgets
 *
 * @uses add_dashboard_widgets()
 */
add_action( 'wp_dashboard_setup', __NAMESPACE__ . '\add_dashboard_widgets' );
