<?php
/**
 * BrightFire Dashboard Widgets
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Dashboard_Widgets;

/**
 * Adds BrightFire widgets to dashboard and sorts them to default on the top
 */
function add_dashboard_widgets() {
	wp_add_dashboard_widget( 'dashboard_support_widget', 'BrightFire Support', __NAMESPACE__ . '\dashboard_support_widget' );
	wp_add_dashboard_widget( 'dashboard_news_widget', 'BrightFire News', __NAMESPACE__ . '\dashboard_news_widget' );

	// By default we want the our dashboard widgets on top.
	global $wp_meta_boxes;

	$dashboard = $wp_meta_boxes['dashboard']['normal']['core'];

	$top_widgets = array(
		'dashboard_support_widget' => $dashboard['dashboard_support_widget'],
		'dashboard_news_widget'    => $dashboard['dashboard_news_widget'],
	);
	unset( $dashboard['dashboard_support_widget'], $dashboard['dashboard_support_widget'] );

	$sorted_dashboard = array_merge( $top_widgets, $dashboard );

	$wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard; // phpcs:ignore
}
