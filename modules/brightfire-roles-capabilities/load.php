<?php
/**
 * BrightFire Roles Capabilities.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Unfiltered_Html;

/** Require Files */
require_once dirname( __FILE__ ) . '/functions.php';

add_filter( 'map_meta_cap', __NAMESPACE__ . '\um_unfilter_multisite', 10, 4 );
