<?php
/**
 * BrightFire Roles Capabilities.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Unfiltered_Html;

/**
 * Filters meta caps
 *
 * @param array   $caps Caps.
 * @param string  $cap Cap.
 * @param integer $user_id User ID.
 * @param array   $args Args.
 *
 * @return array
 */
function um_unfilter_multisite( $caps, $cap, $user_id, $args ) {
	if ( 'unfiltered_html' === $cap ) {

		if ( user_can( $user_id, 'bf_editor' ) || user_can( $user_id, 'administrator' ) ) {
			$caps = array( 'unfiltered_html' );
		}
	}
	return $caps;
}
