<?php
/**
 * BrightFire RSS.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\RSS;

/** Include Files */
require_once dirname( __FILE__ ) . '/shortcode-rsstag.php';


/** Add Shortcode(s) */
add_shortcode( 'rsstag', __NAMESPACE__ . '\rsstag' );
