<?php
/**
 * BrightFire RSS.
 *
 * @package BrightFireCore
 */

/**
 * A shortcode representation of the RSS widget
 *
 * @param array $atts Attributes.
 *
 * @return string
 */
function rsstag( $atts ) {
	$atts = shortcode_atts(
		array(
			'url'          => get_bloginfo( 'rss2_url' ),
			'items'        => '5',
			'show_author'  => 0,
			'show_date'    => 0,
			'show_summary' => 0,
		),
		$atts
	);

	ob_start();

	wp_widget_rss_output( $atts['url'], $atts );
	$rss = ob_get_contents();

	ob_end_clean();

	return $rss;
}
