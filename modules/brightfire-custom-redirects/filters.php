<?php
/**
 * BrightFire Custom Redirects.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Custom_Redirects;

/**
 * Redirects URL's based on custom criteria
 */
function custom_redirects() {
	global $post;

	// Redirect Image URLs appropriately
	// IF image is attached to a post, redirect to post
	// Default redirect to home page.
	if ( $post && wp_attachment_is_image( $post->ID ) ) {

		if ( $post && $post->post_parent ) {
			wp_safe_redirect( get_permalink( $post->post_parent ), 301 );
			exit();
		} else {
			wp_safe_redirect( home_url(), 301 );
			exit();
		}
	}

	// Allow an action for any custom redirections from other plugins
	// or vertical libraries; send global $post object.
	do_action( 'bf_custom_redirects', $post );

}
