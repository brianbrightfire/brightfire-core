<?php
/**
 * BrightFire Custom Redirects.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Custom_Redirects;

/** Require Files */
require_once dirname( __FILE__ ) . '/filters.php';

/**
 * Redirects URL's based on custom criteria
 *
 * @uses custom_redirects
 */
add_filter( 'template_redirect', __NAMESPACE__ . '\custom_redirects' );
