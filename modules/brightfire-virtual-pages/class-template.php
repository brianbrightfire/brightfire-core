<?php
// phpcs:ignorefile
/**
 * BrightFire Virtual Pages.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BrightFire_Virtual_Pages;

interface TemplateLoaderInterface {

	/**
	 * Setup loader for a page objects
	 *
	 * @param PageInterface $page Page.
	 *
	 * @return mixed
	 */
	public function init( PageInterface $page );

	/**
	 * Trigger core and custom hooks to filter templates,
	 * then load the found template.
	 */
	public function load();
}

class TemplateLoader implements TemplateLoaderInterface {

	public function init( PageInterface $page ) {
		$this->templates = wp_parse_args(
			array( 'page.php', 'index.php' ),
			(array) $page->getTemplate()
		);
	}

	public function load() {
		do_action( 'template_redirect' );
		$template = locate_template( array_filter( $this->templates ) );
		$filtered = apply_filters(
			'template_include',
			apply_filters( 'virtual_page_template', $template )
		);
		if ( empty( $filtered ) || file_exists( $filtered ) ) {
			$template = $filtered;
		}
		if ( ! empty( $template ) && file_exists( $template ) ) {
			require_once $template;
		}
	}
}
