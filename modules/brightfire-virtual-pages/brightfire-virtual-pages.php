<?php
/**
 * BrightFire Virtual Pages.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BrightFire_Virtual_Pages;

define( 'BF_VIRTUAL_PAGES', dirname( __FILE__ ) . '/' );

require_once BF_VIRTUAL_PAGES . 'class-page.php';
require_once BF_VIRTUAL_PAGES . 'class-controller.php';
require_once BF_VIRTUAL_PAGES . 'class-template.php';

$virtual_page_controller = new Controller( new TemplateLoader() );

add_filter( 'do_parse_request', array( $virtual_page_controller, 'dispatch' ), PHP_INT_MAX, 2 );

add_action(
	'loop_end',
	function( \WP_Query $query ) {
		if ( isset( $query->virtual_page ) && ! empty( $query->virtual_page ) ) {
			$query->virtual_page = null;
		}
	}
);

add_filter(
	'the_permalink',
	function( $plink ) {
		global $post, $wp_query;
		if (
		$wp_query->is_page
		&& isset( $wp_query->virtual_page )
		&& $wp_query->virtual_page instanceof Page
		&& isset( $post->is_virtual )
		&& $post->is_virtual
		) {
			$plink = home_url( $wp_query->virtual_page->getUrl() );
		}
		return $plink;
	}
);

// phpcs:disable

/* Examples */
// require_once BF_VIRTUAL_PAGES . 'example.php';
