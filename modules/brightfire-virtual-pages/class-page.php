<?php
// phpcs:ignorefile

/**
 * BrightFire Virtual Pages.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BrightFire_Virtual_Pages;

interface PageInterface {

	public function getUrl();

	public function getTemplate();

	public function getTitle();

	public function setTitle( $title );

	public function setContent( $content );

	public function setTemplate( $template );

	public function setPostType( $post_type );

	/**
	 * Get a WP_Post build using virtual Page object
	 *
	 * @return \WP_Post
	 */
	public function asWpPost();
}

class Page implements PageInterface {

	private $url;
	private $title;
	private $content;
	private $template;
	private $post_type;
	private $wp_post;

	/**
	 * Page constructor.
	 *
	 * @param string $url
	 * @param array  $return
	 */
	public function __construct( $url, $args = array() ) {

		$args = wp_parse_args(
			$args,
			array(
				'title'     => 'Untitled',
				'content'   => '',
				'post_type' => 'post',
				'template'  => 'page.php',
			)
		);

		$this->url = filter_var( $url, FILTER_SANITIZE_URL );
		$this->setTitle( $args['title'] );
		$this->setTemplate( $args['template'] );
		$this->setPostType( $args['post_type'] );
		$this->setContent( $args['content'] );
	}

	public function getUrl() {
		return $this->url;
	}

	public function getTemplate() {
		return $this->template;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle( $title ) {
		$this->title = filter_var( $title, FILTER_SANITIZE_STRING );
		return $this;
	}

	public function setContent( $content ) {
		$this->content = $content;
		return $this;
	}

	public function setTemplate( $template ) {
		$this->template = $template;
		return $this;
	}

	public function setPostType( $post_type ) {
		$this->post_type = $post_type;
		return $this;
	}

	public function asWpPost() {
		if ( is_null( $this->wp_post ) ) {
			$post          = array(
				'ID'             => 0, // Positive integers make YOAST break due to @WPSEO_Frontend_Page_Type\is_simple_page method
				'post_title'     => $this->title,
				'post_name'      => sanitize_title( $this->title ),
				'post_content'   => $this->content,
				'post_excerpt'   => '',
				'post_parent'    => 0,
				'menu_order'     => 0,
				'post_type'      => $this->post_type,
				'post_status'    => 'publish',
				'comment_status' => 'closed',
				'ping_status'    => 'closed',
				'comment_count'  => 0,
				'post_password'  => '',
				'to_ping'        => '',
				'pinged'         => '',
				'guid'           => home_url( $this->getUrl() ),
				'post_date'      => current_time( 'mysql' ),
				'post_date_gmt'  => current_time( 'mysql', 1 ),
				'post_author'    => is_user_logged_in() ? get_current_user_id() : 0,
				'is_virtual'     => true,
				'filter'         => 'raw',
			);
			$this->wp_post = new \WP_Post( (object) $post );
		}
		return $this->wp_post;
	}
}
