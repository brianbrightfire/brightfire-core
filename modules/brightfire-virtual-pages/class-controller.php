<?php
// phpcs:ignorefile
/**
 * BrightFire Virtual Pages.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BrightFire_Virtual_Pages;

interface ControllerInterface {

	/**
	 * Register a page object in the controller
	 *
	 * @param PageInterface $page
	 *
	 * @return mixed
	 */
	public function addPage( PageInterface $page );

	/**
	 * Run on 'do_parse_request' and if the request is for one of the registerd
	 * setup global variables, fire core hooks, requires page template and exit.
	 *
	 * @param boolean $bool The boolean flag value passed by 'do_parse_request'
	 * @param \WP     $wp       The global wp object passed by 'do_parse_request'
	 */
	public function dispatch( $bool, \WP $wp );

}

/**
 * Class Controller
 *
 * @package BrightFireCore\BrightFire_Virtual_Pages
 */
class Controller implements ControllerInterface {

	private $pages;
	private $rewrites;
	private $loader;
	private $matched;

	public function __construct( TemplateLoaderInterface $loader ) {
		$this->pages    = new \SplObjectStorage();
		$this->loader   = $loader;
		$this->rewrites = [];
	}

	public function addPage( PageInterface $page ) {
		$this->pages->attach( $page );
		return $page;
	}

	public function addRewrite( $pattern, $callback ) {
		$this->rewrites[ $pattern ] = $callback;
	}

	public function dispatch( $bool, \WP $wp ) {
		do_action( 'bf_virtual_pages', $this );

		$this->checkRewriteRequest();

		if ( $this->checkRequest() && $this->matched instanceof Page ) {
			$this->loader->init( $this->matched );
			$wp->virtual_page = $this->matched;
			do_action( 'parse_request', $wp );
			$this->setupQuery();
			do_action( 'wp', $wp );
			$this->loader->load();
			$this->handleExit();
		}
		return $bool;
	}

	private function checkRewriteRequest() {
		$url = trim( $this->getPathInfo(), '/' );

		// Check Current URL against patterns.
		foreach ( $this->rewrites as $pattern => $callback ) {

			// If match on our pattern set.
			if ( preg_match_all( $pattern, $url, $matches ) ) {

				// Run Callback.
				$callback_return = $this->callback_handler( $callback, $matches[1][0] );

				if ( $callback_return ) {

					$this->addPage( new Page( $url, $callback_return ) );

				} else {

					global $wp_query;
					$wp_query->set_404();
				}
			}
		}
	}

	private function checkRequest() {
		$this->pages->rewind();
		$path = trim( $this->getPathInfo(), '/' );

		while ( $this->pages->valid() ) {
			$current_path = trim( $this->pages->current()->getUrl(), '/' );

			// Check Current URL against match.
			if ( $current_path === $path ) {
				$this->matched = $this->pages->current();
				return true;
			}

			$this->pages->next();
		}
	}

	private function getPathInfo() {
		$home_path = wp_parse_url( home_url(), PHP_URL_PATH );
		return preg_replace( "#^/?{$home_path}/#", '/', add_query_arg( array() ) );
	}

	private function setupQuery() {
		global $wp_query;
		$wp_query->init();
		$wp_query->is_page        = false;
		$wp_query->is_singular    = true;
		$wp_query->is_single      = true;
		$wp_query->is_home        = false;
		$wp_query->found_posts    = 1;
		$wp_query->post_count     = 1;
		$wp_query->max_num_pages  = 1;
		$posts                    = array( $this->matched->asWpPost() );
		$post                     = $posts[0];
		$wp_query->posts          = $posts;
		$wp_query->post           = $post;
		$wp_query->queried_object = $post;
		$GLOBALS['post']          = $post; // phpcs:ignore
		$wp_query->virtual_page   = $post instanceof \WP_Post && isset( $post->is_virtual )
			? $this->matched
			: null;
	}

	private function callback_handler( $maybe_callback, $args = array() ) {

		$function = null;
		$method   = null;
		$object   = null;

		$function_exists = false;
		$method_exists   = false;

		// Is this a simple callable function?.
		if ( ! is_array( $maybe_callback ) ) {
			$function_exists = function_exists( $maybe_callback );
			$function        = $maybe_callback;
		}

		// Is this an object->method callback?.
		if ( is_array( $maybe_callback ) && array_key_exists( 0, $maybe_callback ) && array_key_exists( 1, $maybe_callback ) ) {
			$method_exists = method_exists( $maybe_callback[0], $maybe_callback[1] );
			$object        = $maybe_callback[0];
			$method        = $maybe_callback[1];
		}

		// Is a callable function - return it.
		if ( $function_exists ) {

			// If we got a string, make it an array.
			if ( ! is_array( $args ) ) {
				$args = array( $args );
			}
			return call_user_func_array( $function, $args );
		}

		// Is a callable method - return it.
		if ( $method_exists ) {
			return $object->$method( $args );
		}

		// Is a direct data structure as we expect it - return it.
		if ( ! $function_exists && ! $method_exists ) {

			if ( array_key_exists( 'title', $maybe_callback ) && array_key_exists( 'content', $maybe_callback ) ) {
				return $maybe_callback;
			}
		}

		// Did not get any expected formats - return false.
		return false;
	}

	public function handleExit() {
		exit();
	}
}
