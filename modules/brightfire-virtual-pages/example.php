<?php
/**
 * BrightFire Virtual Pages.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BrightFire_Virtual_Pages;

/**
 * ------------------------------------------
 * Static Virtual Page Example
 * ------------------------------------------
 */
add_action(
	'bf_virtual_pages',
	function( $virtual_page_controller ) {

		// first page.
		$virtual_page_controller->addPage( new Page( '/virtual-example/custom-page' ) )
			->setTitle( 'My First Custom Page' )
			->setContent( '<p>Hey, this is my first cutom virtual page!</p>' );

		// second page.
		$virtual_page_controller->addPage( new Page( '/virtual-example/custom-page-2' ) )
			->setTitle( 'ERIE Insurance Group' )
			->setContent( '<p>This is a custom page I made. It doesn\'t actually exist. How exciting</p>' );

		// third page - direct data send.
		$virtual_page_controller->addPage(
			new Page(
				'/virtual-example/custom-page-3',
				array(
					'title'     => 'My Custom Page of Stuff',
					'content'   => '<p>This is my content for this custom page</p>',
					'post_type' => 'bf_sample_post',
					'template'  => 'page.php',
				)
			)
		);

	}
);



/**
 * ------------------------------------------
 * AddRewrite Example with callback
 * ------------------------------------------
 */
add_action(
	'bf_virtual_pages',
	function( $virtual_page_controller ) {

		// addRewrite ( {REGEX}, {CALLBACK} ).
		// Callback will recieve as a single argument, the match from the REGEX.
		// Callback should return array with index, title, content, post_type.
		$virtual_page_controller->addRewrite( '/carriers\\/(.*)/', "\\BrightFireCore\\BrightFire_Virtual_Pages\\buildCarriers" );

		// Add wildcard rewrite with hard-coded content/title/etc.
		$virtual_page_controller->addRewrite(
			'/virtual-pages\\/(.*)/',
			array(
				'title'     => 'Virtual Pages test',
				'content'   => '<p>This is test data</p>',
				'post_type' => 'bf_virtual_page',
			)
		);

	}
);

/**
 * Build Carriers.
 *
 * @param string $slug Slug.
 * @return array
 */
function buildCarriers( $slug ) { // phpcs:ignore

	$return = array(
		'title'     => 'Carrier: ' . $slug,
		'content'   => 'This is my content for carrier matching slug ' . $slug,
		'post_type' => 'bf_carrier',
		'template'  => 'page.php',
	);

	return $return;

}



/**
 * ------------------------------------------
 * DEBUG
 * ------------------------------------------
 */
add_action(
	'wp_footer',
	function() {
		global $wp_query;
		\BrightFireCore\prettyprintr( $wp_query );
	}
);
