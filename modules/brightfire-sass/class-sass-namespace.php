<?php
/**
 * BrightFire SASS.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore;

/**
 * Class Sass_Namespace
 *      A single 'instance' of related SASS Asset paths, or literal SASS
 *      Accessed via BrightFireSass via it's registered namespace
 */
class Sass_Namespace {

	/**
	 * Registered priorities of sass or paths
	 *
	 * @var array
	 */
	public $assets = array();

	/**
	 * Sass_Namespace constructor.
	 */
	public function __construct() {
		// PHP 7 wants this.
	}

	/**
	 * Sorts $assets based on priority and returns array
	 *
	 * @return array
	 */
	public function sort_sass() {

		$sort = $this->assets;

		usort(
			$sort,
			function( $a, $b ) {
				return $a['priority'] - $b['priority'];
			}
		);

		$sort = array_unique( $sort, SORT_REGULAR );

		return $sort;
	}
}
