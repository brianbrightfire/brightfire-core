<?php
/**
 * BrightFire SASS.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore;

require_once BF_CORE_PATH . 'vendor/scssphp/scss.inc.php';
require_once dirname( __FILE__ ) . '/class-sass-manager.php';
require_once dirname( __FILE__ ) . '/class-sass-compiler.php';
require_once dirname( __FILE__ ) . '/class-sass-namespace.php';

/**
 * Makes a global object to use in plugins and Stellar
 */
global $bf_sass;
$bf_sass = new Sass_Manager();

$bf_sass->register_path( 'bourbon', BF_CORE_PATH . 'vendor/bourbon/_bourbon.scss' );
$bf_sass->register_path( 'neat', BF_CORE_PATH . 'vendor/neat/_neat.scss' );
