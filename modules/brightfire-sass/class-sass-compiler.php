<?php
/**
 * BrightFire SASS.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore;

/**
 * Class Sass_Compiler
 *      Compiles Sass based on the Sass_Manager->namespace_instances
 */
class Sass_Compiler {

	/**
	 * Namespace Instances.
	 *
	 * @var $namespace_instances
	 */
	private $namespace_instances;

	/**
	 * Compile Assets.
	 *
	 * @var array $compile_assets
	 */
	private $compile_assets;

	/**
	 * Sass_Compiler constructor.
	 *
	 * @param array $namespace_instances Namespace Instances.
	 */
	public function __construct( $namespace_instances ) {

		$this->namespace_instances = $namespace_instances;
		$this->compile_assets      = array();
	}

	/**
	 * Ensure we do not include the scss file more than once.
	 * This allows a single path to be registered in multiple namespaces without being included more than once in the
	 * final compilation. The path with earliest priority will be honored.
	 *
	 * @param array $namespaces Namespaces.
	 */
	private function set_compile_assets( $namespaces ) {

		$compile_assets = array();

		foreach ( $namespaces as $namespace ) {
			if ( array_key_exists( $namespace, $this->namespace_instances ) ) {
				$compile_assets = array_merge( $compile_assets, $this->namespace_instances[ $namespace ]->sort_sass() );
			}
		}

		$this->compile_assets = array_unique( $compile_assets, SORT_REGULAR );
	}

	/**
	 * Extract all paths that are registered and put into array
	 *
	 * @return array
	 */
	private function get_import_paths() {

		$paths = array();

		foreach ( $this->namespace_instances as $object ) {
			foreach ( $object->assets as $array ) {
				if ( array_key_exists( 'path', $array ) ) {
					$paths[] = preg_replace( '/[^\/]+$/m', '', $array['path'] );
				}
			}
		}

		return array_unique( $paths );
	}

	/**
	 * Concatenate sass strings
	 *
	 * @return string
	 */
	private function get_sass() {

		$sass = '';

		foreach ( $this->compile_assets as $index => $asset ) {
			$sass .= self::extract_sass( $asset );
		}

		return $sass;
	}

	/**
	 * Extracts sass from file
	 *
	 * @param string $asset Asset.
	 *
	 * @return bool|string
	 */
	private static function extract_sass( $asset ) {

		if ( ! array_key_exists( 'sass', $asset ) && array_key_exists( 'path', $asset ) && file_exists( $asset['path'] ) ) {

			// TODO: Refactor to utilize WP_Filesystem.
			return file_get_contents( $asset['path'] ); // phpcs:ignore

		} elseif ( array_key_exists( 'sass', $asset ) ) {

			return $asset['sass'];

		} else {

			return '';
		}
	}

	/**
	 * Compile sass to css
	 *
	 * @param string|array $namespaces Namespaces.
	 * @param string       $destination Destination.
	 * @param string       $formatter Formatter.
	 *
	 * @return array|string
	 */
	public function compile( $namespaces = array(), $destination = '', $formatter = '' ) {

		if ( ! is_array( $namespaces ) ) {
			$namespaces = array( $namespaces );
		}

		$this->set_compile_assets( $namespaces );

		$sass = $this->get_sass();

		$compiler = new \Leafo\ScssPhp\Compiler();

		$compiler->setImportPaths( $this->get_import_paths() );

		if ( empty( $formatter ) ) {
			$formatter = 'Leafo\ScssPhp\Formatter\Crunched';
		}

		$compiler->setFormatter( $formatter );

		try {
			$compiled = $compiler->compile( $sass );

			if ( empty( $destination ) ) {

				return $compiled;

			} else {

				// Final files saved.
				// TODO: Refactor to utilize WP_Filesystem.
				file_put_contents( $destination, $compiled ); // phpcs:ignore

				return array( 'fresh' => time() );
			}
		} catch ( \Exception $e ) {
			return array( 'rotten' => $e->getMessage() . ' timestamp:' . time() );
		}
	}
}
