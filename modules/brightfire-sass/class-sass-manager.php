<?php
/**
 * BrightFire SASS.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore;

/**
 * Class Sass_Manager
 *      Master SASS constructor class to handle namespacing and manipulation of
 *      each Sass_Namespace instance.
 */
class Sass_Manager {

	/**
	 * Namespace Instances.
	 *
	 * @var array $namespace_instances
	 */
	public $namespace_instances;

	/**
	 * Sass_Manager constructor.
	 */
	public function __construct() {
		$this->namespace_instances = array();
	}

	/**
	 * Register a *.scss file to a given namespace with a priority
	 *
	 * @param string $namespace Namespace.
	 * @param string $path Path.
	 * @param int    $priority Priority.
	 *
	 * @return bool
	 */
	public function register_path( $namespace = '', $path = '', $priority = 10 ) {

		$instance = $this->get_namespace_instance( $namespace );

		if ( ! empty( $path ) ) {

			$new_asset['path']     = $path;
			$new_asset['priority'] = $priority;

			array_push( $instance->assets, $new_asset );

			return true;

		}

		return false;

	}

	/**
	 * Register a literal string of SASS to a given namespace with a priority
	 *
	 * @param string $namespace Namespace.
	 * @param string $sass Sass.
	 * @param int    $priority Priority.
	 *
	 * @return bool
	 */
	public function register_sass( $namespace = '', $sass = '', $priority = 10 ) {

		$instance = $this->get_namespace_instance( $namespace );

		if ( ! empty( $sass ) ) {
			$new_asset['sass']     = $sass;
			$new_asset['priority'] = $priority;

			array_push( $instance->assets, $new_asset );

			return true;
		}

		return false;

	}

	/**
	 * Retrieve an instance, or make a new instance if one does not exist
	 *
	 * @param string $namespace Namespace.
	 *
	 * @return mixed
	 */
	private function get_namespace_instance( $namespace ) {

		if ( ! array_key_exists( $namespace, $this->namespace_instances ) ) {
			$this->namespace_instances[ $namespace ] = new Sass_Namespace();
		}

		return $this->namespace_instances[ $namespace ];
	}

	/**
	 * Helper method in our main Sass object to compile css based on namespacess
	 *
	 * @param array  $namespaces Namespaces.
	 * @param string $destination Destination.
	 * @param string $formatter Formatter.
	 *
	 * @return array|string
	 */
	public function compile( $namespaces = array(), $destination = '', $formatter = '' ) {
		$compiler = new Sass_Compiler( $this->namespace_instances );

		$response = $compiler->compile( $namespaces, $destination, $formatter );
		unset( $compiler );

		return $response;
	}
}
