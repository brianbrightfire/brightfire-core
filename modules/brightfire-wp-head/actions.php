<?php
/**
 * BrightFire WP Head.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\WP_Head;

/**
 * Display the link to the general feed. But NOT the comments feed link
 *
 * @see https://core.trac.wordpress.org/ticket/23692    Possibly a better way in the future?
 *
 * @param array $args Optional arguments.
 */
function remove_comments_feed_link( $args = array() ) {

	if ( ! current_theme_supports( 'automatic-feed-links' ) ) {
		return;
	}

	$defaults = array(
		/* translators: Separator between blog name and feed type in feed links */
		'separator' => _x( '&raquo;', 'feed link' ),
		/* translators: 1: blog title, 2: separator (raquo) */
		'feedtitle' => __( '%1$s %2$s Feed' ),
		/* translators: 1: blog title, 2: separator (raquo) */
		'comstitle' => __( '%1$s %2$s Comments Feed' ),
	);

	$args = wp_parse_args( $args, $defaults );

	echo '<link rel="alternate" type="' . feed_content_type() . '" title="' . esc_attr( sprintf( $args['feedtitle'], get_bloginfo( 'name' ), $args['separator'] ) ) . '" href="' . esc_url( get_feed_link() ) . "\" />\n";

}

/**
 * Removes emoji support from WordPress
 *
 * @link https://wordpress.org/plugins/disable-emojis/ Code taken from this plugin.
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}
