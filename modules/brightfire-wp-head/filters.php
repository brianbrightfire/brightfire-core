<?php
/**
 * BrightFire WP Head.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\WP_Head;

/**
 * Adds current theme name to the body class
 *
 * @param array $classes Classes.
 *
 * @return array
 */
function theme_name_body_class( $classes ) {
	$classes[] = get_option( 'stylesheet' );
	return $classes;
}
