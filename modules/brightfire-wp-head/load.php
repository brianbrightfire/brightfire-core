<?php
/**
 * BrightFire WP Head.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\WP_Head;

/** Require Files */
require_once dirname( __FILE__ ) . '/actions.php';
require_once dirname( __FILE__ ) . '/filters.php';


/**
 * Remove comments feed links from wp_head
 *
 * @uses remove_comments_feed_link
 */
remove_action( 'wp_head', 'feed_links', 2 );
add_action( 'wp_head', __NAMESPACE__ . '\remove_comments_feed_link', 2 );

/**
 * Removes emoji scripts and styles
 *
 * @uses disable_emojis()
 */
add_action( 'init', __NAMESPACE__ . '\disable_emojis' );

/**
 * Adds current theme name to the body class
 *
 * @uses theme_name_body_class
 */
add_filter( 'body_class', __NAMESPACE__ . '\theme_name_body_class' );

/**
 * Removes the RSD link from WordPress <head> "xmlrpc.php"
 */
remove_action( 'wp_head', 'rsd_link' );

/**
 * Removes the wlwmanifest link from WordPress <head>
 */
remove_action( 'wp_head', 'wlwmanifest_link' );

/**
 * Removes the shortlink from WordPress <head>
 */
remove_action( 'wp_head', 'wp_shortlink_wp_head' );
remove_action( 'template_redirect', 'wp_shortlink_header', 11 );
