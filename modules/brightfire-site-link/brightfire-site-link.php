<?php
/**
 * BrightFire Site Link.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Site_Link;

// Require Main Class.
require_once dirname( __FILE__ ) . '/class-bf-site-link.php';

// Run setup() on plugins_loaded.
add_action(
	'plugins_loaded',
	function() {
		BF_Site_Link::setup();
	}
);
