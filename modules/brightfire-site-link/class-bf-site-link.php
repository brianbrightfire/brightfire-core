<?php
/**
 * BrightFire Site Link.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Site_Link;

use function BrightFire\Plugin\Agency\get_location_field;

/**
 * Class BF_Site_Link
 *
 * Registers [bf_site_link] shortcode and callbacks.
 * Registers Keypage Mappings in BrightFire Site Settings.
 *
 * @package BrightFireCore\BF_Site_Link
 */
class BF_Site_Link {

	/**
	 * BF Site Setting prefix.
	 *
	 * @var string $option_prefix
	 */
	private static $option_prefix = 'bf_setting_';

	/**
	 * Set Up our mapping settings, and shortcode
	 */
	public static function setup() {

		// Register the Shortcode.
		add_shortcode( 'bf_site_link', array( '\\' . __NAMESPACE__ . '\\BF_Site_Link', 'bf_site_link_shortcode' ) );

		// Add Settings.
		add_action( 'add_meta_boxes_bf_site_settings', array( '\\' . __NAMESPACE__ . '\\BF_Site_Link', 'bf_settings_keypages' ) );

	}

	/**
	 * Shortcode Callback
	 *
	 * @param array  $attributes Attributes.
	 * @param string $content Content.
	 *
	 * @return string|bool
	 */
	public static function bf_site_link_shortcode( $attributes, $content = '' ) {

		// Parse Defaults.
		$atts = shortcode_atts(
			array(
				'keypage' => '',
				'slug'    => '',
			),
			$attributes
		);

		// If we don't have attributes required, bail.
		if ( '' === $atts['keypage'] && '' === $atts['slug'] ) {
			return self::bail( $content );
		}

		// Get Page ID.
		$post_id = self::get_page_id( $atts );

		// If we didn't find a link, bail.
		if ( ! $post_id ) {
			return self::bail( $content );
		}

		// Get the permalink.
		$permalink = get_the_permalink( $post_id );

		// This is {{one link}} and this is {{another link}}.

		// Merge our link in.
		$content = preg_replace( '/{{([^}]*)}}/', '<a href="' . $permalink . '">$1</a>', $content );

		// Output our content.
		return $content;

	}

	/**
	 * Adds Keypage Mappings Metabox to BF Site Settings
	 */
	public static function bf_settings_keypages() {

		add_meta_box(
			'bf_site_link_settings',
			'Keypage Mappings',
			array( '\\' . __NAMESPACE__ . '\\BF_Site_Link', 'bf_settings_keypages_metabox' ),
			'bf_site_settings',
			'normal',
			'high'
		);

	}

	/**
	 * Builds metabox content (form) for BF Site Settings
	 */
	public static function bf_settings_keypages_metabox() {

		$fields   = array();
		$instance = array();

		foreach ( self::keypages() as $keypage ) {

			$label = ucwords( str_replace( '-', ' ', $keypage ) );

			$fields[ self::$option_prefix . $keypage ] = array(
				'type'        => 'wp_page_select',
				'description' => '[bf_site_link keypage="' . $keypage . '"]',
				'label'       => $label,
			);

			$instance[ self::$option_prefix . $keypage ] = get_option( self::$option_prefix . $keypage, '' );

		}

		// Output and Render our Settings Form.
		echo "<form id='bf_settings_keypages' class='bf-settings-form'>";

		echo '<p>The <strong>Homepage (home)</strong>, <strong>Blog (blog)</strong> and <strong>Primary Location (primary-location)</strong> are automatically mapped in the system.';

		$args = array(
			'fields'   => $fields,
			'instance' => $instance,
			'display'  => 'basic',
			'echo'     => true,
		);

		global $bf_admin_api;
		$bf_admin_api->fields->build( $args );

		echo '</form>';

	}

	/**
	 * Returns a page_id (int) or false (bool) for requested page
	 * via keymap id OR page slug override
	 *
	 * @param array $atts Attributes.
	 * @return int|bool
	 */
	public static function get_page_id( $atts ) {

		// Default Key Page Request.
		if ( '' !== $atts['keypage'] && '' === $atts['slug'] ) {

			switch ( $atts['keypage'] ) {
				case 'home':
					return get_option( 'page_on_front' );
					break; // phpcs:ignore

				case 'blog':
					return get_option( 'page_for_posts' );
					break; // phpcs:ignore

				case 'primary-location':
					return get_option( 'bf_location_primary' );
					break; // phpcs:ignore

				default:
					return get_option( self::$option_prefix . $atts['keypage'], false );
					break; // phpcs:ignore

			}
		}

		// Page by Slug Request.
		if ( '' !== $atts['slug'] ) {

			$posts = get_posts(
				array(
					'name'        => $atts['slug'],
					'post_status' => 'publish',
					'numberposts' => 1,
				)
			);

			if ( $posts ) {
				return $posts[0]->ID;
			} else {
				return false;
			}
		}

		return false;

	}

	/**
	 * Defines which keypages we wish to allow mapping for.
	 *
	 * @return array
	 */
	public static function keypages() {

		return array(
			'about',
			'reviews',
			'policy-service-center',
			'file-a-claim',
			'online-billing-and-payments',
			'certificate-of-insurance-request',
			'policy-change-request',
			'auto-id-card-request',
			'annual-insurance-checklist',
			'write-a-review',
			'secure-contact-form',
			'insurance-resources',
		);

	}

	/**
	 * Control function for bailing from shortcode, if information is missing
	 * or required attributes are not present. Currently returns false.
	 *
	 * @param string $content Content.
	 *
	 * @return string|bool
	 */
	public static function bail( $content ) {
		return false;
	}


}
