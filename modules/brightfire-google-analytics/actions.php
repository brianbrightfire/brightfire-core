<?php
/**
 * BrightFire Google Analytics.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Google_Analytics;

use function BrightFireCore\get_site_domain_name;
use function BrightFireCore\is_bf_editor;
use function BrightFireCore\is_bf_super_admin;
use function BrightFireCore\is_brightfire_stellar_theme;


/**
 * Output google analytics page tracking scripts
 */
function bf_print_google_analytics() {

	$comment_out = false;
	$filter_ips  = array( '127.0.0.1', '4.35.198.66' );
	$request_ip  = bf_get_request_ip();

	// Comment out script if the request IP matching one of the $filter_ips
	// ONLY if on production - still want tracking to test properties on dev + staging.
	if ( in_array( $request_ip, $filter_ips, true ) && ( defined( BF_ENV ) && BF_ENV === 'PROD' ) ) {
		$comment_out = true;
	}

	echo '<!-- BrightFire Google Analytics -->' . "\r\n";
	echo '<script type="text/javascript">' . "\r\n";
	echo '// GA Core analytics.js' . "\r\n";
	echo "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');";
	echo "\r\n\r\n";

	echo $comment_out ? '/*' : '';

	// Available Action to output all GA scripts.
	do_action( 'bf_google_analytics_print' );

	echo $comment_out ? '*/' : '';
	echo '</script>' . "\r\n";
	echo '<!-- / BrightFire Google Analytics -->' . "\r\n";

}

/**
 * Conditional switch for adding specific trackers to specific screens
 */
function bf_add_bfa_trackers() {

	// Don't track super admins or LOCAL/STAGING traffic.
	if ( is_bf_super_admin() || ( defined( 'BF_ENV' ) && 'PROD' !== BF_ENV ) ) {
		return;
	}

	// Admin Aggregate Property ID (BF EDITORS ONLY).
	if ( is_admin() ) {
		bf_add_aggregate_tracking( 'UA-109320794-1' );
	} else {
		bf_add_aggregate_tracking( 'UA-109304193-1' );
	}
}

/**
 * Echo analytics for BFA BrightFire Aggregate
 *
 * @param string $property_id Property ID.
 */
function bf_add_aggregate_tracking( $property_id = '' ) {

	echo '// BFA' . "\r\n";
	echo ga_create_command( $property_id, 'bfa' ) . "\r\n";
	echo ga_set_dimension( 'bfa', 'dimension1', get_site_domain_name( true ) ) . "\r\n";
	echo ga_set_dimension( 'bfa', 'dimension2', get_option( 'bf_setting_website_package' ) ) . "\r\n";

	// Send 'platform' as dimension4.
	if ( is_brightfire_stellar_theme() ) {
		$platform = 'Stellar';
	} else {
		$platform = 'Legacy';
	}
	echo ga_set_dimension( 'bfa', 'dimension4', $platform ) . "\r\n";

	do_action( 'before_bfa_send_pageview' );
	echo ga_send_pageview( 'bfa' ) . "\r\n\r\n";

}


/**
 * Echo analytics for BrightFire Facebook Apps Aggregate
 *
 * @param string $domain Domain.
 * @param string $page_name Page Name.
 */
function bf_facebook_add_aggregate_tracking( $domain = '', $page_name = '' ) {

	$name        = 'bffb';
	$property_id = 'UA-109304193-2';
	$domain      = apply_filters( 'bf_erie_facebook_app_domain', $domain );

	echo '// BFA FACEBOOK' . "\r\n";
	echo ga_create_command( $property_id, $name ) . "\r\n";
	echo ga_set_dimension( $name, 'dimension1', $domain ) . "\r\n";
	echo "ga('{$name}.set', 'page', '{$page_name}');\r\n";
	echo ga_send_pageview( $name ) . "\r\n\r\n";

}
