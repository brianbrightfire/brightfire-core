<?php
/**
 * BrightFire Google Analytics.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Google_Analytics;

use function BrightFireCore\get_site_domain_name;

/**
 * Returns the IP address of the request
 *
 * @return mixed
 */
function bf_get_request_ip() {

	if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {

		// Check ip from share internet.
		$ip = $_SERVER['HTTP_CLIENT_IP'];

	} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {

		// To check ip is pass from proxy.
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

	} else {

		$ip = $_SERVER['REMOTE_ADDR'];

	}

	return $ip;

}

/**
 * Returns FALSE or a string of JS to create a named property ID tracker
 *
 * @param string $property_id Property ID.
 * @param string $name Name.
 *
 * @return bool|string
 */
function ga_create_command( $property_id, $name ) {
	if ( null !== $property_id ) {
		$domain = get_site_domain_name( true );
		return "ga('create', '{$property_id}', '{$domain}', {'name': '{$name}'});";
	} else {
		return false;
	}
}

/**
 * Sends a pageview to a named ga instance
 *
 * @param string $name Name.
 *
 * @return bool|string
 */
function ga_send_pageview( $name ) {
	if ( null !== $name ) {
		return "ga('{$name}.send', 'pageview');";
	} else {
		return false;
	}
}

/**
 * Sets a GA instance dimension
 *
 * @param string $name - GA instance.
 * @param string $dimension - Which dimension.
 * @param string $value - value to set.
 *
 * @return string
 */
function ga_set_dimension( $name, $dimension = '', $value = '' ) {
	return "ga('{$name}.set', '{$dimension}', '{$value}');";
}
