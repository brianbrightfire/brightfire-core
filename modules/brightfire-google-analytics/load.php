<?php
/**
 * BrightFire Google Analytics.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Google_Analytics;

/** Require Files */
require_once dirname( __FILE__ ) . '/actions.php';
require_once dirname( __FILE__ ) . '/functions.php';

/**
 * Google analytics code output
 *
 * @uses bf_print_google_analytics
 */
add_action( 'wp_head', __NAMESPACE__ . '\bf_print_google_analytics', 99 );
add_action( 'admin_head', __NAMESPACE__ . '\bf_print_google_analytics', 99 );

/**
 * Add BF Aggregate Tracking
 */
add_action( 'bf_google_analytics_print', __NAMESPACE__ . '\bf_add_bfa_trackers' );
