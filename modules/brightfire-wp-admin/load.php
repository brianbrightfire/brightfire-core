<?php
/**
 * BrightFire WP Admin.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\WP_Admin;

/** Require Files */
require_once dirname( __FILE__ ) . '/actions.php';
require_once dirname( __FILE__ ) . '/filters.php';

/**
 * Remove / Add Items to the WP Admin Bar
 *
 * @uses toolbar_site_id()
 */
add_action( 'admin_bar_menu', __NAMESPACE__ . '\bf_admin_bar', 31 );

/**
 * Remove / Add Items to the WP Admin Menu
 *
 * @uses remove_menus
 */
add_action( 'admin_menu', __NAMESPACE__ . '\bf_admin_menu', 11 );

/**
 * Adds buttons to the visual editor
 *
 * @uses add_mce_buttons
 */
add_filter( 'mce_buttons', __NAMESPACE__ . '\add_mce_buttons' );

/**
 * Adds BrightFire logo to login screen
 *
 * @uses login_image()
 */
add_action( 'login_head', __NAMESPACE__ . '\login_image' );

/**
 * Remove Dashboard Widgets
 */
add_action(
	'admin_init',
	function() {
		remove_meta_box( 'dashboard_right_now', 'dashboard', 'core' );
		remove_meta_box( 'dashboard_activity', 'dashboard', 'core' );
		remove_meta_box( 'dashboard_quick_press', 'dashboard', 'core' );
		remove_meta_box( 'dashboard_primary', 'dashboard', 'core' );
	}
);

/**
 * Remove Link Manager
 */
add_filter( 'option_link_manager_enabled', '__return_false' );

/**
 * Remove Gutenberg nag
 */
remove_action( 'try_gutenberg_panel', 'wp_try_gutenberg_panel' );

/**
 * Switch to blog when saving network settings
 */
add_action( 'clean_site_cache', __NAMESPACE__ . '\bf_clean_site_cache', 10, 3 );

/**
 * Cleanup tasks while deleting blog
 */
add_action( 'delete_blog', __NAMESPACE__ . '\delete_blog', 10, 2 );
