<?php
/**
 * BrightFire WP Admin.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\WP_Admin;

use function BrightFireCore\is_bf_super_admin;

/**
 * Strip WP Admin Menu of unallowed items
 *
 * @return bool
 */
function bf_admin_menu() {

	/** Non Super-Admin Actions */
	if ( ! is_bf_super_admin() ) {
		remove_menu_page( 'tools.php' );
	}

	return true;

}

/**
 * Strip admin bar of unallowed items, and adds BrightFire Support Link, Site ID Link
 *
 * @return bool
 */
function bf_admin_bar() {

	// Get the admin bar global.
	global $wp_admin_bar;

	// Make sure the admin bar exists as an object.
	if ( ! is_object( $wp_admin_bar ) ) {
		return false;
	}

	/** Non Super-Admin Actions */
	if ( ! is_bf_super_admin() ) {

		// Default WordPress MenuBar Items we want to allow.
		$allowed_items = apply_filters(
			'wp_admin_bar_allowed_items',
			array(
				'site-name',        // Site name and Dashboard Link.
				'edit',             // Edit Page (Frontend).
				'new-content',      // New Content Button.
				'user-actions',      // User Menu.
			)
		);

		/** Removes all WP Admin Bar nodes not listed in @var $allowed_items above */
		$nodes = $wp_admin_bar->get_nodes();
		foreach ( $nodes as $node ) {
			if ( ! $node->parent && ! in_array( $node->id, $allowed_items, true ) ) {
				$wp_admin_bar->remove_menu( $node->id );
			}
		}

		// BrightFire Support Link.
		$support_icon = '<span class="dashicons dashicons-editor-help" style="font-family: \'dashicons\'"></span> ';
		$wp_admin_bar->add_menu(
			array(
				'parent' => false,
				// use 'false' for a root menu, or pass the ID of the parent menu.
				'id'     => 'bf_support',
				// link ID, defaults to a sanitized title value.
				'title'  => $support_icon . __( ' BrightFire Support' ),
				// link title.
				'href'   => 'http://www.brightfiresupport.com',
				// name of file.
				'meta'   => array( 'target' => '_blank' ),
			// phpcs:ignore
			// array of any of the following options: array( 'html' => '', 'class' => '', 'onclick' => '', target => '', title => '' );.
			)
		);

	}

	/** ONLY Super-Admin Actions */
	if ( is_bf_super_admin() ) {

		// Site ID Link.
		$args = array(
			'id'    => 'bf-site-id',
			'title' => 'Site ' . get_current_blog_id(),
			'href'  => network_admin_url( 'site-info.php?id=' . get_current_blog_id() ),
		);

		$wp_admin_bar->add_menu( $args );
	}

	return true;

}

/**
 * Add BrightFire logo image to login screen
 */
function login_image() {
	echo '<style>#login h1 a { background-image: url("' . BF_CORE_ASSETS . 'images/bf-insurancewebsites.png"); background-size: auto; width: auto; margin: 0; }</style>';
}

/**
 * Switch to blog and provide hook when saving network level site info settings.
 *
 * @param integer $blog_id Blog ID.
 * @param string  $blog Blog.
 * @param string  $domain_path_key Domain Path Key.
 */
function bf_clean_site_cache( $blog_id, $blog, $domain_path_key ) {

	switch_to_blog( $blog_id );
	do_action( 'bf_clean_site_cache' );
	restore_current_blog();

}

/**
 * Cleanup we need to do during site deletion
 *
 * @param integer $blog_id Blog ID.
 * @param boolean $drop Drop.
 */
function delete_blog( $blog_id, $drop ) {

	if ( true === $drop ) {

		/**
		 * We remove this filter because it throws a Notice that is derived from Gravity Forms itself.
		 * The gravityforms uninstall script checks for old tables and will delete them. That check triggers the Notice below.
		 *
		 * Notice: An outdated add-on or custom code is attempting to access the wp_*_rg_form table which is not valid in this version of Gravity Forms. Update your add-ons and custom code to prevent loss of form data. Further details: https://docs.gravityforms.com/database-storage-structure-reference/#changes-from-gravity-forms-2-2 in /app/public/wp-content/plugins/gravityforms/gravityforms.php on line 5153
		 */
		remove_filter( 'query', array( 'GFForms', 'filter_query' ) );

		/**
		 * Remove this since the site will not exist when triggered, and it tries to switch to blog
		 */
		remove_action( 'clean_site_cache', 'BrightFireCore\WP_Admin\bf_clean_site_cache' );

		/**
		 * SELECT all tables relating to a specific blog id and add them to wpmu_drop_tables
		 */
		global $wpdb;

		$table_list = $wpdb->get_results( $wpdb->prepare( 'SELECT table_name FROM information_schema.TABLES WHERE table_name LIKE %s;', $wpdb->esc_like( "{$wpdb->base_prefix}{$blog_id}_" ) . '%' ), ARRAY_A );

		add_filter(
			'wpmu_drop_tables',
			function ( $filter_list ) use ( $table_list ) {

				foreach ( $table_list as $index => $data ) {
					$filter_list[] = $data['table_name'];
				}

				return array_unique( $filter_list );

			}
		);
	}
}
