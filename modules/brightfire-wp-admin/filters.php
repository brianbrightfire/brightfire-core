<?php
/**
 * BrightFire WP Admin.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\WP_Admin;

/**
 * Adds buttons to the visual editor
 *
 * @param array $buttons Buttons.
 *
 * @return mixed
 */
function add_mce_buttons( $buttons ) {
	array_push( $buttons, 'fontselect,fontsizeselect' );
	return $buttons;
}
