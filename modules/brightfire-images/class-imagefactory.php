<?php
/**
 * Image Factory
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Images;

/**
 * Class ImageFactory
 *
 * @package BrightFireCore\Image_Handler
 *
 * Main entry point for all things images
 * USAGE:
 *
 * 1. Create a new Image Options Object
 * ---------------------------------------------------------
 * $options = ImageFactory::create_image_options();
 *
 * 2. Update options with any overrides
 * ---------------------------------------------------------
 * $options->force_width_desktop = $block['image_width'];
 * $options->force_width_mobile = $block['image_width_mobile'];
 * $options->widget_instance = $instance;
 * $options->sidebar_id = $args['id'];
 *
 * 3. Generate Image Tag OR
 * ---------------------------------------------------------
 * $img_tag = ImageFactory::get_image_tag( $image_id, $options );
 *
 * 4. Get Image Path
 * ---------------------------------------------------------
 * $path = ImageFactory::get_image_path( $image_id, $options );
 */
class ImageFactory {

	/**
	 * Create an Image Options Object and return it
	 *
	 * @return ImageOptions
	 */
	public static function create_image_options() {
		return new ImageOptions();
	}

	/**
	 * Enforces defaults through options object and validates image_id is valid before instantiating Image class
	 *
	 * @param int   $image_id - Requested Image ID.
	 * @param array $options - Options.
	 *
	 * @return bool | Image | Image_External
	 */
	public static function create_image_object( $image_id = null, $options = null ) {

		// Force $image_id as an integer.
		$image_id = (int) $image_id;

		// Ensure we have a valid Image ID.
		if ( 0 === $image_id || ! is_int( $image_id ) ) {
			return false;
		}

		// Ensure we have a valid ImageOptions object.
		if ( ! is_a( $options, __NAMESPACE__ . '\ImageOptions' ) ) {
			$options = self::create_image_options();
		}

		// Switch for external vs. local Images.
		if ( $options->source_site ) {
			$image = new Image_External( $image_id, $options );
		} else {
			$image = new Image( $image_id, $options );
		}

		// Ensure Image Exists before proceeding.
		if ( $image->options->is_404 ) {
			unset( $image );
			return false;
		} else {
			return $image;
		}
	}

	/**
	 * Gets a full <img> tag.
	 *
	 * @param int                $image_id - Image ID.
	 * @param ImageOptions|array $options - Options.
	 *
	 * @return bool | string
	 */
	public static function get_image_tag( $image_id = null, $options = null ) {

		$img_obj = self::create_image_object( $image_id, $options );

		if ( $img_obj ) {

			// Get Image Tag, Clean Up, and Return.
			$img_rtn = $img_obj->get_img_tag();
			unset( $img_obj );
			return $img_rtn;

		} else {
			return false;
		}

	}

	/**
	 * Gets the FULL size image path
	 *
	 * @param int                $image_id - Image ID.
	 * @param ImageOptions|array $options - Options.
	 * @return bool | string
	 */
	public static function get_image_path( $image_id = null, $options = null ) {

		$img_obj = self::create_image_object( $image_id, $options );

		if ( $img_obj ) {

			// Get Image Tag, Clean Up, and Return.
			$img_rtn = $img_obj->options->src;
			unset( $img_obj );
			return $img_rtn;

		} else {
			return false;
		}

	}

}
