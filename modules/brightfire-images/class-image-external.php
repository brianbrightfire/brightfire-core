<?php
/**
 * BrightFire Image External.
 *
 * @package BrightFireCore
 */

declare(strict_types=1);

namespace BrightFireCore\Images;

/**
 * Class Image_Builder
 * Responsible for building and returning <img> tag and its attributes
 *
 * @requires BrightFire Stellar
 */
class Image_External extends Image {

	/**
	 * Setup the class
	 *
	 * @param int          $image_id  - (REQUIRED) Post ID of Image in WP.
	 * @param ImageOptions $options - (REQUIRED) our options.
	 */
	public function __construct( int $image_id, ImageOptions $options ) { // phpcs:ignore
		parent::__construct( $image_id, $options );
	}

	/**
	 * Setup Options.
	 */
	protected function setup_options() {

		// Run our query.
		$results = $this->query();

		if ( ! $results ) {
			$this->options->is_404 = true;
			return false;
		}

		// Set ALT.
		if ( empty( $this->options->alt ) ) {
			$this->options->alt = isset( $results->_wp_attachment_image_alt ) ? $results->_wp_attachment_image_alt : '';
		}

		// Set TITLE.
		if ( empty( $this->options->title ) ) {
			$this->options->title = $results->post_title;
		}

		// Check Lazyload (add class).
		if ( $this->options->lazy_load ) {
			$this->options->classes[] = 'lazyload';
		}

		// Set METADATA.
		$this->options->metadata = maybe_unserialize( $results->_wp_attachment_metadata );

		// Set SRC, Width, Height.
		$this->options->src = $this->build_image_path( $this->options->metadata['file'] );

		// Set SRCSET.
		$this->options->srcset = $this->build_srcset();

		// Set SIZES.
		$this->options->sizes = $this->calc_sizes();

	}

	/**
	 * Get Image and metadata
	 *
	 * @return array|bool|null|object|void
	 */
	private function query() {

		global $wpdb;

		// Get our Image Post.
		// phpcs:disable
		$image_query = "
			SELECT p.*, sizes._wp_attachment_metadata, alt._wp_attachment_image_alt, file._wp_attached_file 
			FROM wp_{$this->options->source_site}_posts p
			LEFT OUTER JOIN ( 
				SELECT post_id, meta_value as _wp_attachment_metadata 
				FROM wp_{$this->options->source_site}_postmeta 
				WHERE meta_key = '_wp_attachment_metadata' 
					AND post_id = '{$this->image_id}' 
			) as sizes ON sizes.post_id = p.ID
			LEFT OUTER JOIN ( 
				SELECT post_id, meta_value as _wp_attachment_image_alt 
				FROM wp_{$this->options->source_site}_postmeta 
				WHERE meta_key = '_wp_attachment_image_alt' 
					AND post_id = '{$this->image_id}' 
			) as alt ON alt.post_id = p.ID
			LEFT OUTER JOIN (
				SELECT post_id, meta_value as _wp_attached_file
				FROM wp_{$this->options->source_site}_postmeta
				WHERE meta_key = '_wp_attached_file'
					AND post_id = '{$this->image_id}'
			) as file ON file.post_id = p.ID
			WHERE p.ID = {$this->image_id}
		";
		$results     = $wpdb->get_row( $image_query );
		// phpcs:enable

		if ( empty( $results ) ) {
			return false;
		} else {
			return $results;
		}

	}

	/**
	 * Sets our SRCSET attribute value
	 *
	 * @return string
	 */
	private function build_srcset() : string {

		// Default Path.
		$srcset[] = $this->build_image_path( $this->options->metadata['file'] ) . ' ' . $this->options->metadata['width'] . 'w';
		$path     = '';

		// Source Path.
		if ( ! empty( $this->options->src ) ) {
			$path_arr = explode( '/', $this->options->src );

			if ( count( $path_arr ) > 1 ) {
				array_pop( $path_arr );
				$path = implode( '/', $path_arr ) . '/';
			}
		}

		foreach ( $this->options->metadata['sizes'] as $size ) {

			$filename = $size['file'];

			$srcset[] = $this->build_image_path( $path . $filename ) . ' ' . $size['width'] . 'w';

		}

		return implode( ', ', $srcset );
	}

	/**
	 * Given file path in site upload folder, build a complete path from current site URL
	 *
	 * @param string $file File.
	 * @return string
	 */
	private function build_image_path( $file ) {

		return site_url( "wp-content/uploads/sites/{$this->options->source_site}/" . $file );

	}


}
