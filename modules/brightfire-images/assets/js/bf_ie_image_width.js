/**
 * Run IE browser check and image width fix
 */
$(document).on('ready', function () {

    // Check for IE browser
    if (navigator.userAgent.match(/Trident/)) {
       bf_ie_image_width();
    }

});

/**
 * Add image width attribute for all images that use scrset and sizes; Backwards compatibility for IE 11 and older
 */
function bf_ie_image_width() {

    var images = $('img[sizes]');

    var viewportWidth = $(window).width();

    $.each(images, function (index, el) {

        var sizes = $(el).attr('sizes');
        var width = 0;

        // Check for a string that
        // 1) ONLY contains an integer followed by "px" or "vw" and is at the END of the line
        var match = sizes.match('([0-9]+)(px|vw)$');

        // Remove empty array elements and nulls
        match = match.filter(function (e) {
            return e;
        });

        if (match) {

            if (match[2] === 'px') {
                width = match[1];
            } else {
                width = (match[1] * viewportWidth) / 100;
            }
        }

        // Add the width attribute with our calculated value
        $(el).attr('width', width);

    });

}
