var $ = jQuery.noConflict();

/**
 * Global container for LazyLoad
 *
 * @type {null}
 */
var bfLazyLoad = null;

/**
 * Instantiate LazyLoad
 */
$(document).on( 'ready bf_selective_refresh_add bf_selective_refresh_rendered', function() {
    bf_lazyload();
});

/**
 * Attach LazyLoad to '.lazyload' selector
 */
function bf_lazyload() {
    bfLazyLoad = new LazyLoad({elements_selector: ".lazyload"});
}
