<?php
/**
 * Image Module
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Images;

require_once dirname( __FILE__ ) . '/class-image.php';
require_once dirname( __FILE__ ) . '/class-image-external.php';
require_once dirname( __FILE__ ) . '/class-imageoptions.php';
require_once dirname( __FILE__ ) . '/class-imagefactory.php';

add_action(
	'wp_enqueue_scripts',
	function() {
		wp_enqueue_script( 'lazyload' );
	}
);

global $bf_sass;
$bf_sass->register_path( 'bf-core', dirname( __FILE__ ) . '/assets/css/lazyload.scss' );
