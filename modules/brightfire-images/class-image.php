<?php
/**
 * BrightFire Image.
 *
 * @package BrightFireCore
 */

declare(strict_types=1);

namespace BrightFireCore\Images;

use function BrightFire\Theme\Stellar\get_stellar_template_id;

/**
 * Class Image_Builder
 * Responsible for building and returning <img> tag and its attributes
 *
 * @requires BrightFire Stellar
 */
class Image {

	/**
	 * Validated Post ID for a WP Image.
	 *
	 * @var int $image_id
	 */
	public $image_id;

	/**
	 * Object of our options.
	 *
	 * @var ImageOptions $options
	 */
	public $options;

	/**
	 * Breakpoint.
	 *
	 * @var int|string $breakpoint
	 */
	protected $breakpoint = 768;

	/**
	 * Container Width.
	 *
	 * @var int|string $container_width
	 */
	protected $container_width = 1200;

	/**
	 * Setup the class
	 *
	 * @param int          $image_id  - (REQUIRED) Post ID of Image in WP.
	 * @param ImageOptions $options - (REQUIRED) our options.
	 */
	public function __construct( int $image_id, ImageOptions $options ) {

		// Set Image ID and Options.
		$this->image_id = $image_id;
		$this->options  = $options;

		// Site Options.
		$this->breakpoint      = get_theme_mod( 'breakpoint', 768 );
		$this->container_width = get_theme_mod( 'max-width', 1200 );

		// Set up options.
		$this->setup_options();

	}

	/**
	 * Returns '<img {data-}src="" {data-}srcset="" sizes="" alt="" title="" class=""/>'
	 *
	 * @return string
	 */
	public function get_img_tag() : string {

		// Build all of our attributes.
		$attributes          = array();
		$attributes['alt']   = $this->options->alt;
		$attributes['title'] = $this->options->title;
		$attributes['sizes'] = $this->options->sizes;
		$attributes['class'] = implode( ' ', $this->options->classes );

		if ( $this->options->lazy_load ) {
			$attributes['src']         = $this->get_placeholder();
			$attributes['data-src']    = $this->options->src;
			$attributes['data-srcset'] = $this->options->srcset;
		} else {
			$attributes['src']    = $this->options->src;
			$attributes['srcset'] = $this->options->srcset;
		}

		// Build our Attributes Array for placement into our img tag.
		$img_atts = array();
		foreach ( $attributes as $attr => $value ) {

			if ( '' !== $value ) {
				$img_atts[] = "${attr}=\"${value}\"";
			}
		}

		$img_atts = implode( ' ', $img_atts );

		return "<img {$img_atts}/>";

	}

	/**
	 * Setup Options.
	 */
	protected function setup_options() {

		// Check if we have a valid image.
		if ( ! wp_attachment_is_image( $this->image_id ) ) {
			$this->options->is_404 = true;
			return false;
		}

		// Set ALT.
		if ( empty( $this->options->alt ) ) {
			$this->options->alt = get_post_meta( $this->image_id, '_wp_attachment_image_alt', true );
		}

		// Set TITLE.
		if ( empty( $this->options->title ) ) {
			$this->options->title = get_the_title( $this->image_id );
		}

		// Check Lazyload (add class).
		if ( $this->options->lazy_load ) {
			$this->options->classes[] = 'lazyload';
		}

		// Set SRC.
		$attributes = wp_get_attachment_image_src( $this->image_id, 'full' );

		if ( $attributes ) {
			$this->options->src    = $attributes[0];
			$this->options->width  = $attributes[1];
			$this->options->height = $attributes[2];
		}

		// Set SRCSET.
		$this->options->srcset = wp_get_attachment_image_srcset( $this->image_id, 'full' );

		// Set METADATA.
		$this->options->metadata = wp_get_attachment_metadata( $this->image_id );

		// Set SIZES.
		$this->options->sizes = $this->calc_sizes();

	}

	/**
	 * Get Placeholder.
	 *
	 * @return string
	 */
	protected function get_placeholder() : string {
		$data = wp_parse_args(
			$this->options->metadata,
			array(
				'width'  => '',
				'height' => '',
			)
		); // Ensure we have defaults.
		return plugin_dir_url( __FILE__ ) . "placeholder.php?w={$data['width']}&h={$data['height']}";
	}

	/**
	 * Sets our SIZES attribute value
	 *
	 * @return string
	 */
	protected function calc_sizes() : string {

		// Check for simultaneous use of force_width_desktop AND force_height.
		if ( $this->options->force_width_desktop > 0 && $this->options->force_height > 0 ) {
			$msg = 'Image class does not support simultaneous use of force_width AND force_height options. Force height has been removed.';
			trigger_error( $msg, E_USER_NOTICE ); // phpcs:ignore
		}

		// Natural Max Size.
		$metadata = wp_parse_args(
			$this->options->metadata,
			array(
				'width'  => '',
				'height' => '',
			)
		); // Ensure we have defaults.

		// Natural Max Size.
		$natural_width  = (int) $metadata['width'];
		$natural_height = (int) $metadata['height'];

		if ( $this->options->force_height && 0 === $this->options->force_width_desktop ) {
			$aspect    = $natural_width / $natural_height;
			$new_width = $aspect * $this->options->force_height;

			if ( $new_width <= $natural_width ) {
				$this->options->force_width_desktop = $new_width;
			}
		}

		// Get Stellar Breakpoint (if exists), or use 768.
		$media_condition = "(max-width: {$this->breakpoint}px)";

		// Build Desktop / Mobile width(s).
		if ( $this->options->force_width_desktop > 0 ) {
			$desktop = $this->options->force_width_desktop . 'px';
		} elseif ( $this->options->widget_instance ) {
			$desktop = $this->_calculate_max_viewport_width( $natural_width );
		} else {
			$desktop = "{$natural_width}px";
		}

		if ( $this->options->force_width_mobile > 0 ) {
			$mobile = $this->options->force_width_mobile . 'px';
		} else {
			$mobile = '100vw';
		}

		return "{$media_condition} {$mobile}, {$desktop}";

	}

	/**
	 * Returns maximum possible percentage of viewport-width, based on container width, column width, and widget width
	 *
	 * @param int $natural_width - The native width of our image.
	 * @return string
	 */
	protected function _calculate_max_viewport_width( int $natural_width ) : string { // phpcs:ignore

		$window_width = false;

		/** TODO: Is there a better call to define these counts - possibly inside Stellar somewhere?? */
		$structure_classes = [
			'one-twelfth'     => '1',
			'one-sixth'       => '2',
			'one-fifth'       => '2.4',
			'one-fourth'      => '3',
			'one-third'       => '4',
			'two-fifths'      => '4.8',
			'five-twelfths'   => '5',
			'one-half'        => '6',
			'seven-twelfths'  => '7',
			'three-fifths'    => '7.2',
			'two-thirds'      => '8',
			'three-fourths'   => '9',
			'four-fifths'     => '9.6',
			'five-sixths'     => '10',
			'eleven-twelfths' => '11',
			'full-width'      => '12',
		];

		// Get the Percentage Width of our Widget Area in respect to 12 column grid in the row.
		if ( $this->options->sidebar_id ) {

			/** Stellar Layout. @var \Stellar_Layouts_Admin $stellar_layout */
			global $stellar_layout;

			// Make sure our sidebar id isn't a one-off widget area override.
			if ( substr( $this->options->sidebar_id, 0, strrpos( $this->options->sidebar_id, '-' ) ) === 'sidebars' ) {

				$template_id               = get_stellar_template_id();
				$template                  = $stellar_layout->get_layout_option( 'templates' )[ $template_id ];
				$this->options->sidebar_id = "{$template['sidebar_row']}-{$template['sidebar_col']}";

			}

			$row_id     = substr( $this->options->sidebar_id, 0, strrpos( $this->options->sidebar_id, '-' ) );
			$sidebar_id = substr( $this->options->sidebar_id, ( strrpos( $this->options->sidebar_id, '-' ) + 1 ) );
			$row        = $stellar_layout->get_layout_option( 'rows' )[ $row_id ];
			$row_cols   = $row[ $sidebar_id ];
			$col_width  = ( $row_cols / 12 );

			if ( array_key_exists( 'container-classes', $row ) && is_array( $row['container-classes'] ) && in_array( 'window-width', $row['container-classes'], true ) ) {
				$window_width = true;
			}
		} else {
			$col_width = 1;              // Fallback if no sidebar id is provided.
		}

		// Get the Percentage Width of our Widget in respect to 12 column grid in the Widget Area.
		if ( $this->options->widget_instance ) {
			$widget_structure_class = $structure_classes[ str_replace( '-last', '', $this->options->widget_instance['structure_class'] ) ];
			$widget_width           = $widget_structure_class / 12;
		} else {
			$widget_width = 1;    // Fallback if no widget instance is provided.
		}

		// Find Maximum Percentage of Viewport width taking into account widget and widget area widths.
		$max_vw = ( ( $widget_width ) * ( $col_width ) );

		if ( $window_width ) {

			$vw_output = $max_vw * 100;

			return "{$vw_output}vw";

		} else {
			$max_px = $max_vw * $this->container_width;

			if ( $max_px < $natural_width ) {
				return "{$max_px}px";
			} else {
				return "{$natural_width}px";
			}
		}

	}


}
