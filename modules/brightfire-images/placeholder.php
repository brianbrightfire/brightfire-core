<?php
/**
 * BrightFire Images Placeholder.
 *
 * @package BrightFireCore
 */

// Image size.
$image_width  = is_numeric( $_GET['w'] ) ? $_GET['w'] : 0; // phpcs:ignore
$image_height = is_numeric( $_GET['h'] ) ? $_GET['h'] : 0; // phpcs:ignore

// Header.
header( 'Content-Type: image/png' );

// Create Image.
$image = imagecreatetruecolor( $image_width, $image_height );
imagesavealpha( $image, true );
$color = imagecolorallocatealpha( $image, 0, 0, 0, 127 );
imagefill( $image, 0, 0, $color );

// Ouput.
imagepng( $image );
imagedestroy( $image );
