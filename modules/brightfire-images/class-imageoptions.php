<?php
/**
 * BrightFire Image Options.
 *
 * @package BrightFireCore
 */

declare(strict_types=1);

namespace BrightFireCore\Images;

/**
 * Class ImageOptions - any expected options and their defaults for use in Image Class
 *
 * @package BrightFireCore\Image_Handler
 */
class ImageOptions {

	/** Defaults */
	// phpcs:disable
	public $force_width_desktop = 0;        // Forced Width above breakpoint.
	public $force_width_mobile = 0;         // Force Width below breakpoint.
	public $force_height = 0;               // Reverse Engineer width based on height declaration and image ratio.
	public $widget_instance = null;         // Widget instance ($this in context of a widget class).
	public $sidebar_id = null;              // WP Sidebar a widget is in ($args['id'] in context of a widget class).
	public $alt = '';                       // Override for an alt attribute (defaults to postmetadata).
	public $title = '';                     // Override for a title attribute (defaults to postmetadata).
	public $caption = '';                   // Override for an image caption (defaults to postmetadata).
	public $classes = array();              // Array of classes to place on our <img> tag.
	public $source_site = null;             // Source Site (on Network) which this images lives.

	/** Defaults assume lazy-loading */
	public $lazy_load = true;               // Weather or not image should use lazy-loading (defaults true).

	/** Placeholders for Image Classes */
	public $srcset  = '';                   // SrcSet string.
	public $metadata = array();             // Image metadata.
	public $sizes    = array();             // Array of available sizes.
	public $src      = '';                  // Original file path of 'full' image.
	public $width    = 0;                   // Natural Width of 'full' image.
	public $height   = 0;                   // Natural Height of 'full' image.

	/** Test Placeholder */
	public $is_404 = false;
	// phpcs:enable

	/**
	 * ImageOptions constructor.
	 * Translates any provided options into properties of this object
	 */
	public function __construct() { }

}
