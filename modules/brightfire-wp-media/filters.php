<?php
/**
 * BrightFire WP Media.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\WP_Media;

/**
 * Add MIME types to array of acceptable MIME types to upload
 *
 * @param array $existing_mimes Existing Mimes.
 *
 * @return array
 */
function custom_upload_mimes( $existing_mimes = array() ) {

	// Add file extension 'extension' with mime type 'mime/type'.
	$existing_mimes['vcf'] = 'text/x-vcard';

	return $existing_mimes;
}

/**
 * Makes responsive image srcset attribute return correct schema for SSL
 *
 * @param array $sources Sources.
 *
 * @return mixed
 */
function ssl_srcset( $sources ) {
	foreach ( $sources as &$source ) {
		if ( is_ssl() ) {
			$source['url'] = set_url_scheme( $source['url'], 'https' );
		}
	}

	return $sources;
}
