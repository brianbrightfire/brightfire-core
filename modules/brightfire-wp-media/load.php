<?php
/**
 * BrightFire WP Media.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\WP_Media;

/** Require Files */
require_once dirname( __FILE__ ) . '/filters.php';

/**
 * Add MIME types to array of acceptable MIME types to upload
 *
 * @uses custom_upload_mimes
 */
add_filter( 'upload_mimes', __NAMESPACE__ . '\custom_upload_mimes' );

/**
 * Makes responsive image srcset attribute return correct schema for SSL
 *
 * @uses ssl_srcset()
 */
add_filter( 'wp_calculate_image_srcset', __NAMESPACE__ . '\ssl_srcset' );
