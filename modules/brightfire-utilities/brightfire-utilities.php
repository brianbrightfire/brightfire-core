<?php
/**
 * BrightFire Utilities.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore;

/**
 * Pretty print_r function
 *
 * Wraps the print_r function in pre tags so you can see data better.
 * Styles pre tag so overflow and z-indices don't bother you.
 *
 * @param mixed $var Var.
 * @param null  $return Return.
 *
 * @uses \print_r()
 *
 * @return string
 */
function prettyprintr( $var, $return = null ) {
	$pretty  = '<style> .prettyprintr { background-color: #eee; border: 1px solid hotpink; color: #000; max-width: 90%; overflow: auto; padding: 20px; z-index: 9998; position:relative;} </style>';
	$pretty .= '<pre class="prettyprintr">';
	$pretty .= print_r( $var, true ); // phpcs:ignore
	$pretty .= '</pre>';

	if ( $return ) {
		return $pretty;
	} else {
		echo $pretty;
	}
}

/**
 * Recursive function to check empty
 *
 * @param mixed $value Value.
 *
 * @return bool
 */
function empty_r( $value ) {

	if ( is_array( $value ) ) {

		$empty = true;

		array_walk_recursive(
			$value,
			function( $item ) use ( &$empty ) {

				$empty = $empty && empty( $item );
			}
		);

	} else {

		$empty = empty( $value );
	}

	return $empty;
}

/**
 * Recursive function to check in_array
 *
 * @param mixed $needle Needle.
 * @param array $haystack Haystack.
 * @param bool  $strict Strict.
 *
 * @return bool
 */
function in_array_r( $needle, $haystack, $strict = false ) {

	foreach ( $haystack as $item ) {

		if ( ( $strict ? $item === $needle : $item === $needle ) || ( is_array( $item ) && in_array_r( $needle, $item, $strict ) ) ) {

			return true;
		}
	}

	return false;
}

/**
 * Recursively replace values in an array.
 * Runs through the $haystack array to find values that match key of $find_replace and replaces with value of that key.
 *
 * @param array $haystack       array to search through.
 * @param array $find_replace   key => value pairs of array( $find => $replace ).
 *
 * @return array | bool         Returns original array with appropriate replaced values or false
 */
function array_walk_recursive_find_replace( $haystack, $find_replace ) {

	if ( ! is_array( $haystack ) || ! is_array( $find_replace ) ) {
		return false;
	}

	$flag = false;

	array_walk_recursive(
		$haystack,
		function ( &$data ) use ( &$flag, $find_replace ) {

			foreach ( $find_replace as $find => $replace ) {

				if ( $find === $data ) {
					$data = $replace;
					$flag = true;
				}
			}
		}
	);

	if ( ! $flag ) {

		return false;

	} else {

		return $haystack;
	}
}

/**
 *
 * Checks to see if the link passed in is an external link as compared to the current site's domain
 *
 * @param string $link Link.
 *
 * @return bool
 */
function is_external_link( $link = null ) {

	if ( $link ) {

		$current_site_url = site_url();

		// See if the current site's URL can be found in the incoming link arg and return results in $matches_array.
		preg_match( '/' . preg_quote( $current_site_url, '/' ) . '/', $link, $matches_array );

		// If we had a match then this is NOT an external link.
		if ( empty( $matches_array ) ) {
			return true;
		}
	}

	// Defaults as an internal link.
	return false;
}

/**
 * Searches HTML block for first image tag and returns its src URL
 *
 * Used by bf_loop_featured_image_src filter
 *
 * @param string $original_image_src Original Image Source.
 * @param string $html HTML.
 *
 * @return mixed
 */
function extract_image_url_from_html( $original_image_src, $html ) {

	$image_src = $original_image_src;

	// Instantiate DOMDocument http://php.net/manual/en/class.domdocument.php.
	$doc = new \DOMDocument();

	// Load HTML and silence error if malformed HTML.
	$can_use_dom = @$doc->loadHTML( $html ); // phpcs:ignore

	if ( true === $can_use_dom ) {
		$images = $doc->getElementsByTagName( 'img' );

		// Check to see if we have an image and only work with the first one.
		if ( $images && $images->item( 0 ) ) {

			$image = $images->item( 0 );

			$new_img_src = trim( $image->getAttribute( 'src' ) );

			if ( ! empty( $new_img_src ) ) {
				$new_img_src = html_entity_decode( rawurldecode( $new_img_src ) );

				// No resource url please.
				// and request all assets over HTTPS.
				$new_img_src = preg_replace( '/(.{0,6})\/\/(.*)/', 'https://$2', $new_img_src );

				// Some extra work for ERIE Insurance issues.
				// ERIE has some jacked up URL paths that include spaces so we need to clean these up.
				$new_img_src = str_replace( ' ', '%20', $new_img_src );

				$image_src = $new_img_src;
			}
		}
	}

	return $image_src;
}

/**
 * Check for BrighFire Stellar theme
 *
 * @return bool
 */
function is_brightfire_stellar_theme() {

	// The current theme.
	$current_theme = get_option( 'stylesheet' );

	if ( 'brightfire-stellar' === $current_theme ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Check for BrighFire Version 1 themes
 *
 * @return bool
 */
function is_brightfire_v1_theme() {

	// The current theme.
	$current_theme = get_option( 'stylesheet' );

	// List of V1 themes.
	$themes = array(
		'brightfire-illuminate',
		'brightfire-brilliance',
		'brightfire-radiant',
	);

	if ( in_array( $current_theme, $themes, true ) ) {
		return true;
	} else {
		return false;
	}
}



/**
 * Check for BrightFire Bootstrap themes
 *
 * @return bool
 */
function is_brightfire_bs_theme() {

	// The current theme.
	$current_theme = get_option( 'stylesheet' );

	// List of Bootstrap themes.
	$themes = array(
		'brightfire-aurora',
		'custom-breeden',
		'custom-pelnik',
	);

	if ( in_array( $current_theme, $themes, true ) ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Check for BrightFire Standard themes
 *
 * @return bool
 */
function is_brightfire_standard_theme() {

	// The current theme.
	$current_theme = get_option( 'stylesheet' );

	// List of Bootstrap themes.
	$themes = array(
		'brightfire-aurora',
		'brightfire-illuminate',
		'brightfire-brilliance',
		'brightfire-radiant',
	);

	if ( in_array( $current_theme, $themes, true ) ) {
		return true;
	} else {
		return false;
	}
}

/**
 *
 * Checks text for a link and, if external link, opens in new window
 *
 * @param string $text Text.
 *
 * @return mixed
 */
function launch_external_link_in_new_window( $text ) {

	// Take block of text and search for first link.
	preg_match( '/(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?/', $text, $matches );

	// If we found a link, check to see if it's an external link and, if so, launch in new window.
	if ( $matches && \BrightFireCore\is_external_link( $matches[0] ) ) {
		$text = preg_replace( '/<a/', '<a target="_blank"', $text );
	}

	return $text;

}

/**
 * Get favicon url
 *
 *  Return favicon url if is_white_label site.
 *
 * @param string $favicon_url Favicon URL.
 *
 * @return string  (url to favicon.ico)
 */
function get_favicon_url( $favicon_url ) {

	$url = $favicon_url;

	return $url;

}

/**
 * Formats errors for output on frontend
 *
 * @param string  $error_str Error String.
 * @param boolean $debug Debug.
 *
 * @uses \BrightFireCore\prettyprintr()
 *
 * @return string
 */
function format_bf_error( $error_str = 'Unknown Error Occurred', $debug = false ) {

	$output       = '';
	$debug_output = '';

	$container_styles = 'font-family: sans-serif; font-size: 12px; font-weight: 700; display: block; line-height: 19px; padding: 12px 15px; color: #000; text-align: left; margin: 25px 0 25px 2px; background-color: #fff; border-left: 4px solid #dc3232; border-right: 1px solid #efefef; border-top: 1px solid #efefef; 
	border-bottom: 1px solid #efefef; box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);';

	// If we send a debug param, then pretty print it.
	if ( false !== $debug ) {
		$debug_output .= prettyprintr( $debug, true );
	}

	// Setup our formatted error.
	$output .= '<div  style="' . $container_styles . '"><i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:#b11;"></i> ' . $error_str . $debug_output . '</div>';

	return $output;
}


/**
 *
 * Gets the domain name of the current site from WP
 * and optionally strips www
 *
 * @param bool $strip_www Strip WWW.
 *
 * @return string (domain name)
 */
function get_site_domain_name( $strip_www = false ) {

	$domain_name = site_url();
	$domain_name = wp_parse_url( $domain_name, PHP_URL_HOST );

	if ( $strip_www ) {
		$domain_name = preg_replace( '/www\.(.*)/', '$1', trim( $domain_name ) );
	}

	return $domain_name;
}


/**
 * Formats telephone numbers consistently
 *
 * @param mixed $num Number.
 *
 * @return mixed
 */
function format_phone( $num ) {

	if ( is_array( $num ) ) {
		$phone = $num['phone'];
	} else {
		$phone = $num;
	}

	$phone = preg_replace( '/[^0-9]/', '', $phone );

	$len = strlen( $phone );
	if ( 7 === $len ) {
		$phone = preg_replace( '/([0-9]{3})([0-9]{4})/', '$1-$2', $phone );
	} elseif ( 10 === $len ) {
		$phone = preg_replace( '/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $phone );
	} elseif ( 11 === $len ) {
		$phone = preg_replace( '/[0-9]{1}([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $phone );
	}

	return $phone;
}

/**
 * Is this user a bf_editor
 *
 * @param string $user_type User Type.
 *
 * @return bool
 */
function is_user_type( $user_type ) {

	$user       = \wp_get_current_user();
	$user_roles = $user->roles;

	if ( in_array( $user_type, $user_roles, true ) ) {
		return true;
	} else {
		return false;
	}

}

/**
 * Is BF Super Admin.
 *
 * @return bool
 */
function is_bf_super_admin() {

	if ( current_user_can( 'manage_network' ) ) {
		return true;
	} else {
		return false;
	}

}

/**
 * Is this user a bf_editor
 *
 * @return bool
 */
function is_bf_editor() {

	$user       = \wp_get_current_user();
	$user_roles = $user->roles;

	if ( in_array( 'bf_editor', $user_roles, true ) ) {
		return true;
	} else {
		return false;
	}

}

/**
 * Check to see if a post type exists
 * Gets posts of a specified post type and post status and returns true if exists.
 *
 * TODO: We don't use this ANYWHERE that i can find in our files
 *
 * @param string $post_type                     The name of the post type.
 * @param mixed  $post_status                    Default is "any": All WordPress standard statuses. This does not include "auto-draft" or "inherit".
 *                                               Can be an array of post statuses or a string of a single status.
 *
 * @see https://codex.wordpress.org/Post_Status List of possible post statuses
 *
 * @uses get_posts()
 *
 * @return bool
 */
function has_posts_for_post_type( $post_type, $post_status = 'any' ) {

	// All post statuses.
	if ( 'any' === $post_status ) {
		$post_status = array( 'publish', 'future', 'draft', 'pending', 'private', 'trash' );
	}

	$exists = get_posts(
		array(
			'post_type'   => $post_type,
			'post_status' => $post_status,
		)
	);

	if ( empty( $exists ) ) {
		return false;
	} else {
		return true;
	}
}

/**
 * If value is not an array, create array by splitting text on all commas
 *
 * @param mixed $values Values.
 *
 * @return array
 */
function bf_sanitize_checkbox_array( $values ) {

	$multi_values = ! is_array( $values ) ? explode( ',', $values ) : $values;
	return ! empty( $multi_values ) ? array_map( 'sanitize_text_field', $multi_values ) : array();

}

/**
 * Removes shortcode(s) from any string of text
 *
 * TODO: We don't use this ANYWHERE that i can find in our files
 *
 * @param string $content Content.
 * @param array  $remove_shortcodes Remove Shortcodes.
 *
 * @return mixed|string
 */
function remove_shortcodes( $content = '', $remove_shortcodes = array() ) {

	if ( ! is_array( $remove_shortcodes ) || empty( $content ) ) {
		// phpcs:ignore
		// _doing_it_wrong( 'remove_shortcodes', 'BrightFire remove_shortcodes() requires text to filter and an array of shortcodes to remove.', '4.4' );.
		return $content;
	}

	$pattern = get_shortcode_regex();
	preg_match_all( '/' . $pattern . '/s', $content, $matches );

	if ( is_array( $matches ) ) {

		foreach ( $matches[2] as $key => $shortcode ) {

			if ( in_array( $shortcode, $remove_shortcodes, true ) ) {
				$content = str_replace( $matches[0][ $key ], '', $content );
			}
		}
	}

	return $content;

}

/**
 * Object to Array.
 *
 * @param object $obj Object.
 * @param array  $arr Array.
 * @return mixed
 */
function objToArray( $obj, &$arr = array() ) { // phpcs:ignore

	if ( ! is_object( $obj ) && ! is_array( $obj ) ) {
		$arr = $obj;
		return $arr;
	}

	foreach ( $obj as $key => $value ) {
		if ( ! empty( $value ) ) {
			$arr[ $key ] = array();
			objToArray( $value, $arr[ $key ] );
		} else {
			$arr[ $key ] = $value;
		}
	}
	return $arr;
}
