function init_datePickers( target ) {

    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        };
    }

    // Init color picker on all selector arrays
    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );

        // Basic Datepickers
        parent_element.find('.bf-admin-field.date').each(function () {

            // The attribute we want to check
            // FORMAT on atts for Admin API:
            // htmlentities( json_encode( array('minDate' => 0) ) )
            var attr = $(this).attr('datepicker-properties');

            // Check for atts
            if (typeof attr !== typeof undefined && attr !== false) {
                var properties = $(this).attr('datepicker-properties');
                $(this).datepicker($.parseJSON(properties));
            } else {
                $(this).datepicker();
            }

        });

        // DateRange - Start
        parent_element.find('.bf-admin-field.startdate').each(function () {

            var disabled = [];
            var disabledAttr = $(this).attr('datepicker-disable');

            if (typeof disabledAttr !== typeof undefined && disabledAttr !== false) {
                disabled = $.parseJSON(disabledAttr);
            }

            var endDate = $(this).parents('tr').find('.enddate');
            $(this).datepicker({
                minDate: 0,
                onSelect: function () {
                    $(this).change();
                },
                beforeShowDay: function (date) {
                    var string = $.datepicker.formatDate('mm-dd-yy', date);
                    if (disabled.indexOf(string) == -1) {
                        return [true];
                    } else {
                        return [false, "admin-api-date-scheduled", string];
                    }
                }
            });

            $(this).on('change', function () {
                var startDate = $(this).datepicker('getDate');

                // The minDate for our END DATE should be our START DATE
                endDate.datepicker('option', 'minDate', (startDate ? startDate : 0));

                // TO get the maxDate for our END DATE, let's run some logic to look at currently disabled dates
                endDate.datepicker('option', 'maxDate', getEndDateMax(startDate, disabled));
            });

        });

        // DateRange - End
        parent_element.find('.bf-admin-field.enddate').each(function () {

            var disabled = [];
            var disabledAttr = $(this).attr('datepicker-disable');

            if (typeof disabledAttr !== typeof undefined && disabledAttr !== false) {
                disabled = $.parseJSON(disabledAttr);
            }

            var startDate = $(this).parents('tr').find('.startdate');
            $(this).datepicker({
                minDate: 0, // Min date of today
                onSelect: function () {
                    $(this).change();
                },
                beforeShowDay: function (date) {
                    var string = $.datepicker.formatDate('mm-dd-yy', date);

                    if (disabled.indexOf(string) == -1) {
                        return [true]; // Allowed
                    } else {
                        return [false, "admin-api-date-scheduled", string]; // place 'scheduled' class on
                    }
                }
            });

            $(this).on('change', function () {
                var endDate = $(this).datepicker('getDate');

                // To calculate the minDate for our start date, run through some logic
                startDate.datepicker('option', 'minDate', getStartDateMin(endDate, disabled));

                // The maxDate for maxDate for our START DATE should be our END DATE
                startDate.datepicker('option', 'maxDate', endDate);
            });

        });

    } );

}

function getEndDateMax( startDate, disabled ) {

    if( ! startDate ) {
        return '';
    }

    // Sort our disabled dates in ASC order
    disabled.sort(function(a,b){return new Date(a) - new Date(b);});

    var final = ''; // default shouuld be NO MAX DATE for END DATE

    // Loop through our disabled dates
    $.each( disabled, function( index, disabledDate ) {

        // IF this disabled date is greater than our start date then it has to be the maxDate for our END DATE
        if( new Date( disabledDate ) > new Date( startDate ) ) {

            // Subtract one day for technical correctness
            var pos = new Date( disabledDate );
            var offset = pos.setDate( pos.getDate() - 1 );

            final = new Date( offset );
            return false; // leave $.each()
        }

    });

    return final;

}

function getStartDateMin( endDate, disabled ) {

    if( ! endDate ) {
        return 0;
    }

    // Sort our disabled dates in DESC order
    disabled.sort(function(a,b){return new Date(a) - new Date(b);}).reverse();
    endDate = new Date( endDate );

    var final = 0; // Default minDate for START DAY should be today

    // Loop through our disabled dates
    $.each( disabled, function( index, disabledDate ) {

        if( new Date( disabledDate ) < endDate ) {
            final = new Date( disabledDate );
            return false; // leave $.each()
        }

    });

    return final;

}