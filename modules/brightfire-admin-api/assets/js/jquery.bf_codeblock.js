/** Codeblocks **/
$.bf_codeblock = function( el ) {

    var editorSettings = wp.codeEditor.defaultSettings ? _.clone( wp.codeEditor.defaultSettings ) : {};
    editorSettings.codemirror = _.extend(
        {},
        editorSettings.codemirror,
        {
            indentUnit: 2,
            tabSize: 2

        }
    );
    var editor = wp.codeEditor.initialize( $(el), editorSettings );

    // Make sure we update the textarea value for AJAX changes
    editor.codemirror.on( 'change', function( cm ) {
        var textarea = cm.getTextArea();

        var current_value = cm.getValue();

        $(textarea).val( current_value );
    } );

};

$.fn.bf_codeblock = function() {

    return this.each(function() {
        new $.bf_codeblock(this);  // Instantiate a new repeater object for each output range slider
    });
};

function init_codeblock( target ) {


    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        }
    }

    // Init selectize on all selector arrays
    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );
        parent_element.find('.bf-admin-field.codeblock').bf_codeblock();
    } );

}