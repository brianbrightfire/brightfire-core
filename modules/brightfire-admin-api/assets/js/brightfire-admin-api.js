var $ = jQuery.noConflict();

/* Loads ALL Admin API jQuery instantiations on document.ready() */
$(document).ready(function() {

    $.when(

        init_codeblock(),

        init_colorPickers(),

        init_datePickers(),

        init_limitCounters(),

        init_imagePickers(),

        init_bf_url(),

        init_selectize(),

        init_repeaters(),

        init_range(),

        init_checkboxes()

    ).then( function() {

        $(document).trigger('admin-api-ready');

    });

});