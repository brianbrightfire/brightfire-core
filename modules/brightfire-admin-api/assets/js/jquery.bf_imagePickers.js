function init_imagePickers( target ) {

    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        };
    }

    // Init color picker on all selector arrays
    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );

        // Basic Datepickers
        parent_element.on( 'click', '.set-image' , function() {
            set_image( $(this) );
        } );

        parent_element.on( 'click', '.remove-image', function() {
            remove_image( $(this ) );
        } );

    } );
}

function set_image( container ) {
    row = container.parents('.sr-image');

    var image = wp.media({
        title: 'Upload Image',
        multiple: false
    }).open().on('select', function () {

        var uploaded_image = image.state().get('selection').first().toJSON(); // Image object

        if ( typeof uploaded_image.sizes !== 'undefined' && typeof uploaded_image.sizes.thumbnail !== 'undefined' ) {

            image_url = uploaded_image.sizes.thumbnail.url; // Thumbnail src

        } else {

            image_url = uploaded_image.url; // No thumbnail
        }

        container.css('background-image', 'url(' + image_url + ')');
        container.siblings('input').val(uploaded_image.id);
        container.siblings('input').trigger('propertychange');
        container.removeClass('set-image');
        container.addClass('remove-image');

    });
}

function remove_image( container ) {
    container.css('background-image', '');
    container.siblings('input').val('');
    container.siblings('input').trigger('propertychange');
    container.addClass('set-image');
    container.removeClass('remove-image');
}