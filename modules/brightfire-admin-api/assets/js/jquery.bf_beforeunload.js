/**
 * Warn users of changes if not saving on exit of page on post edit screens
 */
$(document).on('admin-api-ready', function(){
    $('body.post-php .bf-admin-field').each( function() {
        var element = $(this);
        element.on( 'change', function(){
            $(window).on( 'beforeunload.edit-post', function() {
                return true;
            });
        })
    });
});