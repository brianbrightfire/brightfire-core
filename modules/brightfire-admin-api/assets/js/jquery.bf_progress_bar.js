$.fn.bf_progress_bar = function( options ) {

    if( this.length > 1 ) {
        this.each( function() { $(this).bf_progress_bar( options ) } );
        return this;
    }

    var $el = $(this);
    var $container, $meter, $message;

    // Set the default options
    var defaults = {
        support_decimal: false,                       // Display percentage with or without decimals

        progress_complete: 50,                        // Items completed
        progress_total: 107,                          // Total Items to complete

        message : 'This is my progress bar at 46% (50 items of 107 to do completed)'
    };

    // Merge the user defined options with the default options
    options = $.extend(defaults, options);

    // Internal Plugin usage
    var settings = {
      progress_display: 46,
      progress_percentage: 46.72897196261682
    };

    /**
     * Builds our basic markup
     * @returns {$.fn.bf_progress_bar}
     */
    this.initialize = function() {

        // Initial Setup
        $el.addClass('bf-progress-bar-container');
        $el.append('<div class="bf-progress-bar-message">Progress Bar...</div><div class="bf-progress-bar"><span class="meter"></span></div>');
        $container = $el.find('div.bf-progress-bar');
        $meter = $el.find('span.meter');
        $message = $el.find('div.bf-progress-bar-message');

        // Initial Setup
        this.setProgress().running().update();

        // Return the object
        return this;

    };

    /**
     * Update all components
     * @returns {$.fn.bf_progress_bar}
     */
    this.update = function() {

        $meter.css( 'width', settings.progress_percentage + '%' );
        $meter.html( settings.progress_display + '%' );
        $message.html( options.message );

        return this;

    };

    /**
     * Set progress and update
     * @param progress
     * @returns {$.fn.bf_progress_bar}
     */
    this.setProgress = function( progress ) {

        if( progress ) {
            options.progress_complete = progress;
        }


        // Calculate Percentage
        settings.progress_percentage = parseFloat( ( options.progress_complete / options.progress_total ) * 100 );

        // Cap at 100
        settings.progress_percentage = Math.min( 100, settings.progress_percentage );

        if( options.support_decimal ) {
            settings.progress_display = settings.progress_percentage.toFixed(2);
        } else {
            settings.progress_display = settings.progress_percentage.toFixed(0);
        }

        this.update();

        return this;

    };

    /**
     * Set progress and update
     * @param progress
     * @returns {$.fn.bf_progress_bar}
     */
    this.setTotal = function( total ) {

        if( total ) {
            options.progress_total = total;
        }


        // Calculate Percentage
        settings.progress_percentage = parseFloat( ( options.progress_complete / options.progress_total ) * 100 );

        // Cap at 100
        settings.progress_percentage = Math.min( 100, settings.progress_percentage );

        if( options.support_decimal ) {
            settings.progress_display = settings.progress_percentage.toFixed(2);
        } else {
            settings.progress_display = settings.progress_percentage.toFixed(0);
        }

        this.update();

        return this;

    };

    /**
     * Set Message and update
     * @param message
     * @returns {$.fn.bf_progress_bar}
     */
    this.setMessage = function( message ) {

        options.message = message;
        this.update();

        return this;

    };

    /**
     * Set state to running
     * @returns {$.fn.bf_progress_bar}
     */
    this.running = function() {
        $meter.addClass('running').removeClass('error').removeClass('complete');

        return this;
    };

    /**
     * Set state to complete
     * @returns {$.fn.bf_progress_bar}
     */
    this.complete = function() {
        $meter.addClass('complete').removeClass('error').removeClass('waiting');

        return this;
    };

    /**
     * Set state to error
     * @returns {$.fn.bf_progress_bar}
     */
    this.error = function() {
        $meter.addClass('error').removeClass('complete').removeClass('waiting');

        return this;
    };

    /**
     * INIT THE PLUGIN
     */
    return this.initialize();
};