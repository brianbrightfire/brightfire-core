/** REPEATER fields **/
$.bf_repeater = function( el ) {
    var repeater = $(el); // element class selector the $.bf_repeater function is called on
    var fieldsets = {}; // Holds a reference too all fieldsets and their parts

    // Private Methods
    var methods = {

        /**
         * MAKE IT SO
         */
        init: function() {
            methods.config();

            repeater.find('.bf-repeater-set').each( function() {

                var repeater_set = $(this);

                methods.renderFieldset( repeater_set );
            });

            methods.evh_Customizer();

        },

        /**
         * Configure our repeater object settings
         */
        config: function() {

            // PHP Callbacks (generates repeater-inside content)
            repeater.callbacks = repeater.data('callbacks');

            // JS Callbacks (fired on new repeater add)
            repeater.js_callbacks = repeater.data('js-callbacks');

            // Basic config data
            repeater.config = repeater.data('config');

            // collapse
            if( true === repeater.config.collapse ) {
                repeater.collapse = true;
            }

            // Sortable
            if( true === repeater.config.sortable ) {
                repeater.sortable = true;
            }

            // Oneopen
            if( true === repeater.config.oneopen ) {
                repeater.oneopen = true;
            }

            // Cleanup Markup
            repeater.removeAttr('data-config data-callbacks data-js-callbacks');

        },

        /**
         * Render Fieldset and Parts
         * @param el
         */
        renderFieldset: function( el ) {

            // Register Repeater Parts
            var this_set_id = methods.uuid();

            var fieldset = {};
                fieldset.parent = el;
                fieldset.top = el.find('.bf-repeater-top');
                fieldset.inputs = el.find('input, textarea, select');
                fieldset.inside = el.find('.bf-repeater-inside');

            // Build Repeater Actions
            var actions = $('<div></div>', {
                "class": 'bf-admin-api bf-repeater-actions'
            });
            actions.append('<a href="javascript: void(0);" class="repeater-button del-bf-repeater"><i class="fa fa-trash-o"></i></a>');
            actions.append('<a href="javascript: void(0);" class="repeater-button add-bf-repeater"><i class="fa fa-plus-square-o"></i></a>');

            fieldset.top.append( actions );
            fieldset.actions = el.find('.bf-repeater-actions');

            fieldset.add = actions.find('.add-bf-repeater').not('.disabled');
            fieldset.del = actions.find('.del-bf-repeater').not('.disabled');

            if( true === repeater.collapse ) {
                actions.prepend('<a href="javascript: void(0);" class="repeater-button collapse-bf-repeater"><i class="fa fa-sort-down"></i></a>');
                fieldset.collapse = actions.find('.collapse-bf-repeater').not('.disabled');
                fieldset.inside.hide();
            }

            if( true === repeater.sortable ) {
                fieldset.top.addClass('bf-repeater-handle');
            }

            // Register Repeater ID
            el.attr('data-fieldset-id', this_set_id );

            // Add to our fieldsets object
            fieldsets[this_set_id] = fieldset;

            methods.evh( fieldset.parent );

        },

        /**
         * EVENT HANDLERS
         * @param el
         */
        evh: function( el ) { // 'el' is the parent element to find evh within

            // Get our fieldset id
            var fieldset_id = methods.get_fieldset_id(el);
            var fieldset = fieldsets[fieldset_id];

            // Attach our Event handlers
            fieldset.add.on( 'click', function( event ) {
                event.preventDefault();
                methods.evh_add( fieldset.parent );
            });
            fieldset.del.on( 'click', function( event ) {
                event.preventDefault();
                if( ! $(this).hasClass('disabled') ) {
                    methods.evh_remove( fieldset.parent );
                }
            });
            fieldset.inputs.on( 'change propertychange', function( event ) {
                methods.evh_triggerPropertyChange( fieldset );
            });

            // If collapse
            if( true === repeater.collapse ) {
                fieldset.collapse.on('click', function (event) {
                    event.preventDefault();
                    methods.evh_collapse(fieldset.parent);
                });
            }

            // If sortable
            if( true === repeater.sortable ) {
                methods.evh_sort();
            }
            el.trigger('register-repeater-set');

        },

        /**
         * EVENTS: Customizer
         */
        evh_Customizer: function() {

            // Trigger a propertychange on our first repeater input to force a
            // refresh when repeaters are removed or sorted
            // we don't need one on add, because on initial add, we will not
            // have any data to display yet
            repeater.on('bf-repeater-sort bf-repeater-remove', function() {
                var triggerEle = repeater.find('input, select').filter(':first');
                triggerEle.trigger('propertychange');
            } );

        },

        /**
         * INIT: Sortable
         */
        evh_sort: function() {

            $(repeater).sortable({
                cursor: 'move',
                axis : 'y',
                handle : '.bf-repeater-handle',
                distance: 2,
                opacity: 0.8,
                tolerance: 'pointer',
                start: function(e, ui){
                    ui.placeholder.height(ui.item.height());
                },
                update: function(e,ui){
                    repeater.trigger('bf-repeater-sort');
                }
            });

        },

        /**
         * EVENT: Add Fieldset
         * @param el
         */
        evh_add: function( el ) {

            // Setup the PostVars for AJAX call
            var postVars = {
                action: 'admin_api_repeater_add',
                config: repeater.config,
                callbacks: repeater.callbacks
            };

            // Send the AJAX Request
            $.post( ajaxurl, postVars, function ( response ) {

                var newRepeater = $(response);

                // Put our new set of repeaters in
                if( el ) {
                    el.after(newRepeater); // Add after triggered element
                } else {
                    repeater.append(newRepeater); // append to set wrapper
                }

                // Call each of our js callbacks
                $.each( repeater.js_callbacks, function( index, func ){
                    if('' !== func && func.length !== 0 ) {
                        try {
                            window[func]();
                        }
                        catch(err) {
                            console.warn( err );
                        }
                    }
                });

                // Attach our handlers to the new element
                methods.renderFieldset( newRepeater );

                // Open the new one
                if( true === repeater.collapse ) {
                    methods.evh_collapse(newRepeater);
                }

            }).error( function( response ) {
                console.warn( response.responseText );
            });

            // Trigger Event
            repeater.trigger('bf-repeater-add');

        },

        /**
         * EVENT: Remove Fieldset
         * @param el
         */
        evh_remove: function( el ) {

            var set_id = methods.get_fieldset_id(el);

            // Cleanup fieldsets
            delete fieldsets[set_id];

            el.remove();
            repeater.trigger('bf-repeater-remove');
            if( ! methods.checkRepeaters() ) {
                methods.evh_add(); // Add a starting repeater if none left
            }

        },

        /**
         * EVENT: Collapse Fieldset
         * @param el
         */
        evh_collapse: function( el ) {

            // Get the fieldset
            var set_id = methods.get_fieldset_id(el);
            var fieldset = fieldsets[set_id];

            if( true === repeater.oneopen && ! fieldset.parent.hasClass( 'open' ) ) {

                var openRepeaters = repeater.find('.bf-repeater.open');

                openRepeaters.each( function() {
                    var oRepeaterId = $(this).attr('data-fieldset-id');
                    var oRepeater = fieldsets[oRepeaterId];
                        oRepeater.collapse.html('<i class="fa fa-sort-down"></i>');
                        oRepeater.inside.slideUp( 150 );
                        oRepeater.parent.removeClass('open');
                });

            }

            if( fieldset.parent.hasClass('open') ) {
                fieldset.inside.slideUp( 150 );
                fieldset.collapse.html('<i class="fa fa-sort-down"></i>');
                fieldset.parent.removeClass('open');
            } else {
                fieldset.inside.slideDown(150);
                fieldset.collapse.html('<i class="fa fa-sort-up"></i>');
                fieldset.parent.addClass('open');
            }
        },


        /**
         * Counts active fieldsets
         * @returns {*}
         */
        checkRepeaters: function() {
            return Object.keys(fieldsets).length;
        },

        evh_triggerPropertyChange: function( fieldset ) {
            fieldset.parent.trigger('bf-repeater-update');
        },

        /**
         * Generate a Random 16-char UUID
         * @returns {*}
         */
        uuid: function() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + s4() + s4();
        },

        /**
         * Gets the Fieldset ID Index of an element
         * @param el
         * @returns {*}
         */
        get_fieldset_id: function( el ) {
            return el.attr('data-fieldset-id');
        }

    };

    // Initialize the Repeater
    methods.init();

};

$.fn.bf_repeater = function() {

    return this.each(function() {
        new $.bf_repeater(this);  // Instantiate a new repeater object for each output repeater
    });
};

function init_repeaters( target ) {

    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        }
    }

    // Init selectize on all selector arrays
    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );
        parent_element.find('.bf-admin-api.bf-repeater-container').bf_repeater();
    } );

}