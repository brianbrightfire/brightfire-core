function init_checkboxes( target ) {

    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        }
    }

    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );
        parent_element.find('.bf-admin-api.checkboxes').each( function() {
            var self = $(this);
            var current_values = self.find('input.checkbox_multi').val();

            if( '' !== current_values ) {

                $.each(current_values.split(','), function () {
                    if( this.length !== 0 ) {
                        self.find('input[type=checkbox][value=' + this + ']').prop('checked', true);
                    }
                });

            }
        } );
    } );

    // Allow click events on checkbox labels to toggle the checkbox if not disabled
    $( document ).on( "click", "div.checkbox-option > span.checkbox-label", function() {

        var checkbox = $(this).siblings('input[type=checkbox]');

        if( checkbox.prop('disabled') ) {
            return false;
        }

        if( checkbox.prop('checked') ) {
            checkbox.prop('checked',false);
            checkbox.trigger('propertychange');
        } else {
            checkbox.prop('checked',true);
            checkbox.trigger('propertychange');
        }

    });

    // Any time a checkbox experiences a change or property change, update our hidden input
    $( document ).on( "change propertychange", "div.checkbox-option > input[type=checkbox]", function() {

        update_checkbox_field( $(this).closest('.bf-admin-api.checkboxes') );

    });

    $( document ).on( "change propertychange", "input.checkbox_multi", function() {

        update_checkbox_field( $(this).closest('.bf-admin-api.checkboxes') );

    } );

}

function update_checkbox_field( wrapper ) {

    var value = [];

    wrapper.find('input[type=checkbox]').each( function() {

        if( $(this).prop('checked') ) {
            value.push($(this).val());
        }

    });

    // Make a string
    var new_value = value.join( ',' );
    wrapper.find('input.checkbox_multi').val( new_value );

}