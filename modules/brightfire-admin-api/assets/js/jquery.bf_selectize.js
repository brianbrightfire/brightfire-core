var bf_selectize = {};

/**
 * Creates a GUID to be used
 *
 * TODO: move into js assets or utilities
 *
 * @returns {*}
 */
function makeGUID() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + s4() + s4();
}

/**
 * Helper function to retrive a selectize JS object (instance)
 *
 * @param el : element to return instance for
 * @returns {*}
 */
function bf_selectize_get_instance( el ) {

    return bf_selectize[ el.attr('data-instance-index') ];

}

function add_instance_to_global( instance ) {
    if( instance.length ) { // If any were created, add them to our global
        $.each( instance, function( index, instance ) {
            var instance_id = instance.selectize.$input.attr('data-instance-index');
            bf_selectize[instance_id] = instance;
        });
    }
}

/** SELECTIZE fields **/
$.fn.bf_selectize = function() {
    var this_selectize = $(this).selectize({
        plugins: ['remove_button', 'drag_drop'],
        allowEmptyOption: true,
        onInitialize: function() {
            this.$input.attr( 'data-instance-index', makeGUID() );
            this.$input.trigger( 'selectizeRendered' );
        },
        onChange: function( value ) {
            this.$input.trigger( 'propertychange' );
            if( ! this.$input.val() ) {
                this.$input.html('<option value="" selected="selected"></option>');
            }
        },
        render: {
            option: function (data, escape) {

                var markup = '<div class="selectize-rich-option">';

                if( '' !== data.swatch && typeof data.swatch !== 'undefined' ) {
                    markup += '<div class="selectize-swatch background-' + data.swatch + '"></div>';
                }

                if( '' !== data.preview ) {
                    markup += '<div class="selectize-preview background-' + data.preview +'">' +
                        '<span class="selectize-name">' + escape(data.name) + ': This is a preview of your text</span>' +
                        '<span class="selectize-desc">' + data.desc + '</span>' +
                        '</div>';
                    markup += '</div>'; // selectize-rich-option

                    return markup;
                }

                markup += '<span class="selectize-name">' + escape(data.name) + '</span>' +
                          '<span class="selectize-desc">' + data.desc + '</span>' +
                          '</div>';

                return markup;
            },
            item: function( data, escape ) {

                var category_class = '';
                if( '' !== data.category && typeof data.category !== 'undefined' ) {
                    category_class = 'category ' + data.category;
                }

                return '<div class="selectize-rich-item ' + category_class + '">' + escape(data.name) + '</div>';
            }
        }
    });

    add_instance_to_global( this_selectize );
};

$.fn.fa_selectize = function () {
    var this_selectize = $(this).selectize({
        preload: 'focus',
        onChange: function( value ) {
            this.$input.trigger('propertychange');
        },
        valueField: 'id',
        labelField: 'name',
        searchField: ['name', 'filter'],
        allowEmptyOption: true,
        onInitialize: function() {
            this.$input.attr( 'data-instance-index', makeGUID() );
        },
        render: {
            option: function (data, escape) {
                return '<div class="fontawesome-select"><i class="fa-' + escape(data.id) + ' fa"></i> ' + escape(data.name) + '</div>';
            },
            item: function (data, escape) {
                return '<div class="fontawesome-select"><i class="fa-' + escape(data.id) + ' fa"></i> ' + escape(data.name) + '</div>';
            }
        },
        load: function (query, callback) {
            $.ajax({
                url: '/wp-content/mu-plugins/brightfire-core/assets/fontawesome.json',
                type: 'GET',
                error: function () {
                    callback();
                },
                success: function (res) {

                    // Sort icons in alpha order by name
                    res.icons.sort( function( a, b ) {
                        a = a.name.toLowerCase();
                        b = b.name.toLowerCase();

                        return a < b ? -1 : a > b ? 1 : 0;
                    });

                    callback(res.icons);
                }
            });
        }
    });

    add_instance_to_global( this_selectize );
};

$.fn.url_selectize = function() {
    var this_selectize = $(this).selectize({
        preload: 'focus',
        onChange: function( value ) {
            this.$input.trigger('propertychange');
        },
        valueField: 'id',
        labelField: 'name', 
        searchField: ['name','link','id'],
        allowEmptyOption: true,
        onInitialize: function() {
            this.$input.attr( 'data-instance-index', makeGUID() );
        },
        render: {
            option: function (data, escape) {
                return '<div class="wp-page-select">' + escape(data.name) + '</div>';
            },
            item: function (data, escape) {
                return '<div class="wp-page-select">' + escape(data.name) + '</div>';
            }
        },
        load: function (query, callback) {
            // Send the AJAX Request
            $.post( ajaxurl, {action: 'admin_api_load_posts_pages'}, function ( response ) {
                callback(response);
            })
            .error( function( res ) {
                callback();
            });
        }
    });

    add_instance_to_global( this_selectize );
};

function init_selectize( target ) {

    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#customize-theme-controls",
                not: ""
            },
            2: {
                selector: "#widgets-right",
                not: ""
            }

        }
    }

    // Init selectize on all selector arrays
    $.each( selector_array, function( index, object ) {

        var parent_element = $( object.selector ).not( object.not );
        parent_element.find( 'select.selectize' ).bf_selectize();
        parent_element.find('select.fontawesome-select').fa_selectize();
        parent_element.find('select.wp-page-select').url_selectize();

    } );

}
function init_child_selectize( parent ) {
    parent.find( 'select.selectize' ).bf_selectize();
    parent.find('select.fontawesome-select').fa_selectize();
    parent.find('select.wp-page-select').url_selectize();
}