function init_limitCounters( target ) {

    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        };
    }

    // Init color picker on all selector arrays
    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );
        parent_element.find("[data-limit!=''][data-limit]").each( function() { build_limited_field( $(this) ) })

    } );

}

function build_limited_field( el ) {

    // Count String Lenth (initial)
    var len = el.val().length;
    var lim = el.attr('data-limit');

    // Build Counter Text
    var countWrapper = $('<div>');
    countWrapper.addClass('input-counter');
    countWrapper.html('<span class="len">' + len + '</span> of ' + lim + ' available characters used');

    // Add our counter after the field
    el.after(countWrapper);

    // Build Event Listener
    el.on('keyup keydown focus blur', function() {

        var str = $(this).val();
        var len = str.length;
        var lim = $(this).attr('data-limit');

        // If we're over the limit, truncate
        if( len > lim ) {
            $(this).val( str.substring(0,lim) );
            $(this).siblings('.input-counter').children('span.len').html(lim).css('color','#bf1111');
        } else if ( len == lim ) {
            $(this).siblings('.input-counter').children('span.len').html(len).css('color','#bf1111');
        } else {
            $(this).siblings('.input-counter').children('span.len').html(len).removeAttr('style');
        }

    });

}