function init_colorPickers( target ) {


    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        };
    }

    // Init color picker on all selector arrays
    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );
        parent_element.find( '.bf-admin-field.color-picker' ).wpColorPicker( { change: function(){ $(this).trigger('propertychange'); } } );

    } );
}

// Properly trigger clear
$(document).on( 'click', '.wp-picker-clear', function () {
    $(this).siblings( '.color-picker' ).trigger('change');
});

$(document).on( 'click', '.color-picker-toggle', function() {
    $(this).siblings('.color-picker-helper').toggle();
});

$(document).on( 'click', '.color-swatch', function() {
    var target = $(this).closest('.input-wrapper').find('input[type="text"]');
    $(target).val( $(this).css('background-color') ).trigger('change');

    $(this).closest('.color-picker-helper').toggle();
});