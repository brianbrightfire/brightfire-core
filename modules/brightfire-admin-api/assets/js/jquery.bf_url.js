/** WP URL Fields **/
$.wp_url_search = function( el ) {

    var search = $(el);
    search.delay = 0;
    search.postsLoaded = false;
    search.value = search.val();
    search.userInput = search.value;

    var structure = {};

    var methods = {

        init: function() {

            // Our Control Wrapper
            structure.wrapper = $('<div/>');
            structure.wrapper.addClass('wp-url-search-control');

            // Our User Input
            structure.input = $('<input/>');
            structure.input
                .addClass('wp-url-user-input')
                .addClass('widefat')
                .addClass('bf-admin-field')
                .attr('type','text')
                .val( search.value );

            // Our Dropdown
            structure.dd = $('<div/>');
            structure.dd
                .addClass('wp-url-search-dropdown')
                .hide()
                .html('my dropdown');

            // Loading
            structure.loading = $('<div/>');
            structure.loading
                .addClass('wp-url-search-loading')
                .addClass('loading')
                .html('<div class="text"><span class="spinner spinner-dark is-active"></span> Loading Posts and Pages</div>');

            // Add all the lovely markup
            structure.input.appendTo(structure.wrapper);
            structure.dd.appendTo(structure.wrapper);
            structure.loading.appendTo(structure.wrapper);
            search.after(structure.wrapper);

            // Hide our original input
            search.attr('type','hidden');

            // engage our event handlers
            methods.evh();

            // load posts and pages
            methods.loadPosts();

        },

        evh: function() {

            // Attach our Event handlers
            structure.input.on( 'keyup focus', function( event ) {

                var value = $(this).val();

                search.userInput = value;

                methods.delay( function() {
                    methods.spotlight( value );
                }, 200);

            } );

            // Hide the dropdown on blur
            structure.input.on( 'blur', function( event ) {
                methods.delay( function() {
                    structure.dd.hide();
                    methods.sanitize();
                }, 200 );

            } );

            // Data-Link click
            $(document).on( 'click', '.wp-url-value-link', function( event ) {

                var link = $(this).attr('data-link');
                var name = $(this).attr('data-name');

                var translator = $('<div/>');
                translator.html(link).text();

                var decodedLink = translator.text();

                var container = $(this).parents('div.wp-url-search-control');
                container.find('input.wp-url-user-input').val( decodedLink );

                search.userInput = decodedLink;
                methods.sanitize();


            } );

        },

        delay: function( callback, ms ) {
            clearTimeout ( search.delay );
            search.delay = setTimeout( callback, ms );
        },

        spotlight: function( val ) {

            if( search.postsLoaded ) {

                search.results = [];

                // Do the search
                for( var i = 0; i < search.posts.length; i++ ) {
                    // Search Name
                    if( search.posts[i].name.toLowerCase().indexOf( val.toLowerCase() ) >= 0 || search.posts[i].link.toLowerCase().indexOf( val.toLowerCase() ) >= 0 ) {
                        search.results.push( search.posts[i] );
                    }
                }

                methods.sortResults();

                // Output Results
                var searchResults = $('<div/>');
                searchResults.addClass('results');

                for( var j = 0; j < search.results.length; j++ ) {

                    var icon = '';
                    if( 'page' == search.results[j].type ) {
                        icon = '<span class="dashicons dashicons-admin-page"></span> ';
                    } else {
                        icon = '<span class="dashicons dashicons-admin-post"></span> ';
                    }

                    var newLink = $('<a/>');
                    newLink.attr('data-link',search.results[j].link);
                    newLink.attr('data-name',search.results[j].name);
                    newLink.html( icon + search.results[j].name/* + '<i>' + search.results[j].link + '</i>'*/).text();
                    newLink.addClass('wp-url-value-link');

                    newLink.appendTo(searchResults);

                }

                // Update Dropdown and show it
                structure.dd.html(searchResults);
                structure.dd.show();

            } else {
                console.warn('Hold on a second, hoss - still loading posts and pages');
            }

        },

        loadPosts: function() {

            // Setup the PostVars for AJAX call
            var postVars = {
                action: 'admin_api_load_posts_pages'
            };

            // Send the AJAX Request
            $.post( ajaxurl, postVars, function ( response ) {

                search.posts = response;
                search.postsLoaded = true;
                structure.loading.fadeOut();

            })
                .error( function( response ) {
                    console.warn( response.responseText );
                });

        },

        sortResults: function( field, reverse, primer ) {

            var sort_by = function(field, reverse, primer){

                var key = primer ?
                    function(x) { return primer(x[field]); } :
                    function(x) { return x[field]; };

                reverse = !reverse ? 1 : -1;

                return function (a, b) {
                    return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
                };
            };

            search.results.sort(sort_by('name', false, function(a){ return a.toUpperCase(); }));

        },

        sanitize: function() {

            // REGEX URL Validation
            if(/^([a-z]([a-z]|\d|\+|-|\.)*):(\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?((\[(|(v[\da-f]{1,}\.(([a-z]|\d|-|\.|_|~)|[!\$&'\(\)\*\+,;=]|:)+))\])|((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=])*)(:\d*)?)(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*|(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)){0})(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(search.userInput)) {
                search.val(search.userInput);
            } else if( '' === search.userInput ) {
                search.val('');
            } else {
                // Invalid URL
                structure.input.val(search.val());
            }

            search.trigger('propertychange');

        }

    };

    methods.init();

};

$.fn.wp_url_search = function() {

    return this.each(function() {
        new $.wp_url_search(this);  // Instantiate a new repeater object for each output repeater
    });
};

function init_bf_url( target ) {

    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        }
    }

    // Init selectize on all selector arrays
    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );
        parent_element.find('.bf-admin-field.wp_url').wp_url_search();
    } );
}