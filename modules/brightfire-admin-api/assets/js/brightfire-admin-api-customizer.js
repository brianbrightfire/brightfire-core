var $ = jQuery.noConflict();

/* Loads Admin API jQuery instantiations as needed for Customizer */
$(document).on( 'ready', function() {


    // Instantiate any lone settings ( Stellar Row settings, etc )
    init_selectize( '.customize-control-selectize' );

    // We can trigger this off the get-go for anything
    // which might be hooked on the 'admin-api-ready'
    // function...
    $(document).trigger('admin-api-ready');

    // When we add or update a widget, set up the fields on that widget only
    $(document).on( 'widget-updated widget-added', function( event, widget ) {

        setup_fields( widget );

    });

});

/* Given a parent, instantiate our fields ONLY within that parent */
function setup_fields( widget ) {

    var target_selector = '#' + widget.attr('id');

    init_codeblock( target_selector );

    init_colorPickers( target_selector );

    init_datePickers( target_selector );

    init_limitCounters( target_selector );

    init_imagePickers( target_selector );

    init_bf_url( target_selector );

    init_selectize( target_selector );

    init_repeaters( target_selector );

    init_range( target_selector );

    init_checkboxes( target_selector );

}