/** RANGE Sliders **/
$.bf_range = function( el ) {
    var rangeSlider = $(el);
    rangeSlider.id = rangeSlider.attr('data-slider-id');
    rangeSlider.units = rangeSlider.attr('data-slider-units');
    rangeSlider.value = rangeSlider.val();
    rangeSlider.curLabel = $('span[data-slider-id="' + rangeSlider.id + '"]');

    var methods = {

        init: function() {

            rangeSlider.on('input change', function( event ) {
                rangeSlider.value = rangeSlider.val();
                methods.update();
            });

            methods.update();

        },

        update: function() {
            rangeSlider.curLabel.html( rangeSlider.value + ' ' + rangeSlider.units );
        }

    };

    methods.init();
};

$.fn.bf_range = function() {

    return this.each(function() {
        new $.bf_range(this);  // Instantiate a new repeater object for each output range slider
    });
};

function init_range( target ) {


    var selector_array = {};

    if( target ) {
        selector_array = {
            0: {
                selector: target,
                not: ""
            }
        }
    } else {
        // Building this selector array allows us to
        // avoid using the eval() function as it is
        // insecure
        selector_array = {
            0: {
                selector: "body",
                not: ".wp-customizer, .widgets-php"
            },
            1: {
                selector: "#widgets-right",
                not: ""
            }

        }
    }

    // Init selectize on all selector arrays
    $.each( selector_array, function( index, object ) {
        var parent_element = $( object.selector ).not( object.not );
        parent_element.find('.bf-admin-field.range').bf_range();
    } );

}