<?php
/**
 * BrightFire Admin API.
 *
 * @package BrightFireCore
 */

namespace BrightFire\Admin_API;

define( 'BF_ADMIN_API_CAPABILITY', 'manage_options' );
define( 'BF_ADMIN_API_INC', dirname( __FILE__ ) . '/includes' );
define( 'BF_ADMIN_API_ASSETS', plugin_dir_url( __FILE__ ) . 'assets' );
define( 'BF_ADMIN_API_FIELDS_DIR', BF_ADMIN_API_INC . '/fields' );
define( 'BF_ADMIN_API_SANITIZE_DIR', BF_ADMIN_API_INC . '/sanitize' );

// Load Requirements.
require_once BF_ADMIN_API_INC . '/functions.php';                           // Base Functions.
require_once BF_ADMIN_API_INC . '/presets.php';                             // API Presets.
require_once BF_ADMIN_API_INC . '/class-bf-admin-api-build.php';    // Form Class.
require_once BF_ADMIN_API_INC . '/class-bf-admin-api-fields.php';   // Fields Class.
require_once BF_ADMIN_API_INC . '/class-bf-admin-api-sanitize.php'; // Sanitation Class.
require_once BF_ADMIN_API_INC . '/ajax.php';                                // AJAX.
require_once BF_ADMIN_API_INC . '/fontawesome/fontawesome-yaml.php';        // Fontawesome.

// Field Types.
require_once BF_ADMIN_API_FIELDS_DIR . '/interface-admin-api-field-base.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-base.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-text.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-button.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-checkbox-multi.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-codeblock.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-date.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-daterange.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-email.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-hidden.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-number.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-password.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-phone.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-range.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-repeater.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-select.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-textarea.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-url.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-wp-color.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-wp-image.php';
require_once BF_ADMIN_API_FIELDS_DIR . '/class-bf-admin-api-field-wp-editor.php';

// Sanitization Types.
require_once BF_ADMIN_API_SANITIZE_DIR . '/interface-admin-api-sanitize-base.php';
require_once BF_ADMIN_API_SANITIZE_DIR . '/class-bf-admin-api-sanitize-base.php';
require_once BF_ADMIN_API_SANITIZE_DIR . '/class-bf-admin-api-sanitize-boolean.php';
require_once BF_ADMIN_API_SANITIZE_DIR . '/class-bf-admin-api-sanitize-date.php';
require_once BF_ADMIN_API_SANITIZE_DIR . '/class-bf-admin-api-sanitize-email.php';
require_once BF_ADMIN_API_SANITIZE_DIR . '/class-bf-admin-api-sanitize-integer.php';
require_once BF_ADMIN_API_SANITIZE_DIR . '/class-bf-admin-api-sanitize-phone.php';
require_once BF_ADMIN_API_SANITIZE_DIR . '/class-bf-admin-api-sanitize-url.php';
require_once BF_ADMIN_API_SANITIZE_DIR . '/class-bf-admin-api-sanitize-callback.php';

// Load Examples if in DEV.
if ( defined( 'BF_ENV' ) && 'DEV' === BF_ENV ) {
	require_once BF_ADMIN_API_INC . '/examples/load.php';
}

// Instantiate the admin API.
add_action( 'init', __NAMESPACE__ . '\load_admin_api' );
