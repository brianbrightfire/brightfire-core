<?php
/**
 * BrightFire Admin API AJAX.
 *
 * @package BrightFireCore
 */

namespace BrightFire\Admin_API;

/**
 * AJAX Repeater Add.
 */
function ajax_repeater_add() {

	// TODO: SECURITY - Need nonce verification.
	$callbacks = stripslashes_deep( $_POST['callbacks'] ); // phpcs:ignore

	$response = build_repeater( $callbacks );

	wp_send_json( $response );

}
add_action( 'wp_ajax_admin_api_repeater_add', __NAMESPACE__ . '\ajax_repeater_add' );

/**
 * AJAX Handler for wp_url search.
 */
function ajax_load_posts_pages() {

	$response = array();

	$allowed_post_types = array(
		'page',
		// 'post',
					'bf_location',
	);

	$args  = array(
		'posts_per_page' => -1,
		'post_type'      => $allowed_post_types,
	);
	$posts = get_posts( $args );

	foreach ( $posts as $post ) {

		$response[] = array(
			'id'       => $post->ID,
			'name'     => get_the_title( $post ),
			'type'     => '',
			'dashicon' => 'dashicons dashicons-admin-page',
		);

	}

	wp_send_json( $response );

}
add_action( 'wp_ajax_admin_api_load_posts_pages', __NAMESPACE__ . '\ajax_load_posts_pages' );
