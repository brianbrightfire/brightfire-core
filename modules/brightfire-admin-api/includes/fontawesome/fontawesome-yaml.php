<?php
/**
 * Font Awesome YAML Parser.
 *
 * @package fontawesome
 */

/**
 * Call this function to get fontawesome in whichever format you prefer
 *
 * @param null $request Request.
 *
 * @return array|mixed|null|object|string
 */
function get_fontawesome( $request = null ) {
	$rtn    = null;
	$json   = get_fontawesome_json();
	$array  = json_decode( $json, true );
	$object = json_decode( $json );

	switch ( $request ) {
		case 'object':
			$rtn = $object;
			break;
		case 'array':
			$rtn = $array;
			break;
		case 'json':
			$rtn = $json;
			break;
		default:
			$rtn = array(
				'json'  => $json,
				'array' => $array,
			);
			break;
	}

	return $rtn;
}

/**
 * Reads json out of our file
 *
 * @return string
 */
function get_fontawesome_json() {
	$json = wp_remote_get( BF_CORE_ASSETS . 'js/fontawesome.json' );

	return $json;
}

/**
 * Run this to parse the new YAML file and rebuild our JSON ( Run each time we update FontAwesome )
 * Predicated that you have pulled the new .yaml file contents from FA Repo
 */
function fontawesome_build_utility() {
	require_once BF_ADMIN_API_INC . '/fontawesome/spyc_yaml_parser.php';
	$icons_array = Spyc::YAMLLoad( BF_ADMIN_API_INC . '/fontawesome/fontawesome.yml' );

	echo '<h1>JSON Output</h1><br><i>This has been inserted into <code>fontawesome.json</code></i>';

	$json = wp_json_encode( $icons_array );

	// TODO: Needs to be refactored to utilize WP_Filesystem.
	file_put_contents( BF_CORE_PATH . '/assets/fontawesome.json', $json ); // phpcs:ignore
}
