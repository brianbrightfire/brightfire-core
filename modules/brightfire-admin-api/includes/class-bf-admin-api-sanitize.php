<?php
/**
 * BrightFire Admin API Sanitize.
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Sanitize
 * Responsibility: Global configuration of Sanitize types and run function
 */
class BF_Admin_API_Sanitize {

	/**
	 * Our Sanitize Types
	 *     array index should be registered sanitation type.
	 *     array value should be class name.
	 *
	 * @return array
	 */
	public static function bf_admin_api_sanitize_array() {
		$bf_admin_api_sanitize = array(
			'boolean' => 'BF_Admin_API_Sanitize_Boolean',
			'date'    => 'BF_Admin_API_Sanitize_Date',
			'email'   => 'BF_Admin_API_Sanitize_Email',
			'integer' => 'BF_Admin_API_Sanitize_Integer',
			'phone'   => 'BF_Admin_API_Sanitize_Phone',
			'url'     => 'BF_Admin_API_Sanitize_URL',
		);

		$bf_admin_api_sanitize = apply_filters( 'bf_admin_api_sanitize', $bf_admin_api_sanitize );

		return $bf_admin_api_sanitize;
	}

	/**
	 * Runs a proper sanitation and returns santized value
	 *
	 * @param mixed $sanitize Sanitize.
	 * @param mixed $value Value.
	 *
	 * @return string
	 */
	public static function bf_admin_api_sanitize_run( $sanitize, $value ) {

		$sanitized_value = ( ! empty( $sanitize ) ? BF_Admin_API_Sanitize_Base::run( $sanitize, $value ) : BF_Admin_API_Sanitize_Base::sanitize( $value ) );

		return $sanitized_value;
	}

}
