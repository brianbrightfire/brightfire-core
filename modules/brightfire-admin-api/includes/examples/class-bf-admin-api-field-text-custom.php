<?php
/**
 * BrightFire Admin API Example Field.
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Field_Text
 *
 * This class generates a specific field
 *
 * Responsibility: Generate markup for a specific input field.
 */
class BF_Admin_API_Field_Text_Custom extends BF_Admin_API_Field_Base {

	/**
	 * Type.
	 *
	 * @var string $type
	 */
	private $type = 'text_custom';

	/**
	 * Creates a Text Field
	 *
	 * @param int    $id ID.
	 * @param array  $field_config Field Config.
	 * @param string $value Value.
	 *
	 * @return mixed
	 */
	public function create_field( $id = 0, $field_config = array(), $value = '' ) {

		// Set Up Everything.
		self::setup( $id, $field_config, $value );

		// The Field HTML.
		$output = '<input type="text" id="' . esc_attr( $id ) . '" name="' . esc_attr( $id ) . '" value="' . self::get_field_value() . '" class="bf-admin-field ' . esc_attr( $this->type ) . '" ' . self::get_field_attributes() . ' />' . "\n";

		self::set_field_output( $output );
		return self::get_field_output();

	}

};

/**
 * Register the field with the Admin API
 *
 * @param array $fields Fields.
 *
 * @return array
 */
function register_field( $fields ) {

	$new_field = array(
		'text_custom' => 'BF_Admin_API_Field_Text_Custom',
	);

	$fields = array_merge( $new_field, $fields );

	return $fields;

}
add_filter( 'bf_admin_api_fields', 'register_field', 10, 1 );
