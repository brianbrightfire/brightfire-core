<?php
/**
 * BrightFire Admin API Examples.
 *
 * @package BrightFireCore
 */

namespace BrightFire\Admin_API;

use function BrightFireCore\prettyprintr;

/**
 * Our Settings Page
 */
function bf_admin_api_examples() {

	// Check Capabilities.
	if ( ! current_user_can( BF_ADMIN_API_CAPABILITY ) ) {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	wp_enqueue_style( 'fontawesome' );

	// What tab are we on now?.
	// TODO: SECURITY - Need nonce verification.
	$current_tab = ( isset( $_GET['tab'] ) ? $_GET['tab'] : 'sample_array' ); // phpcs:ignore

	// Setup our tabs.
	echo '<div class="wrap">';
	echo '<h2>BrightFire Admin API</h2>';
	echo bf_admin_api_examples_tabs( $current_tab );

	echo '<div class="tab-wrapper">';
	echo '<div class="inside">';

	switch ( $current_tab ) {
		case 'sample_array':
			bf_admin_api_page_sample_array();
			break;
		case 'sample_instance':
			bf_admin_api_page_sample_instance();
			break;
		case 'samples':
			bf_admin_api_page_samples();
			break;
		default:
			bf_admin_api_page_sample_array();
	}

	echo '</div>';
	echo '</div>';
	echo '</div>';

}

/**
 * Sets up tabs for display
 *
 * @param string $current Current.
 *
 * @return string
 */
function bf_admin_api_examples_tabs( $current = 'overview' ) {
	$bf_admin_tabs = array(
		'sample_array'    => 'Sample Array',
		'sample_instance' => 'Sample Instance',
		'samples'         => 'Samples',
	);

	$tabs = '';

	$tabs .= '<div id="icon-themes" class="icon32"><br></div>';
	$tabs .= '<h2 class="nav-tab-wrapper">';

	foreach ( $bf_admin_tabs as $tab => $name ) {
		$class = ( $tab === $current ) ? ' nav-tab-active' : '';
		$tabs .= '<a class="bf-admin-api-nav-tab nav-tab' . $class . '" href="?page=bf-admin-api&tab=' . $tab . '">' . $name . '</a>';
	}

	$tabs .= '</h2>';

	return $tabs;
}

/**
 * Example Repeater Title.
 *
 * @param bool $collapse Collapse.
 * @param bool $sortable Sortable.
 * @return string
 */
function example_repeater_title( $collapse = false, $sortable = false ) {
	$output   = array();
	$output[] = 'Admin API Repeater';

	( $collapse ? $output[] = 'Collapsable' : null );
	( $sortable ? $output[] = 'Sortable' : null );
	$output[] = time();

	$rtn = '<h3 style="font-size: 12px; margin-left: 10px;">' . implode( ' | ', $output ) . '</h3>';

	return $rtn;
}

/**
 * Example Repeater Fields.
 *
 * @param array $instance Instance.
 * @return mixed
 */
function example_repeater_fields( $instance = array() ) {

	$fields = array(
		'first_name'   => array(
			'type'          => 'text',
			'default_value' => '',
			'atts'          => array(
				'placeholder' => 'First Name',
			),
		),
		'middle__name' => array(
			'type'          => 'text',
			'default_value' => '',
			'atts'          => array(
				'placeholder' => 'Middle Name',
			),
		),
		'last_name'    => array(
			'type'          => 'text',
			'default_value' => '',
			'atts'          => array(
				'placeholder' => 'Last Name',
			),
		),
	);

	$args = array(
		'fields'   => $fields,
		'instance' => $instance,
		'display'  => 'inline',
		'echo'     => false,
	);

	global $bf_admin_api;
	return $bf_admin_api->fields->build( $args );

}

/**
 * Example Repeater Fields 2.
 *
 * @param array $instance Instance.
 * @return mixed
 */
function example_repeater_fields2( $instance = array() ) {

	$fields = array(
		'first_name'   => array(
			'type'          => 'text',
			'default_value' => '',
			'atts'          => array(
				'placeholder' => 'First Name',
			),
		),
		'middle__name' => array(
			'type'          => 'text',
			'default_value' => '',
			'atts'          => array(
				'placeholder' => 'Middle Name',
			),
		),
		'last_name'    => array(
			'type'          => 'text',
			'default_value' => '',
			'atts'          => array(
				'placeholder' => 'Last Name',
			),
		),
	);

	$args = array(
		'fields'   => $fields,
		'instance' => $instance,
		'display'  => 'block',
		'echo'     => false,
	);

	global $bf_admin_api;
	return $bf_admin_api->fields->build( $args );

}

/**
 * A Sample 'fields' array - usually a subset of a larger configuration array
 *
 * @return array
 */
function bf_admin_api_sample_array() {
	return array(

		'deprecated_field' => array(
			'label'      => 'my field',
			'type'       => 'text',
			'deprecated' => true,
		),

		'nested_index'     => array(
			'type'   => 'nested_fields',
			'fields' => array(
				'text_field_1'   => array(
					'type'  => 'text',
					'label' => 'text_field_1',
				),
				'nested_index_2' => array(
					'type'   => 'nested_fields',
					'fields' => array(
						'text_field_2' => array(
							'type'  => 'Text',
							'label' => 'text_field_2',
						),
						'text_field_3' => array(
							'type'  => 'Text',
							'label' => 'text_field_3',
						),

					),
				),
			),
		),

		'search_pages2'    => array(
			'label'         => 'WP Page Select',
			'type'          => 'wp_page_select',
			'default_value' => '',
		),
		'search_pages'     => array(
			'label'         => 'Fontawesome Selectize',
			'type'          => 'fontawesome_select',
			'default_value' => '',
		),

		'repeater'         => array( // the field id serves as the JS object namespace for each repeater.
			'type'              => 'repeater',
			'instance'          => array(),
			// phpcs:disable
			// 'title_cb' => array(
			// 'function' => __NAMESPACE__ . '\example_repeater_title',
			// 'params' => array( true, true ),
			// ),
			// phpcs:enable
					'fields_cb' => array(
						'function' => __NAMESPACE__ . '\example_repeater_fields',
						'params'   => array(),
					),
			// phpcs:disable
			// 'js_cb' => array( // changed param name just to Fuck with Nathan.
			// 'function' => '',
			// 'params' => array(),
			// ),
			// phpcs:enable
				'collapse'      => true, // changed param name just to Fuck with Nathan.
			'sortable'          => true,
			'oneopen'           => false,
		),

		'repeater2'        => array( // the field id serves as the JS object namespace for each repeater.
			'type'      => 'repeater',
			'instance'  => array(),
			'title_cb'  => array(
				'function' => __NAMESPACE__ . '\exampleRepeaterTitle',
				'params'   => array( true, true ),
			),
			'fields_cb' => array(
				'function' => __NAMESPACE__ . '\example_repeater_fields',
				'params'   => array(),
			),
			'js_cb'     => array( // changed param name just to Fuck with Nathan.
				'function' => '',
				'params'   => array(),
			),
			'collapse'  => false, // changed param name just to Fuck with Nathan.
			'sortable'  => true,
			'oneopen'   => true,
		),

		'field_1'          => array(
			'type'          => 'text',
			'description'   => 'Has some custom classes added as well',
			'atts'          => array(
				'placeholder' => 'Enter some text',
			),
			'classes'       => array(
				'custom-class-one',
				'custom-class-two',
				'custom-class-three',
			),
			'default_value' => '',
			'permit'        => 1,

		),
		'field_2'          => array(
			'label'         => __( 'BF Admin API Date Field', 'bf-admin-api' ),
			'description'   => __( 'This is a Date Field', 'bf-admin-api' ),
			'type'          => 'date',
			'sanitize'      => 'date',
			'default_value' => 'March 31 1986',
			'atts'          => array(
				'placeholder' => 'Select a Date',
			),
			'permit'        => 1,
		),
		'field_3'          => array(
			'label'         => __( 'BF Admin API Email Field', 'bf-admin-api' ),
			'description'   => __( 'This is an Email Field', 'bf-admin-api' ),
			'type'          => 'email',
			'sanitize'      => 'email',
			'default_value' => 'brian@brightfire.com',
			'atts'          => array(
				'placeholder' => 'Please enter an email',
			),
			'permit'        => 1,
		),
		'field_4'          => array(
			'type'          => 'hidden',
			'default_value' => '',
			'permit'        => 1,
		),
		'field_5'          => array(
			'label'         => __( 'BF Admin API Number Field', 'bf-admin-api' ),
			'description'   => __( 'This is an Number Field', 'bf-admin-api' ),
			'type'          => 'number',
			'sanitize'      => 'integer',
			'default_value' => '0',
			'atts'          => array(
				'min'  => '0',
				'max'  => '50',
				'step' => '5',
			),
			'permit'        => 1,
		),
		'field_6'          => array(
			'label'         => __( 'BF Admin API Password Field', 'bf-admin-api' ),
			'description'   => __( 'This is a Password Field', 'bf-admin-api' ),
			'type'          => 'password',
			'default_value' => '',
			'atts'          => array(
				'placeholder' => 'Please enter a password',
			),
			'permit'        => 1,
		),
		'field_7'          => array(
			'label'         => __( 'BF Admin API Phone Field', 'bf-admin-api' ),
			'description'   => __( 'This is a Phone Field', 'bf-admin-api' ),
			'type'          => 'phone',
			'sanitize'      => 'phone',
			'default_value' => '7062246744',
			'atts'          => array(
				'placeholder' => 'Please enter a phone number',
			),
			'permit'        => 1,
		),
		'field_8'          => array(
			'label'         => __( 'BF Admin API Range Field', 'bf-admin-api' ),
			'description'   => __( 'This is a Range Field', 'bf-admin-api' ),
			'type'          => 'range',
			'sanitize'      => 'integer',
			'default_value' => '25',
			'atts'          => array(
				'min'  => '0',
				'max'  => '50',
				'step' => '5',
			),
			'settings'      => array(
				'min-label'    => 0,
				'max-label'    => 50,
				'units'        => 'Units',
				'show-current' => true,
			),
			'permit'        => 1,
		),
		'field_8a'         => array(
			'label'         => __( 'BF Admin API Range Field (Second Instance)', 'bf-admin-api' ),
			'description'   => __( 'This is a Range Field', 'bf-admin-api' ),
			'type'          => 'range',
			'sanitize'      => 'integer',
			'default_value' => '25',
			'atts'          => array(
				'min'  => '0',
				'max'  => '7',
				'step' => '0.1',
			),
			'settings'      => array(
				'min-label'    => 0,
				'max-label'    => 7,
				'units'        => 'Seconds',
				'show-current' => true,
			),
			'permit'        => 1,
		),
		'field_9'          => array(
			'label'         => __( 'BF Admin API Textarea Field', 'bf-admin-api' ),
			'description'   => __( 'This is a Textarea Field', 'bf-admin-api' ),
			'type'          => 'textarea',
			'default_value' => '',
			'permit'        => 1,
		),
		'field_10'         => array(
			'label'         => __( 'BF Admin API URL Field', 'bf-admin-api' ),
			'description'   => __( 'This is a URL Field', 'bf-admin-api' ),
			'type'          => 'url',
			'sanitize'      => 'url',
			'default_value' => 'https://brightfire.com',
			'permit'        => 1,
		),
		'field_11'         => array(
			'label'         => __( 'BF Admin API Select Field', 'bf-admin-api' ),
			'description'   => __( 'This is a Select Field', 'bf-admin-api' ),
			'type'          => 'select',
			'choices'       => array(
				'value1' => 'Value 1',
				'value2' => 'Value 2',
				'value3' => 'Value 3',
				'value4' => 'Value 4',
				'value5' => 'Value 5',
			),
			'default_value' => 'value3',
			'permit'        => 1,

		),
		'field_11b'        => array(
			'label'         => __( 'BF Admin API Select Field', 'bf-admin-api' ),
			'description'   => __( 'This is a Selectize Field', 'bf-admin-api' ),
			'type'          => 'selectize',
			'choices'       => array(
				'value1' => 'Value 1',
				'value2' => 'Value 2',
				'value3' => 'Value 3',
				'value4' => 'Value 4',
				'value5' => 'Value 5',
			),
			'default_value' => 'value3',
			'permit'        => 1,

		),
		'field_12'         => array(
			'label'         => __( 'BF Admin API Select Multi Field', 'bf-admin-api' ),
			'description'   => __( 'This is a Select Multi Field', 'bf-admin-api' ),
			'type'          => 'selectize',
			'atts'          => array(
				'multiple' => 'multiple',
			),
			'choices'       => array(
				'value1' => 'Value 1',
				'value2' => 'Value 2',
				'value3' => 'Value 3',
				'value4' => 'Value 4',
				'value5' => 'Value 5',
			),
			'default_value' => '',
			'permit'        => 1,

		),
		'field_13'         => array(
			'label'         => __( 'BF Admin API Color Picker Field', 'bf-admin-api' ),
			'description'   => __( 'This is a Color Pikcer Field', 'bf-admin-api' ),
			'type'          => 'wp_color',
			'default_value' => '#CC0000',
			'permit'        => 1,

		),
		'field_14'         => array(
			'label'         => __( 'BF Admin API YES NO Field', 'bf-admin-api' ),
			'description'   => __( 'This is a YES NO Field', 'bf-admin-api' ),
			'type'          => 'selectize',
			'default_value' => 'yes',
			'choices'       => 'yesno',
			'permit'        => 1,

		),
		'field_15'         => array(
			'label'         => __( 'BF Admin API ON OFF Field', 'bf-admin-api' ),
			'description'   => __( 'This is a ON OFF Field', 'bf-admin-api' ),
			'type'          => 'selectize',
			'default_value' => 'off',
			'choices'       => 'onoff',
			'permit'        => 1,

		),
		'field_16'         => array(
			'label'         => __( 'BF Admin API States Field', 'bf-admin-api' ),
			'description'   => __( 'This is a States Field', 'bf-admin-api' ),
			'type'          => 'selectize',
			'default_value' => 'Georgia',
			'choices'       => 'states',
			'permit'        => 1,

		),
		// phpcs:disable
		// 'field_17' => array(
		// 'label'       => __( 'BF Admin API Countries Field', 'bf-admin-api' ),
		// 'description' => __( 'This is a Countries Field', 'bf-admin-api' ),
		// 'type'        => 'selectize',
		// 'default_value'     => '',
		// 'choices' => 'countries',
		// 'permit'      => 1,
		//
		// ),
		// phpcs:enable
			'field_18'     => array(
				'label'       => __( 'BF Admin API WP Image Field', 'bf-admin-api' ),
				'description' => __( 'This is a WP Image field', 'bf-admin-api' ),
				'type'        => 'wp_image',
				'sanitize'    => 'integer',
				'permit'      => 1,
			),
	);
}

/**
 * BrightFire Admin API Sample Instance.
 *
 * @return array
 */
function bf_admin_api_sample_instance() {
	return array(
		'nested_index' => array(
			'text_field_1'   => 'text field 1 value',
			'nested_index_2' => array(
				'text_field_2' => 'text field 2 value',
				'text_field_3' => 'field 3 value',
			),
		),
		'field_1'      => 'My Current instance value',
		'field_18'     => '3425',
	);
}

/**
 * Sample Fields - uses above array
 *
 * @uses bf_admin_api_sample_array()
 */
function bf_admin_api_page_samples() {

	$sample_instance = bf_admin_api_sample_instance();
	$sample_fields   = bf_admin_api_sample_array();

	echo 'This example uses the fields array in "Sample Array" and also sends the instance detailed in "Sample Instance" to create the fields below. It instantiates the Admin API with the following code:<br><br>' .
		'<code>' .
		'$args = array(' .
		'    \'fields\' => $sample_fields,' .
		'    \'instance\' => $sample_instance,' .
		'    \'display\' => \'table\',' .
		'    \'section_id\' => \'sample-id\',' .
		'    \'echo\' => true,' .
		'); <br>' .
		'<br>' .
		'$bf_admin_api->fields->build( $args );' .
		'</code>';

	echo "<div id='test-progress-bar'></div>";
	echo "<script type='text/javascript'>$(document).ready( function() { var myProgressBar = $('#test-progress-bar').bf_progress_bar(); });</script>";

	// Run It.
	$args = array(
		'fields'     => $sample_fields,
		'instance'   => $sample_instance,
		'display'    => 'block',
		'section_id' => 'sample_id',
		'echo'       => true,
	);

	global $bf_admin_api;
	return $bf_admin_api->fields->build( $args );

}

/**
 * Show a print_r of our sample array for reference
 *
 * @uses bf-admin_api_smaple_array()
 */
function bf_admin_api_page_sample_array() {
	$sample_fields = bf_admin_api_sample_array();

	prettyprintr( $sample_fields );
}

/**
 * Show a print_r of our sample array for reference
 *
 * @uses bf-admin_api_smaple_array()
 */
function bf_admin_api_page_sample_instance() {
	$sample_instance = bf_admin_api_sample_instance();

	prettyprintr( $sample_instance );
}
