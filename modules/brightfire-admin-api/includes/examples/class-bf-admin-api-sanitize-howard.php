<?php
/**
 * BrightFire Admin API Example Custom Sanitize.
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Sanitize_Howard
 * Adds a custom sanitation method
 */
class BF_Admin_API_Sanitize_Howard extends BF_Admin_API_Sanitize_Base {

	/**
	 * Sanitize value as an integer
	 *
	 * @param mixed $value Value.
	 *
	 * @return mixed
	 */
	public static function sanitize( $value ) {

		// Remove all illegal characters from email address.
		return 'Howard Stern is a BOSS';

	}

};

/**
 * Class BF_Admin_API_Sanitize_Howard_Sucks
 * Adds a custom sanitation method
 */
class BF_Admin_API_Sanitize_Howard_Sucks extends BF_Admin_API_Sanitize { // phpcs:ignore

	/**
	 * Sanitize value as an integer
	 *
	 * @param mixed $value Value.
	 *
	 * @return mixed
	 */
	public static function sanitize( $value ) {

		// Remove all illegal characters from email address.
		return 'Howard Stern sucks';

	}

};

/**
 * Register the sanitation with the Admin API
 *
 * @param array $sanitize Sanitize array.
 *
 * @return array
 */
function register_sanitize( $sanitize ) {

	$new_sanitize = array(
		'howard'       => 'BF_Admin_API_Sanitize_Howard',
		'howard_sucks' => 'BF_Admin_API_Sanitize_Howard_Sucks',
	);

	$sanitize = array_merge( $new_sanitize, $sanitize );

	return $sanitize;

}
add_filter( 'bf_admin_api_sanitize', 'register_sanitize', 10, 1 );
