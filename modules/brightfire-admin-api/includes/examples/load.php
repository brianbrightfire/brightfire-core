<?php
/**
 * BrightFire Admin API Examples Load.
 *
 * @package BrightFireCore
 */

namespace BrightFire\Admin_API;

require_once dirname( __FILE__ ) . '/examples.php';
require_once dirname( __FILE__ ) . '/class-bf-admin-api-field-text-custom.php';
require_once dirname( __FILE__ ) . '/class-bf-admin-api-sanitize-howard.php';



/**
 * Add a page in settings for usage, etc
 */
function bf_admin_api_register_helpers() {
	add_options_page( 'BrightFire Admin API', 'BrightFire Admin API', BF_ADMIN_API_CAPABILITY, 'bf-admin-api', __NAMESPACE__ . '\bf_admin_api_examples' );
}

/**
 * Add Our Helper Menu
 */
add_action( 'admin_menu', __NAMESPACE__ . '\bf_admin_api_register_helpers' );
