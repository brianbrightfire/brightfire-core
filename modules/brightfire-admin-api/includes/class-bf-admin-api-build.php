<?php
/**
 * BrightFire Admin API Build.
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Build
 * Responsibility: Instance for each build() call we make to track config, etc
 */
class BF_Admin_API_Build {

	// Scope Vars.

	/**
	 * Field Config.
	 *
	 * @var array $field_config
	 */
	private $field_config;

	/**
	 * Output.
	 *
	 * @var $output
	 */
	private $output;

	// Defaults.

	/**
	 * Fields.
	 *
	 * @var array $fields
	 */
	private $fields = array();

	/**
	 * Instance.
	 *
	 * @var array $instance
	 */
	private $instance = array();

	/**
	 * Echo.
	 *
	 * @var bool $echo
	 */
	private $echo = true;

	/**
	 * Display.
	 *
	 * @var string $display
	 */
	private $display = 'inline';

	/**
	 * Section ID.
	 *
	 * @var string $section_id
	 */
	private $section_id = '';

	/**
	 * Widget.
	 *
	 * @var mixed $widget
	 */
	private $widget;


	/**
	 * BF_Admin_API_Build constructor.
	 * Creates Form Markup for each build
	 *
	 * @param array $args Arguments.
	 */
	public function __construct( $args ) {

		// Extract our given $args into our class properties.
		foreach ( $args as $key => $value ) {

			// phpcs:ignore
			// Backcompat for 'widget_instance' (Now 'widget').
			if ( 'widget_instance' === $key ) {
				$this->widget = $value;
			} else {
				$this->$key = $value;
			}
		}

		// Grab our field class definitions.
		$this->field_config = BF_Admin_API_Fields::bf_admin_api_field_array();

		$this->open_form();
		$this->fields();
		$this->close_form();

	}

	/**
	 * Returns Form Output - Echo output if flag set
	 *
	 * @return mixed
	 */
	public function output() {

		if ( $this->echo ) {
			echo $this->output;
		}

		return $this->output;
	}

	/**
	 * Appends HTML Markup to $output property - Form Opening Tags
	 *
	 * @return bool
	 */
	private function open_form() {

		if ( 'table' === $this->display ) {
			$output = '<table class="form-table" id="' . $this->section_id . '">';
		} elseif ( 'inline' === $this->display ) {
			$output = '<div class="form-wrapper" id="' . $this->section_id . '">';
		} elseif ( 'basic' === $this->display ) {
			$output = '<div class="form-wrapper basic" id="' . $this->section_id . '">';
		} else {
			$output = ''; // None as default.
		}

		$this->output .= $output;

		return true;

	}

	/**
	 * Appends HTML Markup to $output property - Form Closing Tags
	 *
	 * @return bool
	 */
	private function close_form() {

		if ( 'table' === $this->display ) {
			$output = '</table>';
		} elseif ( 'inline' === $this->display ) {
			$output = '</div>';
		} elseif ( 'basic' === $this->display ) {
			$output = '</div>';
		} else {
			$output = ''; // None as default.
		}

		$this->output .= $output;

		return true;

	}

	/**
	 * Loops Through Fields define and calls recursive method for dealing with sections
	 */
	private function fields() {

		foreach ( $this->fields as $field_id => $field_data ) {

			// Get our instance Value for this field.
			if ( isset( $this->instance[ $field_id ] ) ) {
				$value = $this->instance[ $field_id ];
			} else {
				$value = '';
			}

			// Get Field Output.
			$this->recurse_section_field( $field_id, $field_data, $value );

		}

		return true;

	}

	/**
	 * Recursive method for tracking fields and sections - tracks instance value parity
	 *
	 * @param string $field_id Field ID.
	 * @param array  $field_data Field Data.
	 * @param string $value Value.
	 */
	private function recurse_section_field( $field_id, $field_data, $value = '' ) {

		$data_defaults = array(
			'type'                    => 'text',
			'label'                   => '',
			'sanitize'                => '',
			'instance_value_override' => null,
		);
		$data          = wp_parse_args( $field_data, $data_defaults );

		$classes    = '';
		$attributes = '';

		// Classes and Attributes ( FOR wrappers ).
		if ( array_key_exists( 'classes', $data ) ) {
			$classes = \BrightFire\Admin_API\format_classes( $data['classes'] );
		}

		if ( array_key_exists( 'attributes', $data ) ) {
			$attributes = \BrightFire\Admin_API\format_attributes( $data['attributes'] );
		}

		// Nested Fields, Wrappers, and Fields.
		if ( 'nested_fields' === $data['type'] && array_key_exists( 'fields', $data ) ) {

			$this->output .= "<div id=\"{$this->sanitize_wrapper_id($field_id)}\" class=\"admin-api-section {$classes}\" {$attributes}>";

			// This is a section, we need to recurse on 'fields'.
			foreach ( $data['fields'] as $new_field_id => $new_field_data ) {

				// Map instance to fields define.
				if ( isset( $value[ $new_field_id ] ) ) {
					$new_value = $value[ $new_field_id ];
				} else {
					$new_value = $value;
				}

				$this->recurse_section_field( $field_id . '[' . $new_field_id . ']', $new_field_data, $new_value );

			}

			$this->output .= '</div>';

		} elseif ( 'wrapper' === $data['type'] && array_key_exists( 'fields', $data ) ) {

			$this->output .= "<div id=\"{$this->sanitize_wrapper_id($field_id)}\" class=\"admin-api-wrapper {$classes}\" {$attributes}>";

			foreach ( $data['fields'] as $wrapper_field_id => $wrapper_field_data ) {

				$this->recurse_section_field( $wrapper_field_id, $wrapper_field_data, $value );

			}

			$this->output .= '</div>';

		} else {

			$this->build_field( $field_id, $field_data, $value );

		}

	}

	/**
	 * Builds a single field.
	 *
	 * @param string $field_id Field ID.
	 * @param array  $field_data Field Data.
	 * @param string $value Value.
	 *
	 * @return mixed
	 */
	private function build_field( $field_id, $field_data, $value ) {

		// Set up our widget name / instance if given.
		// Section ID will only be used if widget_name and wiget_instance are not given.
		if ( is_a( $this->widget, 'WP_Widget' ) ) {
			$my_field_name = $this->widget->get_field_name( $field_id );
			$my_field_id   = $this->widget->get_field_id( $field_id );
		} elseif ( isset( $field_data['name'] ) ) { // Allow 'name' override as a field param.
			$my_field_id   = $field_id;
			$my_field_name = $field_data['name'];
		} else {
			$my_field_id   = ( ! empty( $this->section_id ) ? $this->section_id . '[' . $field_id . ']' : $field_id );
			$my_field_name = $my_field_id;
		}

		// Set up field data and values, and instance.
		$field_type  = $field_data['type'];
		$classname   = $this->field_config[ strtolower( $field_type ) ];
		$field_class = new $classname( $this->display );

		// Set Field value from instance or override.
		if ( isset( $field_data['instance_value_override'] ) ) {       // Checks for an override value.
			$instance_value = $field_data['instance_value_override'];
		} else {
			$instance_value = $value;
		}

		// Field instance Setup.
		$setup_args = array(
			'id'     => $my_field_id,
			'name'   => $my_field_name,
			'config' => $field_data,
			'value'  => $instance_value,
		);

		// Run the setup.
		$field_class->setup( $setup_args );

		// Make the field.
		$this->output .= $field_class->create_field();

	}

	/**
	 * Returns "section-$1" where $1 is the last index reference
	 *
	 * @param string $id ID.
	 *
	 * @return string
	 */
	private function sanitize_wrapper_id( $id ) {

		return 'section-' . preg_replace( '/\S*\[(\S*)\]/', '$1', $id );

	}

}
