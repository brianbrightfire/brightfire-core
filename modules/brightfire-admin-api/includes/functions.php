<?php
/**
 * BrightFire Admin API Functions.
 *
 * @package BrightFireCore
 */

namespace BrightFire\Admin_API;

/**
 * Instantiate the Admin API
 */
function load_admin_api() {

	global $bf_admin_api;

	$bf_admin_api = new \stdClass();

	$bf_admin_api->fields   = new \BF_Admin_API_Fields();
	$bf_admin_api->sanitize = new \BF_Admin_API_Sanitize();

	add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\bf_admin_api_assets' );
}

/**
 * Builds a single repeater set
 *
 * @param array $callbacks // array of callbacks | title_cb, fields_cb, title_fields_cb.
 * @param array $repeater_markup // pre-existing markup for title_bar and fields.
 *
 * @return string
 */
function build_repeater( $callbacks = array(), $repeater_markup = array() ) {

	if ( empty( $repeater_markup ) ) {

		$repeater_markup['title_bar'] = callback_handler( $callbacks['title_cb'] );
		$repeater_markup['fields']    = callback_handler( $callbacks['fields_cb'] );

		if ( isset( $callbacks['title_fields_cb'] ) && ! empty( $callbacks['title_fields_cb'] ) ) {
			$repeater_markup = callback_handler( $callbacks['title_fields_cb'] );
		}
	}

	// Create our output.
	$output = '';
	$set_id = uniqid();

	$output .= "<div id='{$set_id}' class='bf-admin-api bf-repeater bf-repeater-set'>";

	$output .= "<div class='bf-admin-api bf-repeater-top'>";
	$output .= "{$repeater_markup['title_bar']}";
	$output .= '</div>'; // repeater-top.

	$output .= "<div class='bf-admin-api bf-repeater-inside'>";

	$output .= "{$repeater_markup['fields']}";

	$output .= '</div>'; // repeater-inside.

	$output .= '</div>'; // repeater.

	return $output;

}

/**
 * Runs a callback and returns results
 *
 * @param array $args Arguments.
 *
 * @return string
 */
function callback_handler( $args = array(
	'function' => '',
	'params'   => array(),
) ) {

	// Was callback provided?
	if ( empty( $args['function'] ) ) {
		return '';
	}

	if ( empty( $args['params'] ) ) {
		$args['params'] = array();
	}

	$exists   = true;
	$function = 'unknown';

	if ( ! is_array( $args['function'] ) ) {
		$exists   = function_exists( $args['function'] );
		$function = $args['function'];
	}

	if ( is_array( $args['function'] ) ) {
		$exists   = method_exists( $args['function'][0], $args['function'][1] );
		$function = $args['function'][1];
	}

	// If Function provided and exists.
	if ( $exists ) {
		$output = call_user_func_array( $args['function'], $args['params'] );
	} else {
		$output = array(
			'title_bar' => 'ERROR',
			'fields'    => "<span class='error'>Bad Callback:: Function [{$function}] does not exist</span>",
		);
	}

	return $output;
}

/**
 * Get Missing Image URL.
 *
 * @return string
 */
function get_missing_image_url() {
	return BF_CORE_ASSETS . '/images/missing-image.png';
}

/**
 * Enqueue scripts and styles for Admin API
 */
function bf_admin_api_assets() {
	wp_enqueue_script( 'jquery-ui-core' );
	wp_enqueue_script( 'jquery-ui-datepicker' );
	wp_enqueue_script( 'jquery-ui-sortable', 'jquery', array(), BF_CORE_VERSION, false );
	wp_enqueue_style( 'brightfire-admin-api-styles', BF_CORE_ASSETS . '/css/brightfire-admin-api.min.css', array(), BF_CORE_VERSION );
	wp_enqueue_style( 'wp-color-picker' );
	wp_enqueue_style( 'jquery-style', '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css', array(), BF_CORE_VERSION );
	wp_enqueue_script( 'selectize' );

	if ( is_customize_preview() ) {
		wp_enqueue_script(
			'brightfire_admin_api_customizer',
			BF_CORE_ASSETS . 'js/brightfire-admin-api-customizer.min.js',
			array(
				'jquery',
				'jquery-ui-core',
				'jquery-ui-sortable',
				'jquery-ui-datepicker',
				'wp-color-picker',
				'jquery-ui-datepicker',
			),
			BF_CORE_VERSION,
			true
		);
	} else {
		wp_enqueue_script(
			'brightfire_admin_api',
			BF_CORE_ASSETS . 'js/brightfire-admin-api.min.js',
			array(
				'jquery',
				'jquery-ui-core',
				'jquery-ui-sortable',
				'jquery-ui-datepicker',
				'wp-color-picker',
				'jquery-ui-datepicker',
			),
			BF_CORE_VERSION,
			true
		);
	}

	wp_enqueue_media();
}

/**
 * Format Attributes.
 *
 * @param array $atts Attributes.
 * @return string
 */
function format_attributes( $atts ) {
	$rtn_atts = array();

	foreach ( $atts as $attribute => $value ) {
		$rtn_atts[] = $attribute . '="' . $value . '"';
	}
	return implode( ' ', $rtn_atts );
}

/**
 * Format Classes.
 *
 * @param array $classes Classes.
 * @return string
 */
function format_classes( $classes ) {
	return implode( ' ', $classes );
}
