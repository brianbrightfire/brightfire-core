<?php
/**
 * Class BF_Admin_API_Field_WP_Image
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Field_WP_Image
 */
class BF_Admin_API_Field_WP_Image extends BF_Admin_API_Field_Base {

	/**
	 * Type.
	 *
	 * @var string $type
	 */
	private $type = 'wp_image';

	/**
	 * Creates a Text Field
	 *
	 * @return mixed
	 */
	public function create_field() {

		$value = self::get_field_value();

		if ( $value ) {
			$img = wp_get_attachment_image_src( $value );

			if ( $img ) {
				$style = ' style="background-image : url(' . $img[0] . ');"';
			} else {
				$style = ' style="background-image : url(' . \BrightFire\Admin_API\get_missing_image_url() . ');"';
			}

			$class = ' remove-image';
		} else {
			$style = '';
			$class = ' set-image';
		}

		$output  = '<div class="bf-admin-image-container' . $class . ' ' . self::get_field_classes() . '"' . $style . '><div class="remove-message">Remove Image</div></div>';
		$output .= '<input name="' . self::get_field_name() . '" id="' . self::get_field_id() . '" type="hidden" value="' . $value . '" ' . self::get_field_attributes() . ' />';

		self::set_field_output( $output );

		return self::get_field_output();

	}
}
