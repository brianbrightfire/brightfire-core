<?php
/**
 * BrightFire Admin API Field Base Interface.
 *
 * @package BrightFireCore
 */

/**
 * Interface Admin_API_Field
 */
interface Admin_API_Field_Base {

	// phpcs:disable
	public function setup( $args );

	public function set_config( $config );

	public function get_config( $index );

	public function set_display( $display );

	public function get_display();

	public function set_field_id( $id );

	public function get_field_id();

	public function set_field_name( $name );

	public function get_field_name();

	public function set_field_echo( $echo );

	public function get_field_echo();

	public function set_field_wrapper( $action );

	public function get_field_wrapper_open();

	public function get_field_wrapper_close();

	public function set_field_prepend( $text );

	public function get_field_prepend();

	public function set_field_append( $text );

	public function get_field_append();

	public function set_field_label_text( $text );

	public function get_field_label_text();

	public function set_field_desc_text( $text );

	public function get_field_desc_text();

	public function set_field_label();

	public function get_field_label();

	public function set_field_attributes( $atts );

	public function get_field_attributes();

	public function set_field_classes( $classes );

	public function get_field_classes();

	public function set_field_sanitize( $sanitize );

	public function get_field_sanitize();

	public function set_field_value( $value );

	public function get_field_value();

	public function set_field_settings( $value );

	public function get_field_settings();

	public function set_field_choices( $choices );

	public function get_field_choices();

	public function set_field_output( $markup, $wrap );

	public function get_field_output();
	//phpcs:enable

}
