<?php
/**
 * BrightFire Admin API Field Base Class.
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Field
 *
 * This is our base class for BF Admin API Fields
 *
 * Responsibility: Formatting and Control
 */
class BF_Admin_API_Field_Base implements Admin_API_Field_Base {

	/**
	 * Defaults.
	 *
	 * @var array $defaults
	 */
	private $defaults;

	/**
	 * Config.
	 *
	 * @var $config
	 */
	public $config;

	/**
	 * ID.
	 *
	 * @var $id
	 */
	private $id;

	/**
	 * Name.
	 *
	 * @var $name
	 */
	private $name;

	/**
	 * Display.
	 *
	 * @var $display
	 */
	private $display;

	/**
	 * Wrapper Open.
	 *
	 * @var $wrapper_open
	 */
	private $wrapper_open;

	/**
	 * Wrapper Close.
	 *
	 * @var $wrapper_close
	 */
	private $wrapper_close;

	/**
	 * Prepend.
	 *
	 * @var $prepend
	 */
	private $prepend;

	/**
	 * Append.
	 *
	 * @var $append
	 */
	private $append;

	/**
	 * Attributes.
	 *
	 * @var $attributes
	 */
	private $attributes;

	/**
	 * Classes.
	 *
	 * @var $classes
	 */
	private $classes;

	/**
	 * Label Text.
	 *
	 * @var $label_text
	 */
	private $label_text;

	/**
	 * Description Text.
	 *
	 * @var $description_text
	 */
	private $description_text;

	/**
	 * Label.
	 *
	 * @var $label
	 */
	private $label;

	/**
	 * Sanitize.
	 *
	 * @var $sanitize
	 */
	private $sanitize;

	/**
	 * Value.
	 *
	 * @var $value
	 */
	private $value;

	/**
	 * Settings.
	 *
	 * @var $settings
	 */
	private $settings;

	/**
	 * Choices.
	 *
	 * @var $choices
	 */
	private $choices;

	/**
	 * Output.
	 *
	 * @var $output
	 */
	private $output;

	/**
	 * Deprecated.
	 *
	 * @var $deprecated
	 */
	private $deprecated;

	/**
	 * BF_Admin_API_Field constructor.
	 *
	 * @param string $display How to display our field | inline | table.
	 */
	public function __construct( $display = 'inline' ) {
		$this->defaults = array(
			'type'          => '',
			'search'        => false,
			'label'         => '',
			'prefix'        => '',
			'description'   => '',
			'default_value' => '',
			'sanitize'      => '',
			'choices'       => array(),
			'atts'          => array(),
			'classes'       => array(),
			'prepend'       => '',
			'append'        => '',
			'settings'      => array(),
			'permit'        => 1,
			'deprecated'    => false,
		);

		self::set_display( $display );
	}

	/**
	 * Pitch Our Configuration to setup our instance.
	 *
	 * @param array $args Arguments.
	 */
	public function setup( $args ) {
		$this->set_config( $args['config'] );
		$this->set_deprecated( $this->config['deprecated'] );
		$this->set_field_id( $args['id'] );
		$this->set_field_name( $args['name'] );
		$this->set_field_wrapper( 'open' );
		$this->set_field_prepend( $this->config['prepend'] );
		$this->set_field_append( $this->config['append'] );
		$this->set_field_label_text( $this->config['label'] );
		$this->set_field_desc_text( $this->config['description'] );
		$this->set_field_label();
		$this->set_field_attributes( $this->config['atts'] );
		$this->set_field_classes( $this->config['classes'] );
		$this->set_field_choices( $this->config['choices'] );
		$this->set_field_sanitize( $this->config['sanitize'] );
		$this->set_field_value( $args['value'] );
		$this->set_field_settings( $this->config['settings'] );
	}

	/**
	 * Parses provided config with defaults.
	 *
	 * @param array $config Config.
	 *
	 * @return bool
	 */
	public function set_config( $config ) {
		$this->config = wp_parse_args( $config, $this->defaults );

		return true;
	}

	/**
	 * Get Field Config.
	 *
	 * @param string $index Index.
	 * @return mixed
	 */
	public function get_config( $index = '' ) {
		if ( ! empty( $index ) ) {

			if ( array_key_exists( $index, $this->config ) ) {
				return $this->config[ $index ];
			} else {
				return false;
			}
		} else {
			return $this->config;
		}
	}

	/**
	 * Set Deprecated.
	 *
	 * @param mixed $deprecated Deprecated.
	 */
	public function set_deprecated( $deprecated ) {
		$this->deprecated = $deprecated;
	}


	/**
	 * Sets our instance display type.
	 *
	 * @param string $display Display.
	 *
	 * @return bool
	 */
	public function set_display( $display ) {
		$this->display = $display;

		return true;
	}

	/**
	 * Get our display type.
	 *
	 * @return mixed
	 */
	public function get_display() {
		return $this->display;
	}

	/**
	 * Sets our field id
	 *
	 * @param mixed $id ID.
	 *
	 * @return bool
	 */
	public function set_field_id( $id ) {
		$this->id = $id;

		return true;
	}

	/**
	 * Get our field ID.
	 *
	 * @return mixed
	 */
	public function get_field_id() {
		return $this->id;
	}

	/**
	 * Sets our field name.
	 *
	 * @param string $name Name.
	 *
	 * @return bool
	 */
	public function set_field_name( $name ) {
		$this->name = $name;

		return true;
	}

	/**
	 * Get our field name.
	 *
	 * @return mixed
	 */
	public function get_field_name() {
		return $this->name;
	}

	/**
	 * Set our echo param.
	 *
	 * @param string $echo Echo.
	 *
	 * @return bool
	 */
	public function set_field_echo( $echo ) {
		$this->echo = $echo;

		return true;
	}

	/**
	 * Get our instance echo settings.
	 *
	 * @return bool
	 */
	public function get_field_echo() {
		return $this->echo;
	}

	/**
	 * Sets Field Wrapper HTML.
	 *
	 * @param string $action Action.
	 *
	 * @return bool
	 */
	public function set_field_wrapper( $action = 'open' ) {

		$display = self::get_display();

		if ( $this->deprecated ) {
			$classes = 'deprecated';
		} else {
			$classes = '';
		}

		$this->wrapper_open  = '<div class="bf-admin-api input-wrapper ' . $display . ' ' . $classes . '">';
		$this->wrapper_close = '</div>';

		return true;
	}

	/**
	 * Get Field Wrapper Opening HTML.
	 *
	 * @return mixed
	 */
	public function get_field_wrapper_open() {
		return $this->wrapper_open;
	}

	/**
	 * Get Field Wrapper Closing HTML.
	 *
	 * @return mixed
	 */
	public function get_field_wrapper_close() {
		return $this->wrapper_close;
	}

	/**
	 * Sets field Prepend HTML.
	 *
	 * @param string $text Text.
	 *
	 * @return bool
	 */
	public function set_field_prepend( $text ) {
		$this->prepend = ( ! empty( $text ) ? '<div class="bf-admin-api-prepend">' . $text . '</div>' : '' );

		return true;
	}

	/**
	 * Get field Prepend HTML.
	 *
	 * @return mixed
	 */
	public function get_field_prepend() {
		return $this->prepend;
	}

	/**
	 * Sets field Append HTML.
	 *
	 * @param string $text Text.
	 *
	 * @return bool
	 */
	public function set_field_append( $text ) {
		$this->append = ( ! empty( $text ) ? '<div class="bf-admin-api-append">' . $text . '</div>' : '' );

		return true;
	}

	/**
	 * Get field Append HTML.
	 *
	 * @return mixed
	 */
	public function get_field_append() {
		return $this->append;
	}

	/**
	 * Sets Field Label HTML.
	 *
	 * @param string $text Text.
	 *
	 * @return bool
	 */
	public function set_field_label_text( $text ) {

		if ( $this->deprecated ) {
			$text = 'DEPRECATED - ' . $text;
		}

		$this->label_text = ( ! empty( $text ) ? '<span class="bf-admin-api field-label">' . $text . '</span>' . "\n" : '' );

		return true;
	}

	/**
	 * Get Field Label HTML.
	 *
	 * @return mixed
	 */
	public function get_field_label_text() {
		return $this->label_text;
	}

	/**
	 * Sets Field Description HTML.
	 *
	 * @param string $text Text.
	 *
	 * @return bool
	 */
	public function set_field_desc_text( $text ) {

		if ( $this->deprecated ) {
			$text = 'Deprecated, please do not use';
		}

		$this->description_text = ( ! empty( $text ) ? '<span class="bf-admin-api field-desc">' . $text . '</span>' . "\n" : '' );

		return true;
	}

	/**
	 * Get Field Description HTML.
	 *
	 * @return mixed
	 */
	public function get_field_desc_text() {
		return $this->description_text;
	}

	/**
	 * Builds our formatted label HTML.
	 *
	 * @return bool
	 */
	public function set_field_label() {

		$id         = self::get_field_id();
		$label_text = self::get_field_label_text();
		$desc_text  = self::get_field_desc_text();

		$output  = '';
		$output .= '<label for="' . $id . '" class="bf-admin-api label">';

		if ( ( ! empty( $label_text ) || ! empty( $desc_text ) ) && ( 'button' !== $this->config['type'] && 'hidden' !== $this->config['type'] ) ) {
			$output .= $label_text . $desc_text;
		}

		$output .= '</label>';

		$this->label = $output;

		return true;
	}

	/**
	 * Get our formated label HTML.
	 *
	 * @return mixed
	 */
	public function get_field_label() {
		return $this->label;
	}

	/**
	 * Sets Formatted Attributes Markup.
	 *
	 * @param array $atts Attributes.
	 *
	 * @return bool
	 */
	public function set_field_attributes( $atts ) {
		if ( empty( $this->attributes ) ) {
			$this->attributes = $atts;
		} else {
			$this->attributes = array_merge( $this->attributes, $atts );
		}

		return true;
	}

	/**
	 * Get Formatted Attributes Markup.
	 *
	 * @return mixed
	 */
	public function get_field_attributes() {
		return \BrightFire\Admin_API\format_attributes( $this->attributes );
	}

	/**
	 * Sets Formatted Class Markup.
	 *
	 * @param array $classes Classes.
	 *
	 * @return bool
	 */
	public function set_field_classes( $classes ) {
		if ( empty( $this->classes ) ) {
			$this->classes = $classes;
		} else {
			$this->classes = array_merge( $this->classes, $classes );
		}

		return true;
	}

	/**
	 * Get Formatted Class Markup.
	 *
	 * @return mixed
	 */
	public function get_field_classes() {
		return \BrightFire\Admin_API\format_classes( $this->classes );
	}

	/**
	 * Set our sanitation method.
	 *
	 * @param mixed $sanitize Sanitize.
	 *
	 * @return bool
	 */
	public function set_field_sanitize( $sanitize ) {
		$this->sanitize = $sanitize;

		return true;
	}

	/**
	 * Get our sanitize
	 *
	 * @return mixed
	 */
	public function get_field_sanitize() {
		return $this->sanitize;
	}

	/**
	 * Sets our Field Value.
	 *
	 * @param string $value Value.
	 *
	 * @return bool
	 */
	public function set_field_value( $value ) {

		$value = ( empty( $value ) && ! is_numeric( $value ) ) ? $this->config['default_value'] : $value;

		if ( is_array( $value ) ) {
			$this->value = $value;
		} else {
			// Leverage Sanitation Class.
			$sanitized   = BF_Admin_API_Sanitize::bf_admin_api_sanitize_run( $this->sanitize, $value );
			$this->value = esc_attr( $sanitized );
		}

		return true;
	}

	/**
	 * Get our field value.
	 *
	 * @param string $index Index.
	 *
	 * @return mixed
	 */
	public function get_field_value( $index = '' ) {
		if ( ! empty( $index ) ) {
			return ( isset( $this->value[ $index ] ) ? $this->value[ $index ] : $this->value );
		} else {
			return $this->value;
		}
	}

	/**
	 * Sets our Field Settings.
	 *
	 * @param string $value Value.
	 *
	 * @return bool
	 */
	public function set_field_settings( $value ) {

		$this->settings = $value;

		return true;
	}

	/**
	 * Get our field settings.
	 *
	 * @return mixed
	 */
	public function get_field_settings() {
		return $this->settings;
	}

	/**
	 * Builds our final output
	 *
	 * @param string  $markup Markup.
	 * @param boolean $wrap Wrap.
	 *
	 * @return bool
	 */
	public function set_field_output( $markup, $wrap = true ) {

		$display = self::get_display();
		$prepend = self::get_field_prepend();
		$append  = self::get_field_append();
		$label   = self::get_field_label();

		$output = '';

		switch ( $display ) {

			case 'block':
			case 'inline':
			case 'basic':
				$output .= ( true === $wrap ? $this->wrapper_open : '' );
				$output .= $label;
				$output .= $prepend . $markup . $append;
				$output .= ( true === $wrap ? $this->wrapper_close : '' );
				break;

			case 'table':
				$output .= '<tr>';
				$output .= '<th scope="row">';
				$output .= $label;
				$output .= '</th>';
				$output .= '<td class="bf-admin-api input-wrapper">';
				$output .= $prepend . $markup . $append;
				$output .= '</td>';
				$output .= '</tr>';
				break;

			default:
				$output .= '<b>Invalid display type set</b>';
				break;

		}

		$this->output = $output;

		return true;
	}

	/**
	 * Gets our output markup
	 *
	 * @return mixed
	 */
	public function get_field_output() {
		return $this->output;
	}

	/**
	 * Set our choices to the instance.
	 *
	 * @param array $choices Choices.
	 *
	 * @return bool
	 */
	public function set_field_choices( $choices ) {
		$this->choices = $choices;

		return true;
	}

	/**
	 * Gets our choices.
	 *
	 * @return mixed
	 */
	public function get_field_choices() {
		return $this->choices;
	}

}
