<?php
/**
 * Class BF_Admin_API_Field_Hidden
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Field_Hidden
 */
class BF_Admin_API_Field_Hidden extends BF_Admin_API_Field_Base {

	/**
	 * Type.
	 *
	 * @var string $type
	 */
	private $type = 'text';

	/**
	 * Creates a Text Field.
	 *
	 * @return mixed
	 */
	public function create_field() {

		// The Field HTML.
		$output = '<input type="hidden" id="' . self::get_field_id() . '" name="' . self::get_field_name() . '" value="' . self::get_field_value() . '" class="bf-admin-field ' . esc_attr( $this->type ) . '" ' . self::get_field_attributes() . ' />' . "\n";

		self::set_field_output( $output, false );

		return self::get_field_output();

	}

}
