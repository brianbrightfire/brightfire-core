<?php
/**
 * Class BF_Admin_API_Field_Repeater
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Field_Repeater
 */
class BF_Admin_API_Field_Repeater extends \BF_Admin_API_Field_Base {

	/**
	 * Config.
	 *
	 * @var array $config
	 */
	public $config = array();

	/**
	 * Config JSON.
	 *
	 * @var string $config_json
	 */
	private $config_json = '{}';

	/**
	 * Creates a Repeater Fieldset
	 *
	 * @return string
	 */
	public function create_field() {

		// Set some defaults.
		$this->config          = self::get_config();
		$config_embed          = array(
			'collapse' => ( isset( $this->config['collapse'] ) ? $this->config['collapse'] : true ),
			'sortable' => ( isset( $this->config['sortable'] ) ? $this->config['sortable'] : true ),
			'oneopen'  => ( isset( $this->config['oneopen'] ) ? $this->config['oneopen'] : true ),
		);
		$this->config_json     = wp_json_encode( $config_embed );
		$container_class_array = array();

		// Start Output.
		$output = '';

		// Default Container Class.
		$container_class_array[] = 'bf-admin-api bf-repeater-container';

		// Set our classes up as a string to use.
		$container_classes = implode( ' ', $container_class_array );

		// Build Callback Json for repeater object.
		// We only need callback data here.
		$callback_array   = array(
			'title_cb'        => isset( $this->config['title_cb'] ) ? $this->config['title_cb'] : array(),
			'fields_cb'       => isset( $this->config['fields_cb'] ) ? $this->config['fields_cb'] : array(),
			'title_fields_cb' => isset( $this->config['title_fields_cb'] ) ? $this->config['title_fields_cb'] : array(),
		);
		$callback_json    = wp_json_encode( $callback_array );
		$js_callback_json = wp_json_encode( isset( $this->config['js_cb'] ) ? $this->config['js_cb'] : array() );

		// Parent Container.
		$attributes = self::get_field_attributes();
		$output    .= "<div class='{$container_classes}' data-config='{$this->config_json}' data-callbacks='{$callback_json}' data-js-callbacks='{$js_callback_json}' {$attributes}>";

		// Loop through existing repeaters.
		if ( ! empty( $this->config['instance'] ) ) {
			// phpcs:ignore
			foreach ( $this->config['instance'] as $index => $values ) { // Values should exist as $values['title_bar'] and $values['field'].
				$output .= \BrightFire\Admin_API\build_repeater( $callback_array, (array) $values );
			}
		} else {
			$output .= \BrightFire\Admin_API\build_repeater( $callback_array );
		}

		// Close Parent Container.
		$output .= '</div>'; // repeater-container.

		// We need this for our buttons.
		wp_enqueue_style( 'fontawesome' );

		// Set and Return Field Markup Output.
		self::set_field_output( $output );
		return self::get_field_output();

	}


}
