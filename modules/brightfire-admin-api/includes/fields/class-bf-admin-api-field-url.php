<?php
/**
 * Class BF_Admin_API_Field_URL
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Field_URL
 */
class BF_Admin_API_Field_URL extends BF_Admin_API_Field_Base {

	/**
	 * Creates a URL or WP_URL Field
	 *
	 * @return mixed
	 */
	public function create_field() {

		$type = self::get_config( 'type' );

		switch ( $type ) {
			case 'url':
				$class = 'url';
				break;
			case 'wp_url':
				$class = 'wp_url';
				break;
			default:
				$class = 'url';
				break;
		}

		// The Field HTML.
		$output = '<input type="text" id="' . self::get_field_id() . '" name="' . self::get_field_name() . '" value="' . self::get_field_value() . '" class="bf-admin-field ' . esc_attr( $class ) . ' widefat ' . self::get_field_classes() . '" ' . self::get_field_attributes() . ' />' . "\n";

		self::set_field_output( $output );

		return self::get_field_output();

	}

}
