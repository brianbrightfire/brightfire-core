<?php
/**
 * Class BF_Admin_API_Field_Range
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Field_Range
 */
class BF_Admin_API_Field_Range extends BF_Admin_API_Field_Base {

	/**
	 * Range.
	 *
	 * @var string $type
	 */
	private $type = 'range';

	/**
	 * Creates a Text Field
	 *
	 * @return mixed
	 */
	public function create_field() {

		// Get Settings.
		$settings             = self::get_field_settings();
		$setting_min_label    = ( isset( $settings['min-label'] ) ? $settings['min-label'] : null );
		$setting_max_label    = ( isset( $settings['max-label'] ) ? $settings['max-label'] : null );
		$setting_show_current = ( isset( $settings['show-current'] ) ? $settings['show-current'] : null );
		$setting_units        = ( isset( $settings['units'] ) ? $settings['units'] : '' );

		// Get a unique Identifier for this slider.
		$unique_id = uniqid( 'slider-' );

		// The Field HTML.
		$output = '';

		$output .= ( isset( $setting_show_current ) ? '<span class="range-current-output" data-slider-id="' . $unique_id . '">CURRENT</span>' : '' );

		$output .= '<input type="range" id="' . self::get_field_id() . '" name="' . self::get_field_name() . '" list="' . self::get_field_id() . '_list" value="' . self::get_field_value() . '" data-slider-units="' . $setting_units . '" data-slider-id="' . $unique_id . '" class="bf-admin-field ' . esc_attr( $this->type ) . ' ' . self::get_field_classes() . '" ' . self::get_field_attributes() . ' />' . "\n";

		$output .= ( isset( $setting_min_label ) ? "<span class='min-label'>{$setting_min_label} {$setting_units}</span>" : '' );
		$output .= ( isset( $setting_max_label ) ? "<span class='max-label'>{$setting_max_label} {$setting_units}</span>" : '' );
		$output .= '<div class="clear"></div>';

		// TODO: $field_config is an undefined variable. At some point it may have been a parameter to this function call, but is no longer the case.
		// Options List if Min and Max are provided.
		if ( isset( $field_config['atts']['min'] ) && isset( $field_config['atts']['max'] ) ) {
			$output .= '<datalist id="' . self::get_field_id() . '_list">';
			$i       = $field_config['atts']['min'];
			while ( $i <= $field_config['atts']['max'] ) {
				$output .= '<option>' . $i . '</option>';
				$i       = ( isset( $field_config['atts']['step'] ) ? $i + $field_config['atts']['step'] : $i + 1 );
			}
			$output .= '</datalist>';
		}

		self::set_field_output( $output );

		return self::get_field_output();

	}

}
