<?php
/**
 * Class BF_Admin_API_Field_Date
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Field_Date
 */
class BF_Admin_API_Field_Date extends BF_Admin_API_Field_Base {

	/**
	 * Type.
	 *
	 * @var string $type
	 */
	private $type = 'date';

	/**
	 * Creates a Text Field.
	 *
	 * @return mixed
	 */
	public function create_field() {

		// The Field HTML.
		$output = '<input type="text" id="' . self::get_field_id() . '" name="' . self::get_field_name() . '" value="' . self::get_field_value() . '" class="widefat bf-admin-field ' . esc_attr( $this->type ) . '" ' . self::get_field_attributes() . ' />' . "\n";

		self::set_field_output( $output );

		return self::get_field_output();

	}

}
