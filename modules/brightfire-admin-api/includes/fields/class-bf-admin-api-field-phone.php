<?php
/**
 * Class BF_Admin_API_Field_Phone
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Field_Phone
 */
class BF_Admin_API_Field_Phone extends BF_Admin_API_Field_Base {

	/**
	 * Type.
	 *
	 * @var string $type
	 */
	private $type = 'phone';

	/**
	 * Creates a Telephone Field.
	 *
	 * @return mixed
	 */
	public function create_field() {

		$val = self::get_field_value();

		if ( is_array( $val ) ) {
			$phone_val = self::get_field_value( 'phone' );
			$ext_val   = self::get_field_value( 'ext' );
		} else {
			$phone_val = $val;
			$ext_val   = '';
		}

		// The Field HTML.
		$output  = '<table width="100%" cellspacing="0" cellpadding="0">';
		$output .= '<tr>';
		$output .= '<td width="75%" style="padding-left: 0;">';
		$output .= '<label for="' . self::get_field_name() . '[phone]" class="phone-label">Phone</label>';
		$output .= '<input type="tel" id="' . self::get_field_id() . '[phone]" name="' . self::get_field_name() . '[phone]" value="' . $phone_val . '" class="bf-admin-field ' . esc_attr( $this->type ) . ' widefat ' . self::get_field_classes() . '" ' . self::get_field_attributes() . ' />' . "\n";
		$output .= '</td>';
		$output .= '<td width="25%" style="padding-right: 0;">';
		$output .= '<label for="' . self::get_field_name() . '[ext]" class="phone-label">Extension</label>';
		$output .= '<input type="tel" id="' . self::get_field_id() . '[ext]" name="' . self::get_field_name() . '[ext]" value="' . $ext_val . '" class="bf-admin-field ' . esc_attr( $this->type ) . '-ext widefat ' . self::get_field_classes() . '" ' . self::get_field_attributes() . ' />' . "\n";
		$output .= '</td>';
		$output .= '</tr>';
		$output .= '</table>';

		self::set_field_output( $output );

		return self::get_field_output();

	}

}
