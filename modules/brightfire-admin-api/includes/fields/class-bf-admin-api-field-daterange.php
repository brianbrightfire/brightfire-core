<?php
/**
 * Class BF_Admin_API_Field_Daterange
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Field_Daterange
 */
class BF_Admin_API_Field_Daterange extends BF_Admin_API_Field_Base {

	/**
	 * Type.
	 *
	 * @var string $type
	 */
	private $type = 'daterange';

	/**
	 * Creates a Schedule date field pair for start and end Field
	 *
	 * @return mixed
	 */
	public function create_field() {

		$disabled = self::get_config()['disable'];

		if ( $disabled ) {
			$disable_attr = ' datepicker-disable = " ' . htmlentities( wp_json_encode( $disabled ) ) . '"';
		} else {
			$disable_attr = '';
		}

		// The Field HTML.
		$output  = '<table width="100%"><tr>';
		$output .= '<td width="45%" style="padding: 0;"><input type="text" id="' . self::get_field_id() . '[start]" name="' . self::get_field_name() . '[start]" value="' . self::get_field_value( 'start' ) . '" class="widefat bf-admin-field startdate ' . esc_attr( $this->type ) . '" placeholder="Start Date" ' . self::get_field_attributes() . $disable_attr . ' /></td>' . "\n";
		$output .= '<td width="10%" style="padding: 0 24px; text-align: center;">TO</td>';
		$output .= '<td width="45%" style="padding: 0;"><input type="text" id="' . self::get_field_id() . '[end]" name="' . self::get_field_name() . '[end]" value="' . self::get_field_value( 'end' ) . '" class="widefat bf-admin-field enddate ' . esc_attr( $this->type ) . '" placeholder="End Date" ' . self::get_field_attributes() . $disable_attr . ' /></td>' . "\n";
		$output .= '</tr></table>';

		self::set_field_output( $output );

		return self::get_field_output();

	}

}
