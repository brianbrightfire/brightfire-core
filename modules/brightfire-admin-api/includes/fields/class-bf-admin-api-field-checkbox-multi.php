<?php
/**
 * Class BF_Admin_API_Field_Checkbox_Multi
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Field_Checkbox_Multi
 */
class BF_Admin_API_Field_Checkbox_Multi extends BF_Admin_API_Field_Base {

	/**
	 * Create our select field
	 *
	 * @return mixed
	 */
	public function create_field() {

		// Format Options.
		$choices = self::get_field_choices();
		$value   = $this->format_values( self::get_field_value() );
		$options = $this->format_choices( $choices );

		// The Field HTML.
		$output = '';

		$output .= '<div class="bf-admin-api checkboxes' . self::get_field_classes() . '" ' . self::get_field_attributes() . ' >';
		$output .= '<input type="hidden" id="' . self::get_field_id() . '" name="' . self::get_field_name() . '" value="' . $value . '" class="widefat bf-admin-field ' . esc_attr( self::get_config( 'type' ) ) . ' ' . self::get_field_classes() . '" ' . self::get_field_attributes() . ' />' . "\n";

		$output .= $options;
		$output .= '</div>';

		self::set_field_output( $output );

		return self::get_field_output();

	}

	/**
	 * Format our choices into <option> tags with the proper selection
	 *
	 * @param array $choices Choices.
	 *
	 * @return string
	 */
	public function format_choices( $choices = array() ) {

		$output = '';

		// Default Defined Choices.
		if ( is_array( $choices ) ) {

			foreach ( $choices as $option_value => $name ) {
				$output .= '<div class="checkbox-option">';
				$output .= "<input type=\"checkbox\" value='{$option_value}' /><span class=\"checkbox-label\">{$name}</span>";
				$output .= '</div>';

			}
		}

		return $output;
	}

	/**
	 * Select values should always be an array.
	 *
	 * @param array $values Values.
	 *
	 * @return array
	 */
	public function format_values( $values ) {

		// If not an array, let's try to make it one.
		if ( is_array( $values ) ) {
			$values = implode( ',', $values );
		}

		return $values;
	}
}
