<?php
/**
 * Class BF_Admin_API_Field_Textarea
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Field_Textarea
 */
class BF_Admin_API_Field_Textarea extends BF_Admin_API_Field_Base {

	/**
	 * Type.
	 *
	 * @var string $type
	 */
	private $type = 'textarea';

	/**
	 * Creates a Text Field
	 *
	 * @return mixed
	 */
	public function create_field() {

		// The Field HTML.
		$output = '<textarea id="' . self::get_field_id() . '" name="' . self::get_field_name() . '" class="bf-admin-field ' . esc_attr( $this->type ) . ' widefat ' . self::get_field_classes() . '" ' . self::get_field_attributes() . ' >' . self::get_field_value() . '</textarea>' . "\n";

		self::set_field_output( $output );

		return self::get_field_output();

	}

}
