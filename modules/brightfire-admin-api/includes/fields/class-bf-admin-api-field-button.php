<?php
/**
 * BrightFire Admin API Field Button.
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Field_Button
 *
 * This class generates a specific field
 *
 * Responsibility: Generate markup for a specific input field.
 */
class BF_Admin_API_Field_Button extends BF_Admin_API_Field_Base {

	/**
	 * Type.
	 *
	 * @var string $type
	 */
	private $type = 'button';

	/**
	 * Creates a Button.
	 *
	 * @return mixed
	 */
	public function create_field() {

		// The Field HTML.
		$output = '<input type="button" id="' . self::get_field_id() . '" name="' . self::get_field_name() . '" value="' . $this->config['label'] . '" class="button bf-admin-field ' . esc_attr( $this->type ) . ' ' . self::get_field_classes() . '" ' . self::get_field_attributes() . ' />' . "\n";

		self::set_field_output( $output );

		return self::get_field_output();

	}

}
