<?php
/**
 * Class BF_Admin_API_Field_WP_Editor
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Field_WP_Editor
 */
class BF_Admin_API_Field_WP_Editor extends BF_Admin_API_Field_Base {

	/**
	 * Type.
	 *
	 * @var string $type
	 */
	private $type = 'wp_editor';

	/**
	 * Creates a TinyMCE Field
	 *
	 * @return mixed
	 */
	public function create_field() {

		$value = html_entity_decode( stripslashes( self::get_field_value() ) );

		/**
		 * TinyMCE editor IDs cannot have brackets.
		 */
		$field_id = str_replace( array( '[', ']' ), '', self::get_field_id() );

		ob_start();
		wp_editor(
			$value,
			$field_id,
			array(
				'wpautop'       => true,
				'media_buttons' => false,
				'editor_class'  => self::get_field_classes(),
				'textarea_name' => self::get_field_name(),
				'textarea_rows' => 10,
				'teeny'         => true,
			)
		);
		$output = ob_get_clean();

		self::set_field_output( $output );

		return self::get_field_output();

	}
}
