<?php
/**
 * Class BF_Admin_API_Field_WP_Color
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Field_WP_Color
 */
class BF_Admin_API_Field_WP_Color extends BF_Admin_API_Field_Base {

	/**
	 * Type.
	 *
	 * @var string $type
	 */
	private $type = 'wp_color';

	/**
	 * Creates a Text Field
	 *
	 * @return mixed
	 */
	public function create_field() {

		$classes = self::get_field_classes();

		$output       = '';
		$color_helper = '';

		if ( $this->get_config( 'color-helper' ) && \BrightFireCore\is_brightfire_stellar_theme() ) {

			$color_helper .= "<button class='color-picker-toggle'>My Colors &#x25BC;</button>";
			$color_helper .= "<div style='clear: both;'></div>";

			$color_helper .= "<div class='color-picker-helper' data-target='" . self::get_field_id() . "'>";

				// Primary Color.
				$color_helper     .= "<div class='helper-column'>";
					$color_helper .= '<div class="helper-label">Primary</div>';
					$color_helper .= "<div class='color-swatch background-primary shade2'></div>";
					$color_helper .= "<div class='color-swatch background-primary shade1'></div>";
					$color_helper .= "<div class='color-swatch background-primary'></div>";
					$color_helper .= "<div class='color-swatch background-primary tint1'></div>";
					$color_helper .= "<div class='color-swatch background-primary tint2'></div>";
				$color_helper     .= '</div>';

				// Secondary Color.
				$color_helper     .= "<div class='helper-column'>";
					$color_helper .= '<div class="helper-label">Secondary</div>';
					$color_helper .= "<div class='color-swatch background-secondary shade2'></div>";
					$color_helper .= "<div class='color-swatch background-secondary shade1'></div>";
					$color_helper .= "<div class='color-swatch background-secondary'></div>";
					$color_helper .= "<div class='color-swatch background-secondary tint1'></div>";
					$color_helper .= "<div class='color-swatch background-secondary tint2'></div>";
				$color_helper     .= '</div>';

				// Tertiary Color.
				$color_helper     .= "<div class='helper-column'>";
					$color_helper .= '<div class="helper-label">CTA</div>';
					$color_helper .= "<div class='color-swatch background-tertiary shade2'></div>";
					$color_helper .= "<div class='color-swatch background-tertiary shade1'></div>";
					$color_helper .= "<div class='color-swatch background-tertiary'></div>";
					$color_helper .= "<div class='color-swatch background-tertiary tint1'></div>";
					$color_helper .= "<div class='color-swatch background-tertiary tint2'></div>";
				$color_helper     .= '</div>';

				// Accent Color.
				$color_helper     .= "<div class='helper-column'>";
					$color_helper .= '<div class="helper-label">Links</div>';
					$color_helper .= "<div class='color-swatch background-accent shade2'></div>";
					$color_helper .= "<div class='color-swatch background-accent shade1'></div>";
					$color_helper .= "<div class='color-swatch background-accent'></div>";
					$color_helper .= "<div class='color-swatch background-accent tint1'></div>";
					$color_helper .= "<div class='color-swatch background-accent tint2'></div>";
				$color_helper     .= '</div>';

			$color_helper .= '</div>';

		} else {

			$color_helper .= "<div style='clear: both;'></div>";

		}

		// The Field HTML.
		$output .= '<input type="text" id="' . self::get_field_id() . '" name="' . self::get_field_name() . '" value="' . self::get_field_value() . '" class="bf-admin-field color-picker ' . esc_attr( $this->type ) . ' ' . $classes . '" ' . self::get_field_attributes() . ' />' . "\n";
		$output .= $color_helper;

		self::set_field_output( $output );

		return self::get_field_output();

	}

}
