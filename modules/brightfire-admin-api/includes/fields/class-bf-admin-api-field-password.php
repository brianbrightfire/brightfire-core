<?php
/**
 * Class BF_Admin_API_Field_Password
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Field_Password
 */
class BF_Admin_API_Field_Password extends BF_Admin_API_Field_Base {

	/**
	 * Type.
	 *
	 * @var string $type
	 */
	private $type = 'password';

	/**
	 * Creates a Text Field.
	 *
	 * @return mixed
	 */
	public function create_field() {

		// The Field HTML.
		$output = '<input type="password" id="' . self::get_field_id() . '" name="' . self::get_field_name() . '" value="' . self::get_field_value() . '" class="bf-admin-field ' . esc_attr( $this->type ) . ' ' . self::get_field_classes() . '" ' . self::get_field_attributes() . ' />' . "\n";

		self::set_field_output( $output );

		return self::get_field_output();

	}

}
