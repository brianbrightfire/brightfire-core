<?php
/**
 * Class BF_Admin_API_Sanitize_URL
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Sanitize_URL
 */
class BF_Admin_API_Sanitize_URL extends BF_Admin_API_Sanitize {

	/**
	 * Sanitize value as a URL
	 *
	 * @param mixed $value Value.
	 *
	 * @return mixed
	 */
	public static function sanitize( $value ) {

		return esc_url( $value );

	}

}
