<?php
/**
 * Class BF_Admin_API_Sanitize_Phone
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Sanitize_Phone
 */
class BF_Admin_API_Sanitize_Phone extends BF_Admin_API_Sanitize {

	/**
	 * Sanitize value as a phone number.
	 *
	 * @param mixed $value Value.
	 *
	 * @return mixed
	 */
	public static function sanitize( $value ) {

		// Remove invalid characters (non-numeric).
		$value = preg_replace( '/[^0-9]/', '', $value );

		// Find length.
		$len = strlen( $value );

		// Format properly.
		if ( 7 === $len ) {
			$value = preg_replace( '/([0-9]{3})([0-9]{4})/', '$1-$2', $value );
		} elseif ( 10 === $len ) {
			$value = preg_replace( '/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $value );
		}

		return $value;
	}

}
