<?php
/**
 * Class BF_Admin_API_Sanitize_Integer
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Sanitize_Integer
 */
class BF_Admin_API_Sanitize_Integer extends BF_Admin_API_Sanitize {

	/**
	 * Sanitize value as an integer
	 *
	 * @param mixed $value Value.
	 *
	 * @return mixed
	 */
	public static function sanitize( $value ) {

		return filter_var( $value, FILTER_SANITIZE_NUMBER_INT );

	}

}
