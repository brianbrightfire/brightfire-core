<?php
/**
 * Class BF_Admin_API_Sanitize_Callback
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Sanitize_Callback
 *
 * Checks for a provided callback function to sanitize through
 */
class BF_Admin_API_Sanitize_Callback extends BF_Admin_API_Sanitize {

	/**
	 * Callback exists.
	 *
	 * @param mixed $callback Callback.
	 * @return bool
	 */
	public static function callback_exists( $callback ) {

		return ( function_exists( $callback ) ? true : false );

	}

	/**
	 * Sanitize value as a Callback.
	 *
	 * @param mixed $callback Callback.
	 * @param mixed $value Value.
	 *
	 * @return mixed
	 */
	public static function sanitize_callback( $callback, $value ) {

		return (string) call_user_func( $callback, $value );

	}

}

new BF_Admin_API_Sanitize_Callback();
