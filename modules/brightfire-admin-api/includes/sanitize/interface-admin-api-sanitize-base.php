<?php
/**
 * Admin API Sanitize Base.
 *
 * @package BrightFireCore
 */

/**
 * Interface Admin_API_Sanitize_Base
 */
interface Admin_API_Sanitize_Base {

	// phpcs:disable
	static function run( $sanitize_type, $value );

	static function sanitize( $value );
	// phpcs:enable

}
