<?php
/**
 * Class BF_Admin_API_Sanitize_Base
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Sanitize_Base
 */
class BF_Admin_API_Sanitize_Base implements Admin_API_Sanitize_Base {

	/**
	 * Run.
	 *
	 * @param string $sanitize_type Sanitize Type.
	 * @param mixed  $value Value.
	 * @return mixed|string
	 */
	public static function run( $sanitize_type, $value ) {

		if ( 'bypass' === $sanitize_type ) {
			return $value;
		}

		$options   = BF_Admin_API_Sanitize::bf_admin_api_sanitize_array();
		$classname = ( isset( $options[ $sanitize_type ] ) ? $options[ $sanitize_type ] : '' );

		if ( ! empty( $classname ) ) {
			$sanitize = new $classname();
			$return   = $sanitize->sanitize( $value );
		} else {
			// Try for Callback or run default sanitize.
			if ( BF_Admin_API_Sanitize_Callback::callback_exists( $sanitize_type ) ) {
				$return = BF_Admin_API_Sanitize_Callback::sanitize_callback( $sanitize_type, $value );
			} else {
				$return = self::sanitize( $value );
			}
		}

		return $return;

	}

	/**
	 * Our Default sanitation to prevent XSS issues
	 *
	 * @param mixed $value Value.
	 *
	 * @return mixed
	 */
	public static function sanitize( $value ) {

		if ( is_array( $value ) ) {

			$sanitized_array = array();

			foreach ( $value as $current_value ) {

				$new_value = sanitize_text_field( $current_value );

				$sanitized_array[] = $new_value;

			}

			return $sanitized_array;

		} else {

			return sanitize_text_field( $value );
		}
	}
}

new BF_Admin_API_Sanitize_Base();
