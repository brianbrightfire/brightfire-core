<?php
/**
 * Class BF_Admin_API_Sanitize_Email
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Sanitize_Email
 */
class BF_Admin_API_Sanitize_Email extends BF_Admin_API_Sanitize {

	/**
	 * Sanitize value as an email.
	 *
	 * @param mixed $value Value.
	 *
	 * @return mixed
	 */
	public static function sanitize( $value ) {

		// Remove all illegal characters from email address.
		return sanitize_email( $value );

	}

}
