<?php
/**
 * Class BF_Admin_API_Sanitize_Boolean
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Sanitize_Boolean
 */
class BF_Admin_API_Sanitize_Boolean extends BF_Admin_API_Sanitize {

	/**
	 * Sanitize value as a boolean value
	 *
	 * @param mixed $value Value.
	 *
	 * @return mixed
	 */
	public static function sanitize( $value ) {

		// Accepts 1, true, on and yes.
		return ( true === filter_var( $value, FILTER_VALIDATE_BOOLEAN ) ? $value : '' );

	}

}
