<?php
/**
 * Class BF_Admin_API_Sanitize_Date
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Sanitize_Date
 */
class BF_Admin_API_Sanitize_Date extends BF_Admin_API_Sanitize {

	/**
	 * Sanitize value as a date.
	 *
	 * @param mixed $value Value.
	 *
	 * @return mixed
	 */
	public static function sanitize( $value ) {

		return date( 'm/d/Y', strtotime( $value ) );

	}

}
