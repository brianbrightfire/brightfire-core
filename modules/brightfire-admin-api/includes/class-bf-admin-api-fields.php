<?php
/**
 * BrightFire Admin API Fields.
 *
 * @package BrightFireCore
 */

/**
 * Class BF_Admin_API_Fields
 * Responsibility: Default registration of field types, and build functions
 */
class BF_Admin_API_Fields {

	/**
	 * Config for all API Classes.
	 *     array index should be registered field type.
	 *     array value should be class name.
	 *
	 * @return array
	 */
	public static function bf_admin_api_field_array() {
		$bf_admin_api_fields = array(
			'button'             => 'BF_Admin_API_Field_Button',
			'checkbox_multi'     => 'BF_Admin_API_Field_Checkbox_Multi',
			'codeblock'          => 'BF_Admin_API_Field_Codeblock',
			'date'               => 'BF_Admin_API_Field_Date',
			'daterange'          => 'BF_Admin_API_Field_Daterange',
			'email'              => 'BF_Admin_API_Field_Email',
			'fontawesome_select' => 'BF_Admin_API_Field_Select',
			'hidden'             => 'BF_Admin_API_Field_Hidden',
			'number'             => 'BF_Admin_API_Field_Number',
			'password'           => 'BF_Admin_API_Field_Password',
			'phone'              => 'BF_Admin_API_Field_Phone',
			'range'              => 'BF_Admin_API_Field_Range',
			'repeater'           => 'BF_Admin_API_Field_Repeater',
			'select'             => 'BF_Admin_API_Field_Select',
			'selectize'          => 'BF_Admin_API_Field_Select',
			'text'               => 'BF_Admin_API_Field_Text',
			'textarea'           => 'BF_Admin_API_Field_Textarea',
			'url'                => 'BF_Admin_API_Field_URL',
			'wp_url'             => 'BF_Admin_API_Field_URL',
			'wp_page_select'     => 'BF_Admin_API_Field_Select',
			'wp_color'           => 'BF_Admin_API_Field_WP_Color',
			'wp_image'           => 'BF_Admin_API_Field_WP_Image',
			'wp_editor'          => 'BF_Admin_API_Field_WP_Editor',
		);

		$bf_admin_api_fields = apply_filters( 'bf_admin_api_fields', $bf_admin_api_fields );

		return $bf_admin_api_fields;
	}

	/**
	 * Entry point to build a form with the API.
	 *
	 * @param array $args Our Fields we want to generate.
	 *
	 * @return string
	 */
	public static function build( $args = array() ) {
		$build = new BF_Admin_API_Build( $args );
		return $build->output();
	}

}
