<?php
/**
 * Plugin Name: BrightFire SMTP
 * Version: 1.0
 * Plugin URI: http://www.brightfire.com
 * Author: mattmckenny
 * Author URI: http://www.brightfgire.com
 * Description: Send email via SMTP for all outgoing email in network.
 *
 * @package BrightFireCore
 */

// Include files.
require_once ABSPATH . WPINC . '/class-phpmailer.php';

// We need user and password before doing anything.
if ( defined( 'BF_SMTP_USER' ) && defined( 'BF_SMTP_PASS' ) ) {
	add_action( 'phpmailer_init', 'bf_smtp_mailer' );
}

/**
 * BF SMTP Mailer.
 *
 * @param object $phpmailer PHP Mailer object.
 */
function bf_smtp_mailer( $phpmailer ) {
	// You can NOT set the From address if using Gmail.
	// "from" address will default to the email account used for SMTP authentication.
	if ( defined( 'BF_SMTP_FROM_NAME' ) ) {
		$phpmailer->FromName = BF_SMTP_FROM_NAME; // phpcs:ignore
	}

	// Override the defined constant for the From name if filter has been used.
	$from_name = apply_filters( 'brightfire_wp_smtp_from_name', '' );
	if ( $from_name ) {
		$phpmailer->FromName = $from_name; // phpcs:ignore
	}

	// Fix for local ssl.
	if ( defined( 'BF_ENV' ) && ( 'DEV' === BF_ENV ) ) {
		$phpmailer->SMTPOptions = array( // phpcs:ignore
			'ssl' => array(
				'verify_peer' => false,
			),
		);
	}

	// phpcs:disable
	$phpmailer->IsSMTP();
	$phpmailer->SMTPAuth   = true;
	$phpmailer->SMTPSecure = 'tls';
	$phpmailer->Host       = 'smtp.gmail.com';
	$phpmailer->Port       = 587;
	$phpmailer->Username   = BF_SMTP_USER;
	$phpmailer->Password   = BF_SMTP_PASS;
	// phpcs:enable
}
