<?php
/**
 * BrightFire Defer JS.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Defer_JS;

/** Require Files */
require_once dirname( __FILE__ ) . '/filters.php';

/**
 * Add defer to JS tags
 *
 * @uses add_js_defer_attribute();
 */
add_filter( 'script_loader_tag', __NAMESPACE__ . '\add_js_defer_attribute', 10, 2 );
