<?php
/**
 * BrightFire Defer JS.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Defer_JS;

/**
 * Adds defer="defer" to all JS tags with an src attribute
 *
 * @param string $tag Tag.
 * @param string $handle Handle.
 *
 * @return mixed
 */
function add_js_defer_attribute( $tag, $handle ) {

	if ( is_admin() ) { // Never defer for admin - breaks stuff.
		return $tag;
	}

	// We need to exclude some library scripts because we.
	// utilize them with inline JS which will not be deferred.
	$exclude_defer = apply_filters(
		'brightfire_defer_js_exceptions',
		array(
			'brightfire-lightbox',
			'jquery-core',
			'google-maps-jsapi',
		)
	);

	foreach ( $exclude_defer as $exclude_handle ) {

		if ( $exclude_handle === $handle ) {
			return $tag;
		}
	}

	return str_replace( ' src', ' defer="defer" src', $tag );
}
