<?php
/**
 * BrightFire Environment.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Environment;

/**
 *
 * Returns either color or message if in STG or DEV environment, otherwise returns false
 */
function get_env_data() {

	if ( defined( 'BF_ENV' ) ) {

		$color   = '';
		$message = '';
		$version = '';

		if ( 'DEV' === BF_ENV ) {
			$stringfromfile = file( ABSPATH . '.git/HEAD' );
			$first_line     = $stringfromfile[0]; // get the string from the array.
			$explodedstring = explode( '/', $first_line, 3 ); // seperate out by the "/" in the string.
			$branchname     = $explodedstring[2]; // get the one that is always the branch name.

			$message = 'LOCAL DEV: ' . $branchname;
			$color   = '#bf1111';
		} elseif ( 'STG' === BF_ENV ) {
			$message = 'STAGING SERVER: ' . apply_filters( 'bf_release_version', BF_CORE_VERSION . 'C' ); // Put a 'C' tag on the end to denote Core version is being shown (by default).
			$color   = '#D85F00';
		}

		if ( in_array( BF_ENV, array( 'DEV', 'STG' ), true ) ) {

			return array(
				'color'   => $color,
				'message' => $message,
				'version' => $version,
			);
		}

		return false;
	}
}



/**
 * Add notice banner to front end if in dev or staging environments
 **/
function environment_banner() {

	$environment_data = get_env_data();

	if ( $environment_data['color'] ) {
		?>
		<style>

			#bf-environment-tag{
				font-family: sans-serif;
				font-size: 12px;
				-webkit-font-smoothing: inherit;
				line-height: 1;
				color: #fff;
				position: fixed;
				bottom: 0;
				right: 0;
				z-index: 99999;
				pointer-events: none;
				font-weight: bold;
				background: <?php echo $environment_data['color']; ?>;
				padding: 8px 10px;

			}
		</style>
		<?php
		echo '<div id="bf-environment-tag"><span>' . $environment_data['message'] . '</span></div>';

	}
}

/**
 * Provides hook that runs only if the insurance network release version isn't the same
 * as the BF_RELEASE_VERSION constant
 *
 * @return bool
 */
function bf_do_deploy_hook() {

	$current_version = apply_filters( 'bf_release_version', BF_CORE_VERSION . 'C' );
	$saved_version   = get_site_option( 'bf_release_version' );

	if ( $current_version && ( $current_version !== $saved_version ) ) {
		do_action( 'bf_deploy_hook', $current_version );
		update_site_option( 'bf_release_version', $current_version );

		// Deploy version updated.
		return true;
	}

	// Nothing was updated.
	return false;

}


/**
 * Add BrightFire release version to the admin footer
 */
function admin_release_version() {
	add_filter(
		'admin_footer_text',
		function() {
			return 'Thank you for using <a href="https://www.inswebsites.com" target="_blank">BrightFire Insurance Websites</a>!';
		}
	);
	add_filter(
		'update_footer',
		function() {
			return 'BrightFire v.' . apply_filters( 'bf_release_version', BF_CORE_VERSION . 'C' );
		},
		99
	);
}

