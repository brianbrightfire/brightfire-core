<?php
/**
 * BrightFire Environment.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Environment;

/** Require Files */
require_once dirname( __FILE__ ) . '/actions.php';

/**
 * Add notice banner to front end if in dev or staging environments
 */
add_action( 'wp_footer', __NAMESPACE__ . '\environment_banner', 999 );
add_action( 'admin_footer', __NAMESPACE__ . '\environment_banner', 999 );

/**
 * Update our bf_release_version option in the db
 */
add_action( 'init', __NAMESPACE__ . '\bf_do_deploy_hook' );

/**
 * Add BrightFire version to admin footer
 *
 * @uses admin_release_version
 */
add_action( 'admin_init', __NAMESPACE__ . '\admin_release_version' );
