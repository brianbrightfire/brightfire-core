<?php
/**
 * BrightFire Pingbacks.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Pingbacks;

/** Require Files */
require_once dirname( __FILE__ ) . '/actions.php';
require_once dirname( __FILE__ ) . '/filters.php';

/**
 * Disables xmlrpc pingbacks
 *
 * @uses remove_pingback_method
 */
add_filter( 'xmlrpc_methods', __NAMESPACE__ . '\remove_pingback_method' );

/**
 * Disables do_pings if running CRON
 */
add_action( 'init', __NAMESPACE__ . '\remove_do_pings_from_cron' );
