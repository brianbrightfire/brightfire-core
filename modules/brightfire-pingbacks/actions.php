<?php
/**
 * BrightFire Pingbacks.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Pingbacks;

/**
 * Remove do_pings if CRON.
 */
function remove_do_pings_from_cron() {
	if ( isset( $_GET['doing_wp_cron'] ) ) { // phpcs:ignore
		remove_action( 'do_pings', 'do_all_pings' );
		wp_clear_scheduled_hook( 'do_pings' );
	}
}
