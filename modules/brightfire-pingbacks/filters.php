<?php
/**
 * BrightFire Pingbacks.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Pingbacks;

/**
 * Disables xmlrpc pingbacks
 *
 * @link http://blog.sucuri.net/2014/03/more-than-162000-wordpress-sites-used-for-distributed-denial-of-service-attack.html
 *
 * @param array $methods Methods.
 *
 * @return array
 */
function remove_pingback_method( $methods ) {
	unset( $methods['pingback.ping'] );
	unset( $methods['pingback.extensions.getPingbacks'] );

	return $methods;
}
