<?php
/**
 * BrightFire Footer Credit.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Footer_Credit;

/**
 * BrightFire Footer Credit widget class
 */
class BrightFire_Footer_Credit_Widget extends \WP_Widget {

	/**
	 * Defaults.
	 *
	 * @var array $defaults
	 */
	private $defaults;

	/**
	 * BrightFire_Footer_Credit_Widget constructor.
	 */
	public function __construct() {

		$this->defaults = array(
			'dark_scheme' => false,
			'login_link'  => false,
		);

		$widget_ops = array(
			'classname'                   => 'brightfire-footer-credit-widget',
			'description'                 => __( 'BrightFire branded copyright and links.' ),
			'customize_selective_refresh' => true,
		);

		parent::__construct( 'brightfire_footer_credit_widget', __( 'BrightFire Footer Credits Widget' ), $widget_ops );
	}

	/**
	 * Front End Display.
	 *
	 * @param array $args Arguments.
	 * @param array $instance Instance.
	 */
	public function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		echo $args['before_widget'];

		echo bf_footer_credit( $instance );

		echo $args['after_widget'];

	}

	/**
	 * Admin display.
	 *
	 * @param array $instance Instance.
	 * @return string|void
	 */
	public function form( $instance ) {
		// Defaults.
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		$fields = array(
			'dark_scheme' => array(
				'type'    => 'selectize',
				'label'   => 'Use dark text and logo?',
				'choices' => 'yesno',
			),
			'login_link'  => array(
				'type'    => 'selectize',
				'label'   => 'Remove Login Link',
				'choices' => 'yesno',
			),
		);

		// Build our widget form.
		$args = array(
			'fields'   => $fields,
			'instance' => $instance,
			'widget'   => $this,
			'display'  => 'basic',
			'echo'     => true,
		);

		global $bf_admin_api;
		$bf_admin_api->fields->build( $args );

	}
}
