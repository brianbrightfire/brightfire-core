<?php
/**
 * BrightFire Footer Credit.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Footer_Credit;

require_once dirname( __FILE__ ) . '/class-brightfire-footer-credit-widget.php';
require_once dirname( __FILE__ ) . '/shortcode-bf-footer-credit.php';


/**
 * Register BrightFire_Footer_Credit_Widget
 */
function register_bf_footer_credit() {
	register_widget( __NAMESPACE__ . '\BrightFire_Footer_Credit_Widget' );
}
add_action( 'widgets_init', __NAMESPACE__ . '\register_bf_footer_credit' );

/** Add Shortcode(s) */
add_shortcode( 'bf_footer_credit', __NAMESPACE__ . '\bf_footer_credit' );

global $bf_sass;
$bf_sass->register_path( 'bf-core', dirname( __FILE__ ) . '/assets/_brightfire_footer_credits.scss' );
