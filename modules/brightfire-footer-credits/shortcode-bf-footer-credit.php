<?php
/**
 * BrightFire Footer Credit.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Footer_Credit;

/**
 * Footer credits output
 *
 * @param array $atts Attributes.
 *          'dark_scheme'   => (bool) Defaults to false. Show dark rather than light version.
 *          'login_link'    => (bool) Defaults to false. Hide login link.
 *          'copyright'     => (bool) Defaults to true. Show copyright section.
 *          'credit'        => (bool) Defaults to true. Show credit section.
 * @return string
 */
function bf_footer_credit( $atts ) {

	$atts = shortcode_atts(
		array(
			'dark_scheme' => false,
			'login_link'  => false,
			'copyright'   => true,
			'credit'      => true,
		),
		$atts,
		'bf_footer_credit'
	);

	// Color Scheme.
	if ( ( 'on' === $atts['dark_scheme'] ) || ( true === $atts['dark_scheme'] ) ) {
		$class = 'dark';
		$logo  = BF_CORE_ASSETS . 'images/brightfire-logo-dark.svg';
	} else {
		$class = 'light';
		$logo  = BF_CORE_ASSETS . 'images/brightfire-logo-white.svg';
	}

	// Login Link.
	if ( ( 'off' === $atts['login_link'] ) || ( true === $atts['login_link'] ) ) {
		$login = '';
	} else {
		$login = ' | <a class="login" rel="nofollow" href="' . admin_url( '', 'https' ) . '">Login</a>';
	}

	$link_atts = array(
		'text' => 'Websites',
		'alt'  => 'BrightFire',
		'url'  => 'https://www.brightfire.com',
	);

	$link_atts = apply_filters( 'bf_footer_credits_link_atts', $link_atts );

	$company_name = apply_filters( 'bf_footer_credits_company_name', get_bloginfo() );

	$copyright = ( true === $atts['copyright'] ) ? '<div class="copyright">&copy; Copyright ' . date( 'Y' ) . ', ' . $company_name . ' | <a rel="nofollow" href="' . site_url( '/copyrights-and-privacy-statement/' ) . '">Privacy Statement</a>' . $login . '</div>' : '';
	$credit    = ( true === $atts['credit'] ) ? '<div class="credit"><a href="' . $link_atts['url'] . '" rel="nofollow" target="_blank"><img class="credit-logo lazyload" width="110" alt="' . $link_atts['alt'] . '" height="20" data-src="' . $logo . '" /><span class="credit-text">' . $link_atts['text'] . '</span></a></div>' : '';

	$output  = '<div id="bf-cred" class="' . $class . '">';
	$output .= '<div class="container">';
	$output .= $copyright;
	$output .= $credit;
	$output .= '<div class="clear"></div>';
	$output .= '</div>';
	$output .= '</div>';

	return $output;
}
