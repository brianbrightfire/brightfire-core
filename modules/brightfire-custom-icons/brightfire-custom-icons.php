<?php
/**
 * BrightFire Custom Icons.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Custom_Icons;

/** Register styles for front end */
function styles() {
	wp_register_style( 'brightfire-custom-icons', BF_CORE_ASSETS . '/css/brightfire-custom-icons.min.css', [], BF_CORE_VERSION );
}
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\styles', 9 );
add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\styles', 9 );
