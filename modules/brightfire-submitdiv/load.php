<?php
/**
 * BrightFire Submit Div.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Submitdiv;

/**
 * Replaces WordPress default 'submitdiv' with a simplified version
 *
 * @param string $screen - What screen you want to replace the submitdiv on.
 * @param string $description - Optional Content to put in the metabox minor-publishing-actions.
 */
function replace_wp_submitdiv( $screen, $description = '' ) {
	add_action(
		'do_meta_boxes',
		function() use ( $screen, $description ) {
			replace_wp_submitdiv_hook( $screen, $description );
		},
		11
	);
}

/**
 * Replace WP Submit Div Hook.
 *
 * @param string $screen Screen.
 * @param string $description Description.
 */
function replace_wp_submitdiv_hook( $screen, $description ) {
	remove_meta_box( 'submitdiv', $screen, 'side' );
	add_meta_box( 'submitdiv', 'Save Changes', __NAMESPACE__ . '\bf_submitdiv', $screen, 'side', 'high', array( 'description' => $description ) );
}


/**
 * The rendering of the submit box.
 *
 * @param object $post Post.
 * @param array  $args Arguments.
 */
function bf_submitdiv( $post, $args ) {

	global $post;

	$nonce      = wp_create_nonce( 'trash-post_' . $post->ID );
	$post_id    = $post->ID;
	$delete_url = admin_url() . "post.php?post={$post_id}&amp;action=trash&amp;_wpnonce={$nonce}";

	echo '<div class="submitbox" id="submitpost">';

	if ( ! empty( $args['args']['description'] ) ) {
		echo '<div id="minor-publishing-actions">';
		echo $args['args']['description'];
		echo '</div>'; // #minor-publishing-actions.
	}

	echo '<div id="major-publishing-actions">';

	// Delete Action (WP Add Post screen automatically hides this).
	echo '<div id="delete-action">';
	echo '<a class="submitdelete deletion" href="' . $delete_url . '">Move to Trash</a>';
	echo '</div>'; // #delete-action.

	// Publish Actions.
	echo '<div id="publishing-action">';
	echo '<span class="spinner"></span>';
	echo '<input name="original_publish" type="hidden" id="original_publish" value="Update">';
	echo '<input name="publish" type="submit" class="button button-primary button-large" id="publish" value="Save">';
	echo '</div>'; // #publishing-action.

	echo '<div class="clear"></div>';
	echo '</div>'; // #major-publishing-actions.
	echo '</div>'; // #submitpost.
}
