<?php
/**
 * BrightFire Pagination Links.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Pagination_Links;

/**
 * BrightFire Print Pagination Links.
 */
function bf_print_pagination_links() {
	echo bf_get_pagination_links();
}

/**
 * Wrapper for pagination. Outputs paginated navigation.
 *
 * This uses $wp_query by default and can be used in templates.
 *
 * Custom queries require the 'paged' parameter to be set like so:
 * <code>
 *  $paged = ( get_query_var('paged') ) ? get_query_var( 'paged' ) : 1;
 *  $query = new WP_Query( array( 'paged' => $paged ) );
 * </code>
 *
 * Custom queries will need to pass the number of pages to this function via this filter:
 * <code>
 *  add_filter( 'bf_pagination_links', function() use ( $query->max_num_pages ) { return $query->max_num_pages; } );
 * </code>
 *
 * For further reading on query pagination parameters
 *
 * @link    https://codex.wordpress.org/Class_Reference/WP_Query#Pagination_Parameters
 *
 * @uses    paginate_links()
 *
 * @global  \WP_Query   $wp_query
 *
 * @return string   String of page links
 */
function bf_get_pagination_links() {

	global $wp_query;

	// We need totals.
	$total = isset( $wp_query->max_num_pages ) ? $wp_query->max_num_pages : 1;
	$total = apply_filters( 'bf_pagination_links_total', $total );

	$current = isset( $wp_query->paged ) ? $wp_query->paged : 1;
	$current = apply_filters( 'bf_pagination_links_current', $current );

	// No need for our previous and next buttons if it is 5 or less pages.
	if ( 5 >= $total ) {
		$prev_next  = false;
		$mid_size   = 1;
		$indicators = '';
	} else {
		$prev_next  = true;
		$mid_size   = 0;
		$indicators = 'indicators'; // Add this class so we can determine if we have the prev and next buttons.
	}

	$args = array(
		'type'      => 'list',
		'prev_text' => '<span class="fa fa-angle-left"></span>',
		'next_text' => '<span class="fa fa-angle-right"></span>',
		'mid_size'  => $mid_size,
		'prev_next' => $prev_next,
		'total'     => $total,
		'current'   => $current,
	);

	$output = '';

	$output .= '<nav class="brightfire-pagination ' . $indicators . '">';
	$output .= paginate_links( apply_filters( 'bf_pagination_links_args', $args ) );
	$output .= '</nav>';

	return $output;
}

global $bf_sass;
$bf_sass->register_path( 'bf-core', dirname( __FILE__ ) . '/_brightfire_pagination_links.scss' );
