<?php
/**
 * BrightFire Robots TXT.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Robots_Txt;

/**
 * Builds our basic robots.txt file
 *
 * @param string $output Output.
 * @param string $public Public.
 *
 * @return string
 */
function robots_text( $output, $public ) {

	$site_url = site_url();

	$output  = "User-agent: * \r\n";
	$output .= "Disallow: /wp-admin/ \r\n";
	$output .= "Allow: /wp-admin/admin-ajax.php \r\n";
	$output .= "Sitemap: {$site_url}/sitemap.xml \r\n";

	// Delay Crawler Bots
	// Only Effects Yahoo!, Bing, and Yandex
	// Google Crawlers can only be limited via SearchConsole.
	$output .= "Crawl-delay: 20 \r\n";

	return $output;
}
