<?php
/**
 * BrightFire Robots TXT.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Robots_Txt;

/** Require Files */
require_once dirname( __FILE__ ) . '/filters.php';

/**
 * Customize the robots.txt files
 */
add_filter( 'robots_txt', __NAMESPACE__ . '\robots_text', 10, 2 );
