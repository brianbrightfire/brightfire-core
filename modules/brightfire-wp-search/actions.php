<?php
/**
 * BrightFire WP Search.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\WP_Search;

/**
 * Disable WordPress Search
 * Strips "s" param from query_string
 *
 * DO WE REALLY WANT TO DO THIS?? 11/1/2017
 */
function remove_search_params() {

	global $wptouch_pro;

	// Bail if we're on WP Touch mobile since we allow search on mobile.
	if ( $wptouch_pro && $wptouch_pro->is_mobile_device ) {
		return;
	}

	// TODO: SECURITY - Need nonce verification.
	if ( ! is_admin() && isset( $_REQUEST['s'] ) ) { // phpcs:ignore

		// This helps break the query out of the full URI since REQUEST_URI contains path + query.
		$parsed_url = wp_parse_url( $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );

		// split query params into array and assign it to $query_params.
		parse_str( $parsed_url['query'], $query_params );

		// remove the search param.
		unset( $query_params['s'] );

		// piece back together query string.
		$cleaned_query_params = http_build_query( $query_params );

		// add ? if cleaned_query_params exists after removing search param.
		$new_query_params = empty( $cleaned_query_params ) ? '' : "?{$cleaned_query_params}";

		wp_safe_redirect( 'http://' . $parsed_url['path'] . $new_query_params, 301 );

		exit;
	}
}
