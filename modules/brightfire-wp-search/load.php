<?php
/**
 * BrightFire WP Search.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\WP_Search;

/** Require Files */
require_once dirname( __FILE__ ) . '/actions.php';


add_action( 'after_setup_theme', __NAMESPACE__ . '\remove_search_params' );
add_filter( 'get_search_form', '__return_null' );
