<?php
/**
 * BrightFire Core Page Title.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Current_Page_Content;

/**
 * BF Page Title.
 *
 * @return string
 */
function bf_page_title() {
	return get_the_title();
}
