<?php
/**
 * BrightFire Current Page Content Widget.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Current_Page_Content;

use BrightFireCore\Images\ImageFactory;

/**
 * Adds Current_Page_Content widget.
 */
class BrightFire_Current_Page_Content extends \WP_Widget {

	/**
	 * Page.
	 *
	 * @var $page
	 */
	public $page;

	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {

		add_filter( 'cpc_title', array( $this, 'get_title' ) );     // CPC title as action.
		add_filter( 'cpc_info', array( $this, 'get_info' ) );     // CPC title as action.
		add_filter( 'cpc_content', array( $this, 'get_content' ) ); // CPC content as action.

		parent::__construct(
			'bf_cpc', // Base ID.
			__( 'BrightFire Current Page Content', 'bf_cpc' ), // Name.
			array(
				'description'                 => __( 'Adds the title and/or content of the current page', 'bf_cpc' ),
				'customize_selective_refresh' => true,
			) // Args.
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {

		$show_title             = ! empty( $instance['show_title'] ) ? $instance['show_title'] : false;
		$show_featured_image    = ! empty( $instance['show_featured_image'] ) ? $instance['show_featured_image'] : false;
		$show_content           = ! empty( $instance['show_content'] ) ? $instance['show_content'] : false;
		$show_demo_content      = ! empty( $instance['show_demo_content'] ) ? $instance['show_demo_content'] : false;
		$show_demo_content_full = ! empty( $instance['show_demo_content_full'] ) ? $instance['show_demo_content_full'] : false;
		$show_info              = ! empty( $instance['show_info'] ) ? esc_attr( $instance['show_info'] ) : false;
		$show_cats_tags         = ! empty( $instance['show_cats_tags'] ) ? esc_attr( $instance['show_cats_tags'] ) : false;
		$show_archives          = ! empty( $instance['show_archives'] ) ? esc_attr( $instance['show_archives'] ) : false;

		// We'll use this across the board because trying to access the_title() or the_content() doesn't work on.
		// static blog page as it returns the title and content for the first post in the loop. This is really the.
		// "main" single post or page.
		$this->page = get_queried_object();

		// Set up array for our CSS class for the wrapping div.
		$bf_cpc_css_class = array();

		// Start building output.
		$widget_output  = '';
		$widget_content = '';

		if ( $show_title ) {
			$bf_cpc_css_class[] = 'title';
			$widget_content    .= apply_filters( 'cpc_title', $this->page );
		}

		if ( $show_info && is_single() ) {
			$bf_cpc_css_class[] = 'info';
			$widget_content    .= apply_filters( 'cpc_info', $this->get_info() );
		}

		if ( $show_featured_image && is_single() && 'no' !== $show_featured_image ) {

			$featured_image_id = get_post_thumbnail_id( $this->page->ID );

			$image_options = ImageFactory::create_image_options();

			if ( 'top' === $show_featured_image ) {
				$show_featured_image = 'none'; // use alignnone class for top.
			}

			$image_options->classes = array( 'align' . $show_featured_image );

			if ( in_array( $show_featured_image, array( 'left', 'right' ), true ) ) {
				$image_options->force_width_desktop = 200;
			}

			$image = ImageFactory::get_image_tag( apply_filters( 'cpc_featured_image_id', $featured_image_id ), apply_filters( 'cpc_featured_image_options', $image_options ) );

			$widget_content .= apply_filters( 'cpc_featured_image', $image );

		}

		if ( $show_content ) {

			// if employee single.
			// add 2 filters.

			$bf_cpc_css_class[] = 'content';

			$widget_content .= apply_filters( 'cpc_content', $this->page );
		}

		// Show archives if requested.
		if ( $show_archives && ( is_home() || is_archive() ) ) {

			$limit = get_option( 'posts_per_page' );
			$query = "limit=$limit paged=true ";
			$year  = get_query_var( 'year' );

			if ( is_object( $this->page ) ) {
				$tax = $this->page->taxonomy;

				if ( 'category' === $tax ) {
					$query .= " category={$this->page->cat_ID} ";
				} elseif ( 'post_tag' === $tax ) {
					$query .= " tag={$this->page->term_id} ";
				}
			} elseif ( $year ) {

				$query .= 'year=' . $year;
			}

			$widget_content .= do_shortcode( '[bf_recent_blog_posts ' . $query . ']' );
		}

		if ( $show_demo_content && current_user_can( 'manage_network' ) ) {
			$widget_content .= self::get_demo_markup();
		}

		if ( $show_demo_content_full && current_user_can( 'manage_network' ) ) {
			$widget_content .= self::get_demo_markup_full();
		}

		if ( $show_cats_tags && is_single() ) {
			$bf_cpc_css_class[] = 'meta';
			$widget_content    .= apply_filters( 'cpc_cats_tags', $this->get_cats_tags( $this->page ) );
		}

		if ( ! empty( $widget_content ) ) { // Only output if we have something to output.

			// Finish up work on CSS classes.
			$bf_cpc_css_class = 'bf-cpc-' . implode( '-', $bf_cpc_css_class );
			$page_css_classes = $this::get_post_classes( $this->page );
			$css_class_markup = "class='{$page_css_classes} {$bf_cpc_css_class}'";

			$widget_output .= $args['before_widget'];

			$widget_output .= "<div {$css_class_markup}>";
			$widget_output .= $widget_content;
			$widget_output .= '</div>';

			$widget_output .= $args['after_widget'];

			echo $widget_output;

		}

	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {

		$fields = array(
			'show_title'             => array(
				'type'    => 'selectize',
				'label'   => 'Show Title',
				'choices' => 'yesno',
			),
			'show_featured_image'    => array(
				'type'    => 'selectize',
				'label'   => 'Show Featured Image',
				'choices' => array(
					'no'    => 'No',
					'left'  => 'Float Left',
					'right' => 'Float Right',
					'top'   => 'Top',
				),
			),
			'show_content'           => array(
				'type'    => 'selectize',
				'label'   => 'Show Content',
				'choices' => 'yesno',
			),
			'show_info'              => array(
				'type'    => 'selectize',
				'label'   => 'Show Post Info',
				'choices' => 'yesno',
			),
			'show_cats_tags'         => array(
				'type'    => 'selectize',
				'label'   => 'Show Categories &amp; Tags',
				'choices' => 'yesno',
			),
			'show_archives'          => array(
				'type'    => 'selectize',
				'label'   => 'Show Archives',
				'choices' => 'yesno',
			),
			'show_demo_content'      => array(
				'type'    => 'selectize',
				'label'   => 'Show Demo Content',
				'choices' => 'yesno',
			),
			'show_demo_content_full' => array(
				'type'    => 'selectize',
				'label'   => 'Show Demo Content (Verbose)',
				'choices' => 'yesno',
			),
		);

		// Build our widget form.
		$args = array(
			'fields'   => $fields,
			'instance' => $instance,
			'widget'   => $this,
			'display'  => 'basic',
			'echo'     => true,
		);

		global $bf_admin_api;
		$bf_admin_api->fields->build( $args );

	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance                           = $old_instance;
		$instance['show_title']             = wp_strip_all_tags( $new_instance['show_title'] );
		$instance['show_content']           = wp_strip_all_tags( $new_instance['show_content'] );
		$instance['show_featured_image']    = wp_strip_all_tags( $new_instance['show_featured_image'] );
		$instance['show_demo_content']      = wp_strip_all_tags( $new_instance['show_demo_content'] );
		$instance['show_demo_content_full'] = wp_strip_all_tags( $new_instance['show_demo_content_full'] );
		$instance['show_info']              = wp_strip_all_tags( $new_instance['show_info'] );
		$instance['show_cats_tags']         = wp_strip_all_tags( $new_instance['show_cats_tags'] );
		$instance['show_archives']          = wp_strip_all_tags( $new_instance['show_archives'] );

		return $instance;
	}

	/**
	 * Get Title.
	 *
	 * @param object $post Post.
	 * @return string
	 */
	public function get_title( $post ) {

		if ( is_404() ) {
			$title = 'Error 404: Page not found';
		} elseif ( is_archive() ) {
			$title = get_the_archive_title();
		} else {
			$title = apply_filters( 'the_title', $post->post_title, $post->ID );
		}

		return '<h1 class="page-title">' . $title . '</h1>';
	}

	/**
	 * Get Content.
	 *
	 * @param object $post Post.
	 * @return mixed|string|void
	 */
	public function get_content( $post ) {

		if ( is_404() ) {

			return '<p>Oh no! It appears that the page you were looking for doesn\'t exist or might have been moved. We apologize for the inconvenience.</p><p>To find the page you\'re looking for, you can either use the navigation menu at the top of this page or go the <a href="' . get_home_url() . '">home page</a>.</p>';

		} elseif ( is_archive() ) {

			return '';

		} elseif ( is_object( $post ) ) {

			$content = apply_filters( 'the_content', $post->post_content );

			// Bail early and return if we don't have any content.
			if ( empty( $content ) ) {
				return $content;
			}

			// Leverage lazyloading for all images in the content - LIMIT TO DEV for time being (issues with some content repo slugs).
			$dom = new \DOMDocument();

			// DOMDocument will throw E_WARNING for invalid HTML, and we want to log that internally.
			// instead of allowing PHP Error Handling to deal with it.
			libxml_use_internal_errors( true ); // phpcs:ignore

			// LIBXML_HTML_NOIMPLIED - ensures that <html><body>...</body><html> isn't thrown on our HTML by $dom.
			// LIBXML_HTML_NODEFDTD  - ensures that DOCTYPE is not declared by $dom.
			// we wrap in <span></span> so that the DOMDocument crawl can recognize outermost content as belonging to a node which is inline.
			$dom->loadHTML( mb_convert_encoding( "<span>{$content}</span>", 'HTML-ENTITIES', 'UTF-8' ), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD ); // phpcs:ignore

			foreach ( $dom->getElementsByTagName( 'img' ) as $img ) {

				$class = $img->getAttribute( 'class' );

				// Make sure this image is not already setup to lazy load.
				if ( $class && strstr( $img->getAttribute( 'class' ), 'lazyload' ) ) {
					continue;
				}

				if ( $img->hasAttribute( 'class' ) ) {
					$img->setAttribute( 'class', $class . ' lazyload' );
				} else {
					$img->setAttribute( 'class', 'lazyload' );
				}

				$src = $img->getAttribute( 'src' );
				$img->setAttribute( 'src', '' );
				$img->setAttribute( 'data-src', $src );

				$srcset = $img->getAttribute( 'srcset' );
				$img->setAttribute( 'srcset', '' );
				$img->setAttribute( 'data-srcset', $srcset );

			}

			$content = $dom->saveHTML();

			return $content;

		} else {

			return '';
		}
	}

	/**
	 * Get Info.
	 *
	 * @return string
	 */
	public function get_info() {
		return '<span class="entry-meta">Posted by <span class="entry-author" itemprop="author" itemscope="" itemtype="http://schema.org/Person"><span class="entry-author-name" itemprop="name">' . get_the_author() . '</span></span> on <time class="entry-time" itemprop="datePublished" datetime="' . get_the_date( 'Y-m-d' ) . '">' . get_the_date() . '</time> </span>';
	}

	/**
	 * Get Cats Tags.
	 *
	 * @param object $post Post.
	 * @return string
	 */
	private function get_cats_tags( $post ) {

		$output = '<span class="entry-footer"><span class="entry-meta">';

		$categories = get_the_category();

		if ( $categories ) {

			foreach ( $categories as $cat ) {
				if ( 'Uncategorized' !== $cat->name ) {
					$cat_array[] = '<a href="' . esc_url( get_category_link( $cat->term_id ) ) . '" title="' . $cat->name . '" rel="category tag">' . $cat->name . '</a>';
				}
			}

			if ( ! empty( $cat_array ) ) {
				$output .= '<span class="entry-categories">Filed Under: ' . implode( ', ', $cat_array ) . '</span>';
			}
		}

		$tags = get_the_tags( $post );

		if ( $tags ) {

			// Divider if we have categories and tags.
			$output .= ! empty( $cat_array ) ? '<span class="divider"> | </span>' : '';

			foreach ( $tags as $tag ) {
				$tag_array[] = '<a href="' . esc_url( get_tag_link( $tag->term_id ) ) . '" title="' . $tag->name . '" rel="category tag">' . $tag->name . '</a>';
			}

			if ( ! empty( $tag_array ) ) {
				$output .= '<span class="entry-categories">Tagged With: ' . implode( ', ', $tag_array ) . '</span>';
			}
		}

		$output .= '</span></span>';

		return $output;

	}

	/**
	 * Get Post Classes.
	 *
	 * @param object $post Post.
	 * @return string
	 */
	private static function get_post_classes( $post ) {
		$page_css_classes = array();

		if ( $post ) {
			$page_css_classes = get_post_class( '', $post->ID );
		}

		return implode( ' ', $page_css_classes );
	}

	/**
	 * Get Demo Markup.
	 *
	 * @return string
	 */
	private static function get_demo_markup() {
		return '<h1>Header one</h1><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean leo nibh, porta eget pharetra eget, facilisis id lorem. Aliquam blandit sapien sed libero ultrices, at auctor tortor dapibus. Nullam pretium dui elit, nec lacinia dui dapibus vitae. Nunc dapibus lacus et neque posuere dignissim. Aliquam eu elementum velit. Nunc egestas at lorem vel fermentum. Suspendisse suscipit orci ut sodales ultricies.</p><p>Integer volutpat vulputate enim id sagittis. Aliquam erat volutpat. Fusce finibus, ante nec efficitur ultrices, ante lectus tempor tellus, ac dignissim ex urna ut leo. Quisque mauris massa, hendrerit non erat dignissim, blandit rutrum ligula. Integer tincidunt sed nulla a fermentum. Quisque nec ipsum non quam varius vulputate tempor quis purus. Nam sit amet finibus metus. Vestibulum posuere cursus maximus. Cras lobortis sit amet neque at volutpat. Curabitur eget lectus eu augue luctus blandit.</p><h2>Header two</h2><p>Curabitur feugiat enim in vulputate scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas congue bibendum mi. Sed leo lacus, auctor in facilisis eu, sagittis a elit. Quisque mi magna, sollicitudin eu dictum vitae, dapibus non urna. Donec orci ligula, consectetur vitae velit vel, interdum vulputate massa. Phasellus tortor dolor, pulvinar in libero non, molestie finibus tortor.</p><h2>Header two</h2><p>Sed fringilla venenatis ligula. Fusce et lectus in erat aliquet bibendum sed ac ipsum. Ut mattis mi non mi vestibulum posuere. In eu placerat justo. Quisque ultricies sapien eget vehicula rutrum. Aliquam pellentesque orci sed eros aliquam, in sollicitudin sem dignissim. In hac habitasse platea dictumst. Vivamus tincidunt, turpis id vestibulum eleifend, nulla felis pharetra elit, at cursus mi purus sed nibh. Vestibulum rhoncus laoreet dui, vel dictum turpis dictum sed. Praesent sagittis feugiat justo a congue. Cras interdum tincidunt eros congue rhoncus. Aliquam euismod mi libero, vitae blandit mauris tempor ac. In eget ante pulvinar, facilisis nunc non, tincidunt neque. Donec eros sem, ultrices id velit quis, porta interdum erat.</p><h3>Header three</h3><p>Morbi pellentesque sem ante, nec sodales augue dapibus pulvinar. Mauris ut magna sed ipsum ultrices feugiat. Pellentesque blandit magna tristique, interdum nibh eget, aliquam nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent neque est, congue in ornare et, pretium quis justo. Cras elementum vel metus nec volutpat. Donec fermentum nisi vitae nulla blandit, vel viverra nisi semper.</p><h2>Header two</h2><p>Morbi pellentesque sem ante, nec sodales augue dapibus pulvinar. Mauris ut magna sed ipsum ultrices feugiat. Pellentesque blandit magna tristique, interdum nibh eget, aliquam nulla. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent neque est, congue in ornare et, pretium quis justo. Cras elementum vel metus nec volutpat. Donec fermentum nisi vitae nulla blandit, vel viverra nisi semper.</p>';
	}

	/**
	 * Get Demo Markup Full.
	 *
	 * @return string
	 */
	private static function get_demo_markup_full() {
		return "<p><strong>Headings</strong><h1>Header one</h1><h2>Header two</h2><h3>Header three</h3><h4>Header four</h4><h5>Header five</h5><h6>Header six</h6><h2>Blockquotes</h2><p>Single line blockquote:<blockquote><p>Stay hungry. Stay foolish.</blockquote><p>Multi line blockquote with a cite reference:<blockquote><p>People think focus means saying yes to the thing you’ve got to focus on. But that’s not what it means at all. It means saying no to the hundred other good ideas that there are. You have to pick carefully. I’m actually as proud of the things we haven’t done as the things I have done. Innovation is saying no to 1,000 things. <cite>Steve Jobs – Apple Worldwide Developers’ Conference, 1997</cite></blockquote><h2>Tables</h2><table><tr><th>Employee<th class=views>Salary<th><tr class=odd><td><a href=http://example.com/ >Jane</a><td>$1<td>Because that’s all Steve Jobs needed for a salary.<tr class=even><td><a href=http://example.com>John</a><td>$100K<td>For all the blogging he does.<tr class=odd><td><a href=http://example.com/ >Jane</a><td>$100M<td>Pictures are worth a thousand words, right? So Tom x 1,000.<tr class=even><td><a href=http://example.com/ >Jane</a><td>$100B<td>With hair like that?! Enough said…</table><h2>Definition Lists</h2><dl><dt>Definition List Title<dd>Definition list division.<dt>Startup<dd>A startup company or startup is a company or temporary organization designed to search for a repeatable and scalable business model.<dt>#dowork<dd>Coined by Rob Dyrdek and his personal body guard Christopher “Big Black” Boykins, “Do Work” works as a self motivator, to motivating your friends.<dt>Do It Live<dd>I’ll let Bill O’Reilly will <a href=\"https://www.youtube.com/watch?v=O_HyZ5aW76c\"title=\"We'll Do It Live\">explain</a> this one.</dl><h2>Unordered Lists (Nested)</h2><ul><li>List item one<ul><li>List item one<ul><li>List item one<li>List item two<li>List item three<li>List item four</ul><li>List item two<li>List item three<li>List item four</ul><li>List item two<li>List item three<li>List item four</ul><h2>Ordered List (Nested)</h2><ol><li>List item one<ol><li>List item one<ol><li>List item one<li>List item two<li>List item three<li>List item four</ol><li>List item two<li>List item three<li>List item four</ol><li>List item two<li>List item three<li>List item four</ol><h2>HTML Tags</h2><p>These supported tags come from the WordPress.com code <a href=http://en.support.wordpress.com/code/ title=Code>FAQ</a>.<p><strong>Address Tag</strong><address>1 Infinite Loop<br>Cupertino, CA 95014<br>United States</address><p><strong>Anchor Tag (aka. Link)</strong><p>This is an example of a <a href=http://apple.com title=Apple>link</a>.<p><strong>Abbreviation Tag</strong><p>The abbreviation <abbr title=Seriously>srsly</abbr> stands for “seriously”.<p><strong>Acronym Tag</strong><p>The acronym <acronym title=\"For The Win\">ftw</acronym> stands for “for the win”.<p><strong>Big Tag</strong><p>These tests are a <big>big</big> deal, but this tag is no longer supported in HTML5.<p><strong>Cite Tag</strong><p>“Code is poetry.” —<cite>Automattic</cite><p><strong>Code Tag</strong><p>You will learn later on in these tests that <code>word-wrap: break-word;</code> will be your best friend.<p><strong>Delete Tag</strong><p>This tag will let you <del>strikeout text</del> , but this tag is no longer supported in HTML5 (use the <code>&lt;strike></code> instead).<p><strong>Emphasize Tag</strong><p>The emphasize tag should <em>italicize</em> text.<p><strong>Insert Tag</strong><p>This tag should denote <ins>inserted</ins> text.<p><strong>Keyboard Tag</strong><p>This scarcely known tag emulates <kbd>keyboard text</kbd>, which is usually styled like the <code>&lt;code></code> tag.<p><strong>Preformatted Tag</strong><p>This tag styles large blocks of code.<pre>.post-title {margin: 0 0 5px;font-weight: bold;	font-size: 38px;line-height: 1.2;}</pre><p><strong>Quote Tag</strong><p><q>Developers, developers, developers…</q> –Steve Ballmer<p><strong>Strong Tag</strong><p>This tag shows <strong>bold<strong> text.</strong></strong><p><strong>Subscript Tag</strong><p>Getting our science styling on with H<sub>2</sub>O, which should push the “2” down.<p><strong>Superscript Tag</strong><p>Still sticking with science and Isaac Newton’s E = MC<sup>2</sup>, which should lift the 2 up.<p><strong>Teletype Tag</strong><p>This rarely used tag emulates <tt>teletype text</tt>, which is usually styled like the <code>&lt;code></code> tag.<p><strong>Variable Tag</strong><p>This allows you to denote <var>variables</var>.";
	}

}

