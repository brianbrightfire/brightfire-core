<?php
/**
 * BrightFire Current Page Content.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Current_Page_Content;

/** Include Files */
require_once dirname( __FILE__ ) . '/class-brightfire-current-page-content.php';
require_once dirname( __FILE__ ) . '/shortcode-bf-page-title.php';


/**
 * Register BrightFire_Footer_Credit_Widget
 */
function register_bf_current_page_content() {
	register_widget( __NAMESPACE__ . '\BrightFire_Current_Page_Content' );
}
add_action( 'widgets_init', __NAMESPACE__ . '\register_bf_current_page_content' );

/** Register Shortcode(s) */
add_shortcode( 'bf_page_title', __NAMESPACE__ . '\bf_page_title' );

global $bf_sass;
$bf_sass->register_path( 'bf-core', dirname( __FILE__ ) . '/_brightfire_current_content.scss' );
