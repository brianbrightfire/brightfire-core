<?php
/**
 * BrightFire Pages.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Pages;

/**
 * Remove default WP metaboxes for bf_editors from pages and posts
 */
function remove_metaboxes() {

	// Super Admins can still have access.
	if ( current_user_can( 'manage_network' ) ) {
		return false;
	}

	// Remove Template / Parent MetaBox from Posts and Pages Edit Screens.
	remove_meta_box( 'pageparentdiv', array( 'post', 'page' ), 'side' );

	// Remove Featured Image from Pages Edit Screen.
	remove_meta_box( 'postimagediv', 'page', 'side' );

	// Remove Custom Fields MetaBox from Posts and Pages Edit Screens.
	remove_meta_box( 'postcustom', array( 'post', 'page' ), 'normal' );

	// Remove Slug MetaBox from Posts and Pages Edit Screens.
	remove_meta_box( 'slugdiv', array( 'post', 'page' ), 'normal' );

	// Remove Author MetaBox from Posts and Pages Edit Screens.
	remove_meta_box( 'authordiv', array( 'post', 'page' ), 'normal' );

	// Remove Post Excerpt MetaBox from Posts and Pages Edit Screens.
	remove_meta_box( 'postexcerpt', 'post', 'normal' );

}
