<?php
/**
 * BrightFire Pages
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Pages;

require_once dirname( __FILE__ ) . '/actions.php';

/** Remove Featured Image for bf_editors */
add_action( 'do_meta_boxes', __NAMESPACE__ . '\remove_metaboxes' );
