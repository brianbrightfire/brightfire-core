/**
 * Customizer Previewer Object
 * This file only exists as a placeholder currently for completion
 * Add calls or methods initialized in the init method below to add
 * hooks to the Previewer object
 */
( function ( exports, $ ) {
    "use strict";

    var api = wp.customize, wpPreviewer;

    // Custom Customizer Previewer class (attached to the Customize API)
    api.brightFirePreviewer = {

        // Execute
        init: function () {

            this.previewer.bind( 'ready', function() {
                $(document).trigger( 'customizer_controls_ready' );
            });


        }
    };

    /**
     * Extend the wp.customize object with our own bind events and functions
     */
    wpPreviewer = api.Previewer;
    api.Previewer = wpPreviewer.extend( {
        initialize: function( params, options ) {
            // Store a reference to the Previewer
            api.brightFirePreviewer.previewer = this;

            // Call the old Previewer's initialize function
            wpPreviewer.prototype.initialize.call( this, params, options );
        }
    } );

    /**
     * Initialize our Previewer JS
     */
    $( function() {
        // Initialize our Previewer
        api.brightFirePreviewer.init();
    } );

} )( wp, jQuery );