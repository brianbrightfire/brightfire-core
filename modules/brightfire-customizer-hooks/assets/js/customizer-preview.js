/**
 * Customizer Preview Object
 */
( function ( wp, $ ) {
    "use strict";

    // Bail if the customizer isn't initialized
    if ( ! wp || ! wp.customize ) {
        return;
    }
    var api = wp.customize, wpPreview;

    /**
     * Custom Customizer Preview class (attached to the Customize API)
     * @type {{init: init, bindPartialsHooks: bindPartialsHooks}}
     */
    api.brightFirePreview = {

        // Execute
        init: function () {
            var self = this;

            // When the previewer is active, the "active" event has been triggered (on load)
            // This also triggers when a widget or navigation click loads or refreshes a page
            this.preview.bind( 'active', function() {
                self.bindPartialsHooks();
            } );
        },

        // Trigger events on document to hook
        bindPartialsHooks: function() {

            // Selective Refresh - add
            wp.customize.selectiveRefresh.partial.bind( 'add', function( addedPartial ) {
                $(document).trigger( 'bf_selective_refresh_add', addedPartial );
            } );

            // Selective Refresh - content rendered
            wp.customize.selectiveRefresh.bind( 'partial-content-rendered', function( e, widgetPartial ) {
                $(document).trigger( 'bf_selective_refresh_rendered', e, widgetPartial );
            } );

        }
    };

    /**
     * Extend the wp.customize object with our own bind events and functions
     */
    wpPreview = api.Preview;
    api.Preview = wpPreview.extend( {
        initialize: function( params, options ) {
            // Store a reference to the Preview
            api.brightFirePreview.preview = this;

            // Call the old Preview's initialize function
            wpPreview.prototype.initialize.call( this, params, options );
        }
    } );

    /**
     * Initialize our customize preview
     */
    $( function () {
        // Initialize our Preview
        api.brightFirePreview.init();
    } );

} )( window.wp, jQuery );