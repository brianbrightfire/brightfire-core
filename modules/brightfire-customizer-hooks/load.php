<?php
/**
 * BrightFire Customizer Hooks.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Customizer_Hooks;

require_once dirname( __FILE__ ) . '/actions.php';

/**
 * Enqueue WP Customizer Preview JS Object Extension
 * This is the Preview JS Object in Customizer
 */
add_action( 'customize_preview_init', __NAMESPACE__ . '\customize_preview_init' );


/**
 * Enqueue WP Customizer Previewer JS Object Extension
 * This is the Controls JS Object in Customizer
 * NOT CURRENTLY USED
 */
// phpcs:ignore
// add_action( 'customize_controls_enqueue_scripts', __NAMESPACE__ . '\customize_controls_enqueue_scripts' );
