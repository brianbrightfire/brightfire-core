<?php
/**
 * BrightFire Customizer Hooks.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Customizer_Hooks;

/**
 * Add our BFCore Customizer Controls JS Object Extensions to provide
 * useful hook sin the customizer preview/previewer environment
 */
function customize_controls_enqueue_scripts() {

	wp_register_script( 'brightfire_customizer_controls', BF_CORE_ASSETS . 'js/brightfire-customizer-controls.min.js', array( 'jquery', 'customize-controls', 'wp-color-picker' ), BF_CORE_VERSION, false );
	wp_enqueue_script( 'brightfire_customizer_controls' );

}

/**
 * Add our BFCore Customizer Preview Object Extension to provide
 * useful hook sin the customizer preview/previewer environment
 */
function customize_preview_init() {

	wp_register_script( 'brightfire_customizer_preview', BF_CORE_ASSETS . 'js/brightfire-customizer-preview.min.js', array( 'customize-preview-widgets' ), BF_CORE_VERSION, false );
	wp_enqueue_script( 'brightfire_customizer_preview' );

}
