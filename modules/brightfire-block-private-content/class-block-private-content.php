<?php
/**
 * Class Block_Private_Content
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Block_Private_Content;

/**
 * Class Block_Private_Content - Provides a global object to block content using a 410 response code
 *
 * @package BrightFireCore\Block_Private_Content
 */
class Block_Private_Content {

	/**
	 * Array holding all settings to be blocked for a given site id.
	 *
	 * @var $blog_settings
	 *      EXAMPLE: array(
	 *        {blog_id} => array( 'pages', 'posts', 'feeds', 'sitemap', etc )
	 *      )
	 */
	private $blog_settings = array();

	/**
	 * Our blog ID for the current blog @see __construct()
	 *
	 * @var int
	 */
	private $blog_id = 0;

	/**
	 * If we want to use a wp_die(), what message do we want to display
	 *
	 * @var string
	 */
	public $die_message = '';

	/**
	 * Private_Site_Settings constructor.
	 */
	public function __construct() {

		// Ensure defaults for object properties.
		$this->blog_settings = array();
		$this->blog_id       = get_current_blog_id();
		$this->die_message   = 'We apologize that you are not able to view our agency\'s website. If you need immediate assistance, please call us directly.<br><br>If you are the owner of this site, we are aware of the issue and are working to resolve it.';

		// On template redirect, let's check our site settings against permissions.
		add_action( 'template_redirect', array( $this, 'block_content' ) );
	}

	/**
	 * Block Private Content based on settings - hooked on 'template_redirect'
	 *
	 * @return bool
	 */
	public function block_content() {

		// If we have no settings, do nothing.
		if ( ! $this->blog_settings( $this->blog_id ) ) {
			return false;
		}

		// Address Pages and Archives.
		if ( ( is_front_page() || is_home() || is_page() || is_archive() || is_404() || is_attachment() ) && ( $this->setting_exists_for_blog( $this->blog_id, 'pages' ) ) ) {
			$this->client_die();
		}

		// Address Feeds.
		if ( is_feed() && $this->setting_exists_for_blog( $this->blog_id, 'feeds' ) ) {
			$this->client_die();
		}

		// Address Posts.
		if ( is_single() && $this->setting_exists_for_blog( $this->blog_id, 'posts' ) ) {
			$this->client_die();
		}

		// Provide a WP Action hook to allow for additional functionality for custom permission keys.
		do_action( 'redirect_private_blog_settings', $this->blog_id );

		return false;

	}

	/**
	 * Adds a $blog_id => $setting index to the $blog_settings array
	 *
	 * @param int          $blog_id Blog ID.
	 * @param string|array $settings Settings.
	 *
	 * @return bool
	 */
	public function block( $blog_id, $settings ) {

		// If this blog has no settings yet, make a index.
		if ( ! $this->blog_settings( $blog_id ) ) {
			$this->blog_settings[ $blog_id ] = array();
		}

		// Want to allow for single or multiple settings to be passed to this.
		foreach ( (array) $settings as $setting ) {

			// Add the setting if it does not exist.
			if ( ! $this->setting_exists_for_blog( $blog_id, $setting ) ) {
				array_push( $this->blog_settings[ $blog_id ], $setting );
			}
		}

		return true;

	}

	/**
	 * Removes a $blog_id => $setting index to the $blog_settings array
	 *
	 * @param int          $blog_id Blog ID.
	 * @param string|array $settings Settings.
	 *
	 * @return bool
	 */
	public function allow( $blog_id, $settings ) {

		if ( ! $this->blog_settings( $blog_id ) ) {
			return true;
		}

		// Want to allow for single or multiple settings to be passed to this.
		foreach ( (array) $settings as $setting ) {

			// Remove the setting key if that setting exists.
			$key = array_search( $setting, $this->blog_settings[ $blog_id ], true );

			if ( false !== $key ) {
				unset( $this->blog_settings[ $key ] );
			}
		}

		return true;

	}

	/**
	 * If $this->blog_settings holds information for a given blog id
	 *
	 * @param int $blog_id Blog ID.
	 *
	 * @return bool|array
	 */
	private function blog_settings( $blog_id ) {

		// Do we have a blog_id index in our settings array?.
		if ( array_key_exists( $blog_id, $this->blog_settings ) ) {
			return $this->blog_settings[ $blog_id ];
		} else {
			return false;
		}

	}

	/**
	 * If $this->blog_settings holds a specific setting for a specific blog id
	 *
	 * @param int    $blog_id Blog ID.
	 * @param string $setting Setting.
	 *
	 * @return bool
	 */
	public function setting_exists_for_blog( $blog_id, $setting ) {

		// Never block content for super admins.
		if ( \BrightFireCore\is_bf_super_admin() ) {
			return false;
		}

		$blog_settings = $this->blog_settings( $blog_id );

		if ( $blog_settings && in_array( $setting, $blog_settings, true ) ) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Kill our request with a 410
	 */
	private function client_die() {
		wp_die( __( $this->die_message ), '410: Unknown Content', array( 'response' => 410 ) ); // phpcs:ignore
	}

}
