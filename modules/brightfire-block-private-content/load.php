<?php
/**
 * BrightFire Core Module Block Private Content.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Block_Private_Content;

/** Require Files */
require_once dirname( __FILE__ ) . '/class-block-private-content.php';

// Instantiate our private site settings object.
global $bf_block_private_content;
$bf_block_private_content = new Block_Private_Content();
