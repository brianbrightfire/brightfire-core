<?php
/**
 * BrightFire Child Page Links.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Child_Page_Links;

/**
 * BrightFire Footer Credit widget class
 */
class BrightFire_Child_Page_Links extends \WP_Widget {

	/**
	 * Defaults.
	 *
	 * @var array $defaults
	 */
	private $defaults;

	/**
	 * BrightFire_Child_Page_Links constructor.
	 */
	public function __construct() {

		$this->defaults = array(
			'page_id' => '',
			'columns' => '2',
			'exclude' => '',
			'sort'    => 'post_title',
		);

		$widget_ops = array(
			'classname'                   => 'brightfire-child-page-links',
			'description'                 => __( 'Output Child Page Links for this page or any other specified page' ),
			'customize_selective_refresh' => true,
		);

		parent::__construct( 'brightfire_child_page_links', __( 'BrightFire Child Page Links' ), $widget_ops );
	}

	/**
	 * Front end display.
	 *
	 * @param array $args Arguments.
	 * @param array $instance Instance.
	 */
	public function widget( $args, $instance ) {
		$instance = wp_parse_args( (array) $instance, $this->defaults );
		$title    = trim( $instance['title'] );

		echo $args['before_widget'];
		echo ( ! empty( $title ) ? $args['before_title'] . apply_filters( 'widget_title', $title, $instance, $this->id_base ) . $args['after_title'] : '' );

		echo bf_child_page_links( $instance );

		echo $args['after_widget'];

	}

	/**
	 * Admin display.
	 *
	 * @param array $instance Instance.
	 * @return string|void
	 */
	public function form( $instance ) {
		// Defaults.
		$instance = wp_parse_args( (array) $instance, $this->defaults );

		$fields = array(
			'title'   => array(
				'type'          => 'text',
				'label'         => 'Title',
				'default_value' => '',
				'sanitize'      => '',
			),
			'page_id' => array(
				'type'          => 'text',
				'label'         => 'Parent Page',
				'description'   => 'Leave blank to use current page',
				'default_value' => '',
				'sanitize'      => '',
			),
			'columns' => array(
				'type'          => 'number',
				'label'         => 'Columns',
				'description'   => '1-5 columns',
				'default_value' => '2',
				'sanitize'      => '',
			),
			'exclude' => array(
				'type'          => 'text',
				'label'         => 'Exclude',
				'description'   => 'Comma delimited list of page IDs to exclude',
				'default_value' => '',
				'sanitize'      => '',
			),
		);

		$args = array(
			'fields'          => $fields,
			'instance'        => $instance,
			'widget_instance' => $this,
			'display'         => 'basic',
			'echo'            => true,
		);

		global $bf_admin_api;
		$bf_admin_api->fields->build( $args );

	}
}
