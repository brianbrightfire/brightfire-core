<?php
/**
 * BrightFire Child Page Links.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Child_Page_Links;

/** Include Files */
require_once dirname( __FILE__ ) . '/class-brightfire-child-page-links.php';
require_once dirname( __FILE__ ) . '/shortcode-bf-child-page-links.php';


/**
 * Register BrightFire_Footer_Credit_Widget
 */
function register_bf_current_page_content() {
	register_widget( __NAMESPACE__ . '\BrightFire_Child_Page_Links' );
}
add_action( 'widgets_init', __NAMESPACE__ . '\register_bf_current_page_content' );

/** Register Shortcode(s) */
add_shortcode( 'bf_child_page_links', __NAMESPACE__ . '\bf_child_page_links' );

global $bf_sass;
$bf_sass->register_path( 'bf-core', dirname( __FILE__ ) . '/_brightfire_child_page_links.scss' );
