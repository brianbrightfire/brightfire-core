<?php
/**
 * BrightFire Child Page Links Shortcode.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\BF_Child_Page_Links;

/**
 * Output balanced table of childpages
 *
 * @param array $atts Attributes.
 *      page_id: override for a specific page id.
 *      columns: override for how many cols to output.
 *
 * @return string
 */
function bf_child_page_links( $atts ) {
	global $wp_query;

	$atts = shortcode_atts(
		array(
			'page_id' => '',
			'columns' => 2,
			'exclude' => '',
			'sort'    => 'post_title',
		),
		$atts,
		'child_page_links'
	);

	// Get our Exclude List Formatted.
	$exclude_arr = explode( ',', $atts['exclude'] );

	if ( ! empty( $atts['page_id'] ) ) {
		$page_id = $atts['page_id'];
	} elseif ( is_object( $wp_query->post ) ) {
		$page_id = $wp_query->post->ID;
	} else {
		$page_id = '';
	}

	// Arguments for getting the pages.
	$query_args = array(
		'post_type'      => 'page',
		'post_status'    => 'publish',
		'post_parent'    => $page_id,
		'orderby'        => $atts['sort'],
		'posts_per_page' => '-1',
		'order'          => 'ASC',
		'post__not_in'   => $exclude_arr,
	);

	/**
	 * Begin output
	 */
	$output  = '<div class="child-page-links">';
	$output .= '<ul class="columns-' . $atts['columns'] . '">';

	$child_pages = get_posts( $query_args );

	// Calculate items per column.
	$items_per_column = ceil( ( count( $child_pages ) / $atts['columns'] ) );
	$ccount           = 1;

	foreach ( $child_pages as $child ) {
		/**
		 * Add page to our output
		 */
		if ( $items_per_column < $ccount ) {
			$output .= '</ul><ul class="columns-' . $atts['columns'] . '">';
			$ccount  = 1;
		}

		$output .= '<li><a href="' . get_permalink( $child->ID ) . '">' . $child->post_title . '</a></li>';

		$ccount++;
	}

	// Close main container and list.
	$output .= '</ul>';
	$output .= '</div>';

	return $output;
}
