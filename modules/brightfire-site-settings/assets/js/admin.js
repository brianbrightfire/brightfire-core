var $ = jQuery.noConflict();

function serializeSettingsForms() {

    var option = {};

    $('form.bf-settings-form').each( function() {

        meta_section = {};

        $(this).serializeArray().map( function(x){

            var key_name = x.name;
            var key_value = x.value;

            // Remove Array brackets
            key_name = key_name.replace( '[]', '' );

            if( meta_section[key_name] !== undefined ) {
                meta_section[key_name] = meta_section[key_name] + ',' + key_value;
            } else {
                meta_section[key_name] = key_value;
            }
        });


        option[$(this).attr('id')] = meta_section;

    });

    return option;
}

$(document).on( 'admin-api-ready', function() {
    bf_settings_adminListeners();
    bf_settings_packageListener();
});

// Settings Update Listener
function bf_settings_adminListeners() {

    $(document).on('change keyup keydown', 'form.bf-settings-form input, select, textarea', function() {
        $('#ajax-response').html('<div class="notice notice-warning" style="margin: -6px -12px"><p>Changes not saved</p></div>');
    });

    $(document).on('click', 'input#bf-settings-publish', function() {

        var spinner = $(this).siblings('.spinner');

        spinner.addClass('is-active');

        var option = serializeSettingsForms();

        var postvars = {
            'action': 'update_bf_settings',
            'data' : option
        };

        $.post( ajaxurl, postvars, function( response ) {
            $('#ajax-response').html(response.message);
            spinner.removeClass('is-active');
        } );

    });

}

function bf_settings_packageListener() {

    assign_package_values( $('select#bf_setting_website_package' ), false );

    $( document ).on( 'change propertychange', 'select#bf_setting_website_package', function() {

        // Make sure we enable everything
        $('input[type=checkbox]').prop('disabled',false).prop('checked',false);

        assign_package_values( this, true );

    });

}

function assign_package_values( package_input, precheck ) {

    var marketing_services = $.parseJSON( $('option:selected', package_input).attr('data-marketing-services') );
    var optional_services = $.parseJSON( $('option:selected', package_input).attr('data-optional-services') );

    var marketing_service_disabled = marketing_services['disabled'].split( ',' );
    var optional_services_disabled = optional_services['disabled'].split( ',' );

    $.each( marketing_service_disabled, function( index, value ) {

        if( '' !== value ) {
            $('input[type=checkbox][value=' + value + ']').prop('disabled', true).prop('checked',false);
        }

    });

    $.each( optional_services_disabled, function( index, value ) {

        if( '' !== value ) {
            $('input[type=checkbox][value=' + value + ']').prop('disabled', true).prop('checked',false);
        }

    });

    // We don't want to check things on window load, only disable
    if( precheck ) {

        var marketing_services_precheck = marketing_services['precheck'].split(',');
        var optional_services_precheck = optional_services['precheck'].split(',');

        $.each(marketing_services_precheck, function (index, value) {

            if ('' !== value) {
                $('input[type=checkbox][value=' + value + ']').prop('checked', true);
            }

        });

        $.each(optional_services_precheck, function (index, value) {


            if ('' !== value) {
                $('input[type=checkbox][value=' + value + ']').prop('checked', true);
            }

        });

        $('input.checkbox_multi').trigger('propertychange');

    }

}