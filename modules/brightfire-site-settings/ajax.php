<?php
/**
 * BrightFire Site Settings.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Site_Settings;

/**
 * Update BF Settings.
 */
function update_bf_settings() {

	// TODO: SECURITY - Need nonce verification.
	$bf_settings_sections = $_POST['data']; // phpcs:ignore

	// Each Meta Form on the page, which contains specific settings to save.
	foreach ( $bf_settings_sections as $section ) {

		// Save each specific setting.
		foreach ( $section as $option => $value ) {

			update_option( $option, $value );

		}
	}

	// Double check for empty sets on Marketing Services and Optional Features.
	// TODO: SECURITY - Need nonce verification.
	if ( ! array_key_exists( 'bf_setting_marketing_services', $_POST['data']['bf_settings_general'] ) ) { // phpcs:ignore
		update_option( 'bf_setting_marketing_services', '' );
	}

	// TODO: SECURITY - Need nonce verification.
	if ( ! array_key_exists( 'bf_setting_optional_features', $_POST['data']['bf_settings_general'] ) ) { // phpcs:ignore
		update_option( 'bf_setting_optional_features', '' );
	}

	// TODO: SECURITY - Need nonce verification.
	do_action( 'bf_save_settings_ajax', $_POST['data'] ); // phpcs:ignore

	$message  = "<div class='notice notice-success' style='margin: -6px -12px'><p>Changes have been saved</p></div>";
	$response = array(
		'message'    => $message,
		// TODO: SECURITY - Need nonce verification.
		'values_new' => $_POST['data'], // phpcs:ignore
	);
	wp_send_json( $response );

}
add_action( 'wp_ajax_update_bf_settings', __NAMESPACE__ . '\update_bf_settings' );
