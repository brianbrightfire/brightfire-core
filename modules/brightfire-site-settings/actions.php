<?php
/**
 * BrightFire Site Settings.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Site_Settings;

/**
 * Enqueue Admin Scripts.
 */
function enqueue_admin_scripts() {
	wp_register_script( 'brightfire_site_settings', BF_CORE_ASSETS . 'js/brightfire-site-settings.min.js', array( 'jquery' ), BF_CORE_VERSION, false );
}

/**
 * Build our menu and page callback
 */
function add_settings_page() {
	add_options_page(
		'BrightFire Site Settings',
		'BrightFire Site Settings',
		SITE_SETTINGS_CAPABILITY,
		'bf_site_settings',
		__NAMESPACE__ . '\site_settings_screen'
	);

	add_meta_box(
		'bf_site_general_settings',
		'General Settings',
		__NAMESPACE__ . '\general_settings_meta_box',
		'bf_site_settings',
		'normal',
		'high'
	);

	add_meta_box(
		'update',
		'Update Settings',
		__NAMESPACE__ . '\publish_metabox_callback',
		'bf_site_settings',
		'side',
		'high'
	);
}

/**
 * Callback: General Settings Meta Box Form
 */
function general_settings_meta_box() {

	echo wp_nonce_field( 'bf_settings_general', 'bf_settings_general_nonce', true, false );
	echo "<form id='bf_settings_general' class='bf-settings-form'>";

	$args = array(
		'fields'   => define_general_settings(),
		'instance' => get_instance_settings(),
		'display'  => 'basic',
		'echo'     => true,
	);

	global $bf_admin_api;
	$bf_admin_api->fields->build( $args );

	echo '</form>';

}

/**
 * Callback: Settings Page Markup
 */
function site_settings_screen() {

	// Enqueue WordPress' script for handling the meta boxes.
	wp_nonce_field( 'meta-box-order', 'meta-box-order-nonce', false );
	wp_nonce_field( 'closedpostboxes', 'closedpostboxesnonce', false );
	wp_enqueue_script( 'postbox' );
	wp_enqueue_script( 'brightfire_site_settings' );

	do_action( 'add_meta_boxes_bf_site_settings', 'bf_site_settings' );

	// Add screen option: user can choose between 1 or 2 columns (default 2).
	add_screen_option(
		'layout_columns',
		array(
			'max'     => 2,
			'default' => 2,
		)
	);

	// Markup Heading / Wrap.
	echo '<div class="wrap">';
	echo '<h1 class="wp-heading-inline">Site Settings</h1>';
	echo '<hr class="wp-header-end">';

	echo '<div id="poststuff">';
	echo '<div id="post-body" class="metabox-holder columns-2">';

	echo '<div id="postbox-container-1" class="postbox-container">';

	// WP Meta Box Support.
	do_meta_boxes( 'bf_site_settings', 'side', null );

	echo '</div>'; // id="postbox-container-1.

	echo '<div id="postbox-container-2" class="postbox-container">';

	// WP Meta Box Support.
	do_meta_boxes( 'bf_site_settings', 'normal', null );
	do_meta_boxes( 'bf_site_settings', 'advanced', null );

	echo '</div>'; // id="postbox-container-2.

	// Close Wrapper.
	echo '</div>'; // id post-body.
	echo '</div>'; // id poststuff.
	echo '</div>'; // class wrap.

	echo "<script>jQuery(document).ready(function(){ if( typeof postboxes !== 'undefined' ) { postboxes.add_postbox_toggles(pagenow); } });</script>";

}
