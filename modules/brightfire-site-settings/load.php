<?php
/**
 * BrightFire Site Settings.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Site_Settings;

define( 'SITE_SETTINGS_CAPABILITY', 'manage_network' );

/** Require Files */
require_once dirname( __FILE__ ) . '/functions.php';
require_once dirname( __FILE__ ) . '/actions.php';
require_once dirname( __FILE__ ) . '/ajax.php';


add_action( 'admin_menu', __NAMESPACE__ . '\add_settings_page', 11 );
add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\enqueue_admin_scripts' );
