<?php
/**
 * BrightFire Site Settings.
 *
 * @package BrightFireCore
 */

namespace BrightFireCore\Site_Settings;

/**
 * Buildout dynamic publish box for settings pages
 */
function publish_metabox_callback() {

	echo '<div id="ajax-response"><div style="margin: -7px -12px; border-left: 4px solid #00a0d2; padding: 1px 12px;"><p style="margin: .5em 0; padding: 2px;">Save Changes before leaving!</p></div></div>';

	echo '<div id="major-publishing-actions" style="margin: 6px -12px -12px;">';
	echo '<div id="publishing-action">';
	echo '<span class="spinner"></span>';
	echo "<input type='submit' name='bf-settings-publish' id='bf-settings-publish' class='button button-primary button-large' value='Save Changes'></div>";
	echo '<div class="clear"></div>';
	echo '</div>';

}

/**
 * Get our options formatted to match our fields
 *
 * @return array
 */
function get_instance_settings() {

	$instance = array();
	$fields   = define_general_settings();

	foreach ( $fields as $setting_id => $data ) {

		$instance[ $setting_id ] = get_option( $setting_id, '' );

	}

	return $instance;

}

/**
 * Admin API Fields define for general settings
 *
 * @return array
 */
function define_general_settings() {

	$fields = array(
		'bf_setting_website_package'    => array(
			'label'   => 'Website Package',
			'type'    => 'select',
			'choices' => define_package_options(),
		),
		'bf_setting_marketing_services' => array(
			'label'   => 'Marketing Services',
			'type'    => 'checkbox_multi',
			'choices' => define_marketing_services_options(),
		),
		'bf_setting_optional_features'  => array(
			'label'   => 'Optional Features',
			'type'    => 'checkbox_multi',
			'choices' => define_optional_features_options(),
		),
		'bf_setting_content_set'        => array(
			'label'   => 'Content Set',
			'type'    => 'selectize',
			'choices' => define_content_set_options(),
		),
		'bf_setting_analytics_id'       => array(
			'label'         => 'Google Analytics ID',
			'description'   => 'UA-xxxxxxxx-1',
			'type'          => 'text',
			'default_value' => '',
		),
		'bf_setting_google_verify'      => array(
			'label'         => 'Google Webmaster Code',
			'type'          => 'text',
			'default_value' => '',
		),
		'bf_setting_bing_verify'        => array(
			'label'         => 'Bing Webmaster Code',
			'type'          => 'text',
			'default_value' => '',
		),
	);

	return apply_filters( 'bf_general_settings_fields', $fields );

}

/**
 * Filter for Package Options
 *
 * @return mixed
 */
function define_package_options() {

	$options = array();

	return apply_filters( 'bf_website_package_options', $options );

}

/**
 * Filter for Marketing Services
 *
 * @return mixed
 */
function define_marketing_services_options() {

	$options = array();

	return apply_filters( 'bf_marketing_services_options', $options );

}

/**
 * Filter for Optional Features
 *
 * @return mixed
 */
function define_optional_features_options() {

	$options = array();

	return apply_filters( 'bf_optional_features_options', $options );

}

/**
 * Filter for Content Set
 *
 * @return mixed
 */
function define_content_set_options() {

	$options = array();

	return apply_filters( 'bf_content_set_options', $options );

}
