module.exports = {
	all: {
		files: {
			'assets/js/brightfire-admin-api.min.js': ['assets/js/brightfire-admin-api.js'],
            'assets/js/brightfire-admin-api-customizer.min.js': ['assets/js/brightfire-admin-api-customizer.js'],
			'assets/js/brightfire-customizer-controls.min.js': ['modules/brightfire-customizer-hooks/assets/js/customizer-controls.js'],
            'assets/js/brightfire-customizer-preview.min.js': ['modules/brightfire-customizer-hooks/assets/js/customizer-preview.js'],
            'assets/js/brightfire-site-settings.min.js': ['assets/js/brightfire-site-settings.js'],
            'assets/js/brightfire-core-frontend.min.js': ['assets/js/brightfire-core-frontend.js'],
        },
		options: {
			banner: '',
			mangle: {
				except: ['jQuery']
			}
		}
	}
};
