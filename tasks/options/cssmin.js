module.exports = {
	options: {
		banner: '/*! <%= pkg.title %> - v<%= pkg.version %>\n' +
		' * <%=pkg.homepage %>\n' +
		' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
		' * Licensed GPL-2.0+' +
		' */\n'
	},
	minify: {
		expand: true,

		cwd: 'assets/css/',
		src: [
			'brightfire-admin-api.css',
			'brightfire-footer-credits.css',
			'brightfire-pagination-links.css',
			'brightfire-recent-blog-posts.css',
			'brightfire-child-page-links.css',
			'brightfire-custom-icons.css'
		],

		dest: 'assets/css/',
		ext: '.min.css'
	}
};
