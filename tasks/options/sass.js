module.exports = {
	all: {
		options: {
			precision: 2,
			sourceMap: true
		},
		files: {
			'assets/css/brightfire-admin-api.css': 'modules/brightfire-admin-api/assets/css/brightfire-admin-api.scss',
			'assets/css/brightfire-custom-icons.css': 'modules/brightfire-custom-icons/assets/css/brightfire-custom-icons.scss'
		}
	}
};