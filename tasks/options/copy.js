module.exports = {
    main: {
        files: [
            // includes files within path
            {flatten: true, expand: true, src: ['modules/**/assets/images/*.*'], dest: 'assets/images', filter: 'isFile'}
        ]
    }
};
