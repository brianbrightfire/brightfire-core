module.exports = {
	options: {
		stripBanners: true,
			banner: '/*! <%= pkg.title %> - v<%= pkg.version %>\n' +
		' * <%= pkg.homepage %>\n' +
		' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
		' * Licensed GPL-2.0+' +
		' */\n'
	},
	admin_api: {
		src: [
            'modules/brightfire-admin-api/assets/js/jquery.bf_checkbox.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_codeblock.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_beforeunload.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_colorPickers.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_datePickers.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_imagePickers.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_limitCounters.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_progress_bar.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_range.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_repeaters.js',
			'modules/brightfire-admin-api/assets/js/jquery.bf_selectize.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_url.js',
            'modules/brightfire-admin-api/assets/js/brightfire-admin-api.js'
		],
		dest: 'assets/js/brightfire-admin-api.js'
	},
    admin_api_customizer: {
        src: [
            'modules/brightfire-admin-api/assets/js/jquery.bf_checkbox.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_codeblock.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_colorPickers.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_datePickers.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_imagePickers.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_limitCounters.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_progress_bar.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_range.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_repeaters.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_selectize.js',
            'modules/brightfire-admin-api/assets/js/jquery.bf_url.js',
            'modules/brightfire-admin-api/assets/js/brightfire-admin-api-customizer.js'
        ],
        dest: 'assets/js/brightfire-admin-api-customizer.js'
    },
	core_frontend: {
		src: [
            'assets/js/brightfire-core-utilities.js',
			'modules/brightfire-categories-archives/assets/js/REST.js',
            'modules/brightfire-links/assets/js/brightfire-links.js',
            'modules/brightfire-images/assets/js/bf_lazyload.js',
            'modules/brightfire-images/assets/js/bf_ie_image_width.js'
        ],
		dest: 'assets/js/brightfire-core-frontend.js'
    },
    site_settings: {
        src: [
            'modules/brightfire-site-settings/assets/js/admin.js'
        ],
        dest: 'assets/js/brightfire-site-settings.js'
    }
};
