module.exports = {
	livereload: {
		files: ['assets/css/*.css'],
		options: {
			livereload: true
		}
	},
	css: {
		files: ['modules/**/assets/css/*.scss'],
		tasks: ['css'],
		options: {
			debounceDelay: 500
		}
	},
	js: {
		files: ['modules/**/assets/js/*.js'],
			tasks: ['js'],
			options: {
			debounceDelay: 500
		}
	},
	copy: {
		files: ['modules/**/assets/images/*.*'],
		tasks: ['copy']
	}
};
