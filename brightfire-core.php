<?php
/**
 * Plugin Name:  BrightFire Core
 * Plugin URI:   https://www.brightfire.com
 * Description:  Core Asset Library for BrightFire Networks
 * Version:      1.3.6
 * Author:       BrightFire Development Team
 * Author URI:   https://developer.wordpress.org/
 * License:      GPL2
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
 *
 * @package BrightFireCore
 */

namespace BrightFireCore;

define( 'BF_CORE_VERSION', '1.3.6' );
define( 'BF_CORE_PATH', dirname( __FILE__ ) . '/' );
define( 'BF_CORE_MODULES', BF_CORE_PATH . 'modules/' );
define( 'BF_CORE_PLUGINS', BF_CORE_PATH . 'plugins/' );
define( 'BF_CORE_THEMES', BF_CORE_PATH . 'themes/' );
define( 'BF_CORE_ASSETS', plugin_dir_url( __FILE__ ) . 'assets/' );
define( 'BF_CORE_VENDOR', plugin_dir_url( __FILE__ ) . 'vendor/' );

// +-------------------------------------------------+
// | Vendor                                          |
// +-------------------------------------------------+
require_once BF_CORE_PATH . 'vendor/load.php';

// +-------------------------------------------------+
// | Modules                                         |
// +-------------------------------------------------+
require_once BF_CORE_MODULES . 'brightfire-utilities/brightfire-utilities.php';
require_once BF_CORE_MODULES . 'brightfire-sass/load.php';
require_once BF_CORE_MODULES . 'brightfire-admin-api/brightfire-admin-api.php';
require_once BF_CORE_MODULES . 'brightfire-block-private-content/load.php';
require_once BF_CORE_MODULES . 'brightfire-callout/load.php';
require_once BF_CORE_MODULES . 'brightfire-categories-archives/load.php';
require_once BF_CORE_MODULES . 'brightfire-child-page-links/load.php';
require_once BF_CORE_MODULES . 'brightfire-current-page-content/load.php';
require_once BF_CORE_MODULES . 'brightfire-custom-icons/brightfire-custom-icons.php';
require_once BF_CORE_MODULES . 'brightfire-custom-redirects/load.php';
require_once BF_CORE_MODULES . 'brightfire-customizer-hooks/load.php';
require_once BF_CORE_MODULES . 'brightfire-dashboard-widgets/load.php';
require_once BF_CORE_MODULES . 'brightfire-defer-js/load.php';
require_once BF_CORE_MODULES . 'brightfire-environment/load.php';
require_once BF_CORE_MODULES . 'brightfire-footer-credits/load.php';
require_once BF_CORE_MODULES . 'brightfire-google-analytics/load.php';
require_once BF_CORE_MODULES . 'brightfire-images/load.php';
require_once BF_CORE_MODULES . 'brightfire-links/load.php';
require_once BF_CORE_MODULES . 'brightfire-network-sites/load.php';
require_once BF_CORE_MODULES . 'brightfire-pages/load.php';
require_once BF_CORE_MODULES . 'brightfire-pagination-links/brightfire-pagination-links.php';
require_once BF_CORE_MODULES . 'brightfire-pingbacks/load.php';
require_once BF_CORE_MODULES . 'brightfire-recent-blog-posts/load.php';
require_once BF_CORE_MODULES . 'brightfire-robots-txt/load.php';
require_once BF_CORE_MODULES . 'brightfire-roles-capabilities/load.php';
require_once BF_CORE_MODULES . 'brightfire-rss/load.php';
require_once BF_CORE_MODULES . 'brightfire-site-link/brightfire-site-link.php';
require_once BF_CORE_MODULES . 'brightfire-site-settings/load.php';
require_once BF_CORE_MODULES . 'brightfire-submitdiv/load.php';
require_once BF_CORE_MODULES . 'brightfire-virtual-pages/brightfire-virtual-pages.php';
require_once BF_CORE_MODULES . 'brightfire-wp-filters/load.php';
require_once BF_CORE_MODULES . 'brightfire-wp-admin/load.php';
require_once BF_CORE_MODULES . 'brightfire-wp-head/load.php';
require_once BF_CORE_MODULES . 'brightfire-wp-media/load.php';
require_once BF_CORE_MODULES . 'brightfire-wp-search/load.php';
require_once BF_CORE_MODULES . 'brightfire-wp-smtp/brightfire-wp-smtp.php';

// +-------------------------------------------------+
// | Plugins                                         |
// +-------------------------------------------------+
require_once BF_CORE_PLUGINS . 'brightfire-landing-pages/load.php';
require_once BF_CORE_PLUGINS . 'gravityforms/load.php';
require_once BF_CORE_PLUGINS . 'svg-support/load.php';
require_once BF_CORE_PLUGINS . 'wp-smush-pro/load.php';
require_once BF_CORE_PLUGINS . 'yoast/load.php';
require_once BF_CORE_PLUGINS . 'blogtemplates/load.php';



/** --------------------------------------------------------------------- */
/** Scripts and Styles                                                    */
/** --------------------------------------------------------------------- */
function register_scripts_frontend() {
	wp_enqueue_script(
		'brightfire_core_frontend',
		BF_CORE_ASSETS . 'js/brightfire-core-frontend.min.js',
		array(
			'jquery',
			'lazyload',
		),
		BF_CORE_VERSION,
		true
	);
}
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\register_scripts_frontend', 10 );
