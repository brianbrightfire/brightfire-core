<?php

namespace BrightFireCore\Vendor;

/**
 * Queue our script registration for admin and front end.
 */
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\register_scripts', 9 );
add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\register_scripts',9 );

/**
 * Register all Vendor scripts and styles
 */
function register_scripts(){

	/**
	 * FLEXSLIDER
	 */
	wp_register_script('flexslider', BF_CORE_VENDOR . 'flexslider/jquery.flexslider-min.js' , array( 'jquery' ), '2.5.0', true );

	/**
	 * FONTAWESOME
	 *
	 * When updating fontawesome, pull new .yml file from repo and run fontawesome_build_utility() to create
	 * a new JSON file for our fontawseome selectize to work
	 *
	 * @see fontawesome_build_utility()
	 *
	 * @link https://github.com/FortAwesome/Font-Awesome/blob/master/src/icons.yml
	 */
	wp_register_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', [], '4.7.0' );

	/**
	 * SLICK
	 *
	 * TODO: The `bf_slick_js` handle refers to the fix in this issue: https://github.com/kenwheeler/slick/issues/3458
	 * TODO: This affects centerMode, we should be using ONLY the bf_slick_js intil above has a fix
	 */
	wp_register_script('bf_slick_js', BF_CORE_VENDOR . 'slick/slick/bf_slick.min.js' , array( 'jquery' ), '1.9', true );
//	wp_register_script('slick_js', BF_CORE_VENDOR . 'slick/slick/slick.min.js' , array( 'jquery' ), '1.9', true );
	wp_register_style('slick_css', BF_CORE_VENDOR . 'slick/slick/slick.css' , '', '1.9' );
	wp_register_style('slick_theme', BF_CORE_VENDOR . 'slick/slick/slick-theme.css' , array( 'slick_css' ), '1.9' );

	/**
	 * SELECTIZE
	 * @todo: current version is 0.12.4 - need to update and test
	 */
	wp_register_script( 'selectize', BF_CORE_VENDOR . 'selectize/selectize.min.js', array( 'jquery' ), '0.12.1', true );

	/**
	 * VUE
	 */
	$vue_file = ( defined( 'BF_ENV' )  && 'DEV' == BF_ENV ) ? 'vue.js' : 'vue.min.js';
	wp_register_script( 'vue', BF_CORE_VENDOR . 'vue/' . $vue_file, '', '2.5.13', true );

	/**
	 * ACTUAL
	 */
	wp_register_script('actual', BF_CORE_VENDOR . 'actual/jquery.actual.min.js' , array( 'jquery' ), '1.8', true );

	/**
	 * LazyLoad
	 *
	 * @link https://www.andreaverlicchi.eu/lazyload/
	 */
	add_action( 'wp_enqueue_scripts', function() {
        wp_register_script( 'lazyload', 'https://cdnjs.cloudflare.com/ajax/libs/vanilla-lazyload/10.19.0/lazyload.min.js', array('jquery'), '10.19.0', true );
	});
}