# BrightFire Core

## 1.3.6
* Updates JSON to be valid

## 1.3.5
* Adds .gitattributes file to control VCS exports

## 1.3.4
* Another version update for testing builds

## 1.3.3
* Another version update for testing builds

## 1.3.2
* Just a version update for testing builds

## 1.3.1
* FIX: Don't use BF_RELEASE_VERSION (not available)
* FIX: README.md formatting

## 1.3.0
* FIX: Composer Name / Requirements
 
## 1.2.0
* Add Composer.json Support

## 1.1.0
* Content Marketing support:
    * Image factory support for "external" images from other network sites
    * Pagination to include Content Marketing posts
    * CPC featured image support for Content Marketing posts

## 1.0.1
* Module: Footer Credits: Add rel="nofollow" to BrightFire credits link

## 1.0.0
* Initial Release Version